package frgp.utn.edu.ar.main;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


import frgp.utn.edu.ar.entidad.*;


public class Main1 {

	public static void main(String[] args) {
		
        //SessionFactory sessionFactory;
        
        
        //Configuration configuration = new Configuration();
        //configuration.configure();
        
      //  ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        //sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        SessionFactory sessionFactory = frgp.utn.edu.ar.Dao.HibernateUtil.getSessionFactory();
		//Session session = sessionFactory.openSession();
        
        Session session = sessionFactory.openSession();
        
        session.beginTransaction();

		//Guardo Tipo cuentas
		TipoCuenta ca_pesos = AddTipoCuenta("Caja de ahorro en pesos","ca_pesos", session);
		TipoCuenta ca_dolares = AddTipoCuenta("Caja de ahorro en dolares","ca_dolares", session);
		TipoCuenta cc = AddTipoCuenta("Cuenta corriente","cc", session);
		TipoCuenta cc_esp_pesos = AddTipoCuenta("Cuenta corriente especial en pesos","cc_esp_pesos", session);
		TipoCuenta cc_esp_dolares = AddTipoCuenta("Cuenta corriente especial en dolares","cc_esp_dolares", session);

//		//Guardo Tipo Moviminetos
		TipoMovimiento alta_cuenta = AddTipoMovimiento("Alta de cuenta","alta_cuenta", session);
		TipoMovimiento alta_prestamo = AddTipoMovimiento("Alta Prestamo","alta_prestamo", session);
		TipoMovimiento pago_prestamo = AddTipoMovimiento("Pago prestamo","pago_prestamo", session);
		TipoMovimiento transferencia = AddTipoMovimiento("Transferencia","transferencia", session);

		
		//Inserto Perfiles Admin y cliente
		Perfil admin = AddPerfil("administrador", session);
		Perfil cliente = AddPerfil("cliente", session);

		
		//Insert provincias
		Provincia bsas = addProvincia("Buenos Aires", session);
		Provincia enri = addProvincia("Entre Rios", session);
		Provincia mend = addProvincia("Mendoza", session);
		Provincia saju = addProvincia("San Juan", session);
		Provincia sacr = addProvincia("Santa Cruz", session);
		Provincia safe = addProvincia("Santa Fe", session);
		Provincia misi = addProvincia("Misiones", session);
		Provincia lapa = addProvincia("La Pampa", session);
		
		Localidad pacheco = addLocalidad(1234, "Pacheco", bsas, session);
		Localidad escobar = addLocalidad(2345, "Escobar", bsas, session);
		Localidad benavidez = addLocalidad(1236, "Benavidez", bsas, session);
		Localidad castelar = addLocalidad(1237, "Castelar", bsas, session);
		Localidad ituzaingo = addLocalidad(1238, "Ituzaingo", bsas, session);
		Localidad sanisidro = addLocalidad(1239, "San Isidro", bsas, session);
		

		//Alta de personas
		Persona adm = AddPersona(
				00000000, "admin", "admin",
				"00000000000", "0", "0",
				new Date(), "admin@asd.com", "Argentino", "Masculino", pacheco, bsas, session);
		Persona per1 = AddPersona(
				11111123, "Jose", "Garcia",
				"20111111117", "Arcos", "222",
				new Date(), "jgarcia@asd.com", "Argentino", "Masculino", castelar, bsas, session);
		Persona per2 = AddPersona(
				22222223, "Lucia", "Diaz",
				"20222222227", "Hipolito", "264",
				new Date(), "ldelatorre@asd.com", "Argentino", "Feminino", ituzaingo, bsas, session);
		Persona per3 = AddPersona(
				33333334, "Pedro", "Pedros",
				"20333333337", "Hipolito", "111",
				new Date(), "jperez@asd.com", "Argentino", "Masculino", sanisidro, bsas, session);
		Persona per4 = AddPersona(
				44444445, "Jorge", "Perez",
				"20444444447", "Mariscal Perez", "665",
				new Date(), "jperez@asd.com", "Argentino", "Masculino", escobar, bsas, session);
		Persona per5 = AddPersona(
				55555556, "Carla", "Garcia",
				"20555555557", "Coronel Asd", "6542",
				new Date(), "jperez@asd.com", "Argentino", "Femenino", benavidez, bsas, session);
		Persona per6 = AddPersona(
				66666667, "Josefina", "Hernandez",
				"20666666667", "Yrigoyen", "111",
				new Date(), "jperez@asd.com", "Argentino", "Femenino", pacheco, bsas, session);
		Persona per7 = AddPersona(
				77777778, "maria", "Fernandez",
				"20777777777", "San Pedro", "2256",
				new Date(), "jperez@asd.com", "Argentino", "Femenino", pacheco, bsas, session);
		Persona per8 = AddPersona(
				111113453, "Jorge", "Ruckauf",
				"20888888887", "Hipolito", "111",
				new Date(), "jperez@asd.com", "Argentino", "Masculino", castelar, bsas, session);
		Persona per9 = AddPersona(
				99123123, "Ana", "Lopez",
				"20999999997", "Sanz Pena", "345",
				new Date(), "alopez@asd.com", "Argentino", "Femenino", ituzaingo, bsas, session);


		Usuario usuadmin = AddUsuario(adm, admin, "admin", "admin", session);
		Usuario usu1 = AddUsuario(per1, cliente, "cli1", "cli1", session);
		Usuario usu2 = AddUsuario(per2, cliente, "cli2", "cli2", session);
		Usuario usu3 = AddUsuario(per3, cliente, "cli3", "cli3", session);
		Usuario usu4 = AddUsuario(per4, cliente, "cli4", "cli4", session);
		Usuario usu5 = AddUsuario(per5, cliente, "cli5", "cli5", session);
		Usuario usu6 = AddUsuario(per6, cliente, "cli6", "cli6", session);
		Usuario usu7 = AddUsuario(per7, cliente, "cli7", "cli7", session);
		Usuario usu8 = AddUsuario(per8, cliente, "cli8", "cli8", session);
		Usuario usu9 = AddUsuario(per9, cliente, "cli9", "cli9", session);

		
		double importeAltaCuenta=10000.0;
        Cuenta cue1 = addCuenta(
        		ca_pesos, "111111", per1, "ca_pesos-111111", "ca_pesos-111111",
        		"1111111111111111111111", importeAltaCuenta, "ca_pesos-111111", new Date(), session);
        Cuenta cue2 = addCuenta(
        		ca_pesos, "222222", per2, "ca_pesos-222222", "ca_pesos-222222",
        		"2222222222222222222222", importeAltaCuenta, "ca_pesos-222222", new Date(), session);
        Cuenta cue3 = addCuenta(
        		ca_pesos, "333333", per3, "ca_pesos-333333", "ca_pesos-333333",
        		"3333333333333333333333", importeAltaCuenta, "ca_pesos-333333", new Date(), session);
        Cuenta cue4 = addCuenta(
        		ca_pesos, "444444", per4, "ca_pesos-444444", "ca_pesos-444444",
        		"4444444444444444444444", importeAltaCuenta, "ca_pesos-444444", new Date(), session);
        Cuenta cue5 = addCuenta(
        		ca_pesos, "555555", per5, "ca_pesos-555555", "ca_pesos-555555",
        		"555555555555555555555", importeAltaCuenta, "ca_pesos-555555", new Date(), session);
        Cuenta cue6 = addCuenta(
        		ca_pesos, "666666", per6, "ca_pesos-666666", "ca_pesos-666666",
        		"66666666666666666666", importeAltaCuenta, "ca_pesos-666666", new Date(), session);
        Cuenta cue7 = addCuenta(
        		ca_pesos, "777777", per7, "ca_pesos-777777", "ca_pesos-777777",
        		"7777777777777777777777", importeAltaCuenta, "ca_pesos-777777", new Date(), session);

        //Movimiento mov1 = addMovimiento("Alta de Cuenta", tm_altaCuenta, 2222.0, new Date(), session);
        //Movimiento mov2 = addMovimiento("Alta de Cuenta", tm_altaCuenta, 3333.0, new Date(), session);
        //Movimiento mov3 = addMovimiento("Alta de Cuenta", tm_altaCuenta, 4444.0, new Date(), session);
        //Movimiento mov4 = addMovimiento("Alta de Cuenta", tm_altaCuenta, 5555.0, new Date(), session);
        //Movimiento mov5 = addMovimiento("Alta de Cuenta", tm_altaCuenta, 6666.0, new Date(), session);
        //Movimiento mov6 = addMovimiento("Alta de Cuenta", tm_altaCuenta, 7777.0, new Date(), session);
        //Movimiento mov7 = addMovimiento("Alta de Cuenta", tm_altaCuenta, 8888.0, new Date(), session);

        //AltaCuenta addAltaCuenta(Cuenta cuenta, double monto, Session session) {
        AltaCuenta acta1 = addAltaCuenta(cue1, importeAltaCuenta, session);
        AltaCuenta acta2 = addAltaCuenta(cue2, importeAltaCuenta, session);
        AltaCuenta acta3 = addAltaCuenta(cue3, importeAltaCuenta, session);
        AltaCuenta acta4 = addAltaCuenta(cue4, importeAltaCuenta, session);
        AltaCuenta acta5 = addAltaCuenta(cue5, importeAltaCuenta, session);
        AltaCuenta acta6 = addAltaCuenta(cue6, importeAltaCuenta, session);
        AltaCuenta acta7 = addAltaCuenta(cue7, importeAltaCuenta, session);

        //Movimiento addMovimiento(String detalle, TipoMovimiento tp, Date fecha, Session session)
        Movimiento mov1 = addMovimiento("Alta de Cuenta", alta_cuenta, new Date(), session,10000.0 ,cue1);
        Movimiento mov2 = addMovimiento("Alta de Cuenta", alta_cuenta, new Date(), session,10000.0,cue2);
        Movimiento mov3 = addMovimiento("Alta de Cuenta", alta_cuenta, new Date(), session,10000.0,cue3);
        Movimiento mov4 = addMovimiento("Alta de Cuenta", alta_cuenta, new Date(), session,10000.0,cue4);
        Movimiento mov5 = addMovimiento("Alta de Cuenta", alta_cuenta, new Date(), session,10000.0,cue5);
        Movimiento mov6 = addMovimiento("Alta de Cuenta", alta_cuenta, new Date(), session,10000.0,cue6);
        Movimiento mov7 = addMovimiento("Alta de Cuenta", alta_cuenta, new Date(), session,10000.0,cue7);

        //Transferencias
        double importe=100;
        Transferencia tra1 = addTransferencia(cue1, cue2, importe, session);
        Transferencia tra2 = addTransferencia(cue2, cue3, importe, session);
        Transferencia tra3 = addTransferencia(cue3, cue4, importe, session);
        Transferencia tra4 = addTransferencia(cue3, cue5, importe, session);
        Transferencia tra5 = addTransferencia(cue3, cue6, importe, session);
        Transferencia tra6 = addTransferencia(cue3, cue7, importe, session);

        //Movimiento addMovimiento(String detalle, TipoMovimiento tp, Transaccion transaccion, Date fecha, Session session)
       
        Movimiento movTra1a = addMovimiento("transferencia", transferencia, new Date(), session,importe * (-1),cue1);
        Movimiento movTra1b = addMovimiento("transferencia", transferencia, new Date(), session,importe ,cue2);
        
        //TODO sumar y restar montos en cuentas 
        cue1.setSaldo(cue1.getSaldo() - importe);
        session.save(cue1);
        
        cue2.setSaldo(cue2.getSaldo() + importe);
        session.save(cue1);
        
        Movimiento movTra2a = addMovimiento("transferencia", transferencia, new Date(), session,importe * (-1),cue2);
        Movimiento movTra2b = addMovimiento("transferencia", transferencia, new Date(), session,importe ,cue3);
        
        //TODO sumar y restar montos en cuentas 
        cue2.setSaldo(cue2.getSaldo() - importe);
        session.save(cue2);
        
        cue3.setSaldo(cue3.getSaldo() + importe);
        session.save(cue3); 
        
        Movimiento movTra3a = addMovimiento("transferencia", transferencia, new Date(), session,importe * (-1),cue3);
        Movimiento movTra3b = addMovimiento("transferencia", transferencia, new Date(), session,importe ,cue4);
        //TODO sumar y restar montos en cuentas 
       
        cue3.setSaldo(cue3.getSaldo() - importe);
        session.save(cue3);
        
        cue4.setSaldo(cue4.getSaldo() + importe);
        session.save(cue4);  
        
        Movimiento movTra4a = addMovimiento("transferencia", transferencia, new Date(), session,importe * (-1),cue3);
        Movimiento movTra4b = addMovimiento("transferencia", transferencia, new Date(), session,importe ,cue5);
        //TODO sumar y restar montos en cuentas 
        cue3.setSaldo(cue3.getSaldo() - importe);
        session.save(cue3);
        
        cue5.setSaldo(cue5.getSaldo() + importe);
        session.save(cue5);   
        Movimiento movTra5a = addMovimiento("transferencia", transferencia, new Date(), session,importe * (-1),cue3);
        Movimiento movTra5b = addMovimiento("transferencia", transferencia, new Date(), session,importe ,cue6);
        //TODO sumar y restar montos en cuentas 
        cue3.setSaldo(cue3.getSaldo() - importe);
        session.save(cue3);
        
        cue6.setSaldo(cue6.getSaldo() + importe);
        session.save(cue6);   
        Movimiento movTra6a = addMovimiento("transferencia", transferencia, new Date(), session,importe * (-1),cue3);
        Movimiento movTra6b = addMovimiento("transferencia", transferencia, new Date(), session,importe ,cue7);
        //TODO sumar y restar montos en cuentas 
        cue3.setSaldo(cue3.getSaldo() - importe);
        session.save(cue3);
        
        cue7.setSaldo(cue7.getSaldo() + importe);
        session.save(cue7); 
        
//        Movimiento movTP3 = addMovimiento("transferencia", transferencia, tra3, new Date(), session);
//        Movimiento movTN3 = addMovimiento("transferencia", transferencia, tra3, new Date(), session);
//        Movimiento movTP4 = addMovimiento("transferencia", transferencia, tra4, new Date(), session);
//        Movimiento movTN4 = addMovimiento("transferencia", transferencia, tra4, new Date(), session);
//        Movimiento movTP5 = addMovimiento("transferencia", transferencia, tra5, new Date(), session);
//        Movimiento movTN5 = addMovimiento("transferencia", transferencia, tra5, new Date(), session);
//        Movimiento movTP6 = addMovimiento("transferencia", transferencia, tra6, new Date(), session);
//        Movimiento movTN6 = addMovimiento("transferencia", transferencia, tra6, new Date(), session);


        //Prestamos
        EstadoPrestamo pendiente = addEstadoPrestamo(1,"pendiente", "Pendiente", session);
        EstadoPrestamo aceptado = addEstadoPrestamo(2,"aceptado", "Aceptado", session);
        EstadoPrestamo rechazado = addEstadoPrestamo(3,"rechazado", "Rechazado", session);
        EstadoPrestamo cancelado = addEstadoPrestamo(4,"cancelado", "Cancelado", session);
        EstadoPrestamo completado = addEstadoPrestamo(5,"completado", "Completado", session);

        
        //Usuario usu, Date fecha, int cuotas, int plazo, double monto, Session session

        double importePrestamo=2000;
        Prestamo pre1 = addPrestamo(per1, new Date(), 3, 3, importePrestamo, pendiente, session, cue1);
        Prestamo pre2 = addPrestamo(per2, new Date(), 2, 2, importePrestamo, pendiente, session, cue2);
        Prestamo pre3 = addPrestamo(per3, new Date(), 4, 4, importePrestamo, pendiente, session, cue3);
        Prestamo pre4 = addPrestamo(per4, new Date(), 3, 3, importePrestamo, pendiente, session, cue4);
        Prestamo pre5 = addPrestamo(per5, new Date(), 1, 1, importePrestamo, pendiente, session, cue5);
        Prestamo pre6 = addPrestamo(per6, new Date(), 2, 2, importePrestamo, pendiente, session, cue6);
        Prestamo pre7 = addPrestamo(per7, new Date(), 1, 1, importePrestamo, pendiente, session, cue7);


        //addCuota(Prestamo prestamo, int nroCuota, Date fechaPago, Session session)
        
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP11 = addCuota(pre1, 1, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP12 = addCuota(pre1, 2, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP13 = addCuota(pre1, 3, cal.getTime(), session);

        cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP21 = addCuota(pre2, 1, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP22 = addCuota(pre2, 2, cal.getTime(), session);

        cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP31 = addCuota(pre3, 1, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP32 = addCuota(pre3, 2, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP33 = addCuota(pre3, 3, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP34 = addCuota(pre3, 4, cal.getTime(), session);

        cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP41 = addCuota(pre4, 1, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP42 = addCuota(pre4, 2, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP43 = addCuota(pre4, 3, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP51 = addCuota(pre5, 1, cal.getTime(), session);

        cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP61 = addCuota(pre6, 1, cal.getTime(), session);
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP62 = addCuota(pre6, 2, cal.getTime(), session);

        cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        Cuota cuoP71 = addCuota(pre7, 1, cal.getTime(), session);

        session.getTransaction().commit();
        session.close();
        sessionFactory.close();


	}
	
	public static Provincia addProvincia(String nombre, Session session) {
    	Provincia pro = new Provincia();
    	pro.setProvincia(nombre);
    	pro.setActivo(true);
    	session.save(pro);
    	return pro;
	}
	
	public static Localidad addLocalidad(int cp, String nombre, Provincia pro, Session session) {
		Localidad loc = new Localidad();
		loc.setCP(cp);
		loc.setLocalidad(nombre);
		loc.setProvincia(pro);
		loc.setActivo(true);
		session.save(loc);
		return loc;
	}

	public static Cuota addCuota(Prestamo prestamo, int nroCuota, Date fechaVencimiento, Session session) {
		Cuota cuo = new Cuota();
		cuo.setPrestamo(prestamo);
		cuo.setNroCuota(nroCuota);
		cuo.setFechaVencimiento(fechaVencimiento);
		cuo.setFechaPago(null);
		cuo.setMonto( prestamo.getMonto() / prestamo.getCuotas() );
		session.save(cuo);
		return cuo;
	}

	public static EstadoPrestamo addEstadoPrestamo(int id,String codigo, String descripcion, Session session) {
		EstadoPrestamo estPre = new EstadoPrestamo();
		estPre.setId(id);
		estPre.setCodigo(codigo);
		estPre.setDescripcion(descripcion);
		session.save(estPre);
		return estPre;
	}

	public static Prestamo addPrestamo(Persona usu, Date fecha, int cuotas, int plazo, double monto, EstadoPrestamo estPre, Session session, Cuenta cuenta) {
		Prestamo pre = new Prestamo();
		pre.setCliente(usu);
		pre.setFecha(fecha);
		pre.setCuotas(cuotas);
		pre.setPlazo(plazo);
		pre.setMonto(monto);
		pre.setMontoPorMes(monto/cuotas);
		pre.setEstado(estPre);
		pre.setCuentaDestino(cuenta);
		session.save(pre);
		return pre;
	}


	public static Transferencia addTransferencia(Cuenta orig, Cuenta dest, double monto, Session session) {
		Transferencia tra = new Transferencia();
		tra.setCuentaOrigen(orig);
		tra.setCuentaDestino(dest);
		tra.setMonto(monto);
		tra.setFechaAlta(new Date());
		session.save(tra);
		return tra;
	}
	
	public static AltaCuenta addAltaCuenta(Cuenta cuenta, double monto, Session session) {
		AltaCuenta acta = new AltaCuenta();
		acta.setCuenta(cuenta);
		acta.setMonto(monto);
		session.save(acta);
		return acta;
	}

	public static Cuenta addCuenta(TipoCuenta tp, String nroCuenta, Persona cli, String codigo, String desc, String cbu, Double saldo, String nombre, Date fecha, Session session) {
	    Cuenta cue = new Cuenta();
	    cue.setTipoCuenta(tp);
	    cue.setNroCuenta(nroCuenta);
	    cue.setCliente(cli);;
	    cue.setCodigo(codigo);
	    cue.setDescripcion(desc);
	    cue.setCbu(cbu);
	    cue.setSaldo(saldo);
	    cue.setNombre(nombre);
	    cue.setFechaCreacion(fecha);
	    cue.setActivo(true);
	    session.save(cue);
	    return cue;
	}


	public static Movimiento addMovimiento(String detalle, TipoMovimiento tp, Date fecha, Session session,Double importe,Cuenta cuenta) {
		Movimiento mov = new Movimiento();
		mov.setDetalleMovimiento(detalle);
		mov.setTipoMovimiento(tp);
		mov.setFechaAlta(fecha);
		mov.setImporte(importe);
		mov.setCuenta(cuenta);
		session.save(mov);
		return mov;
	}

	public static TipoCuenta AddTipoCuenta(String tipoCuenta,String codigo, Session session) {
    	TipoCuenta tc = new TipoCuenta();
    	tc.setTipoCuenta(tipoCuenta);
    	tc.setCodigo(codigo);
    	tc.setActivo(true);
    	session.save(tc);
		return tc;
	}

	public static TipoMovimiento AddTipoMovimiento(String movimiento,String codigo, Session session) {
		TipoMovimiento tm = new TipoMovimiento();
		tm.setMovimiento(movimiento);
		tm.setCodigo(codigo);
		tm.setActivo(true);
		session.save(tm);
		return tm;
	}

	public static Perfil AddPerfil(String perfil, Session session) {
		Perfil prf = new Perfil();
		prf.setPerfil(perfil);
		prf.setActivo(true);
		session.save(prf);
		return prf;
	}

	public static Persona AddPersona(
			int dni, String nombre, String apellido,
			String cuil, String direccion, String direccionNro,
			Date fecha, String email, String nacionalidad, String sexo, Localidad loc, Provincia pro, Session session) {
		Persona per = new Persona();
		per.setDni(dni);
		per.setNombre(nombre);
		per.setApellido(apellido);
		per.setCuil(cuil);
		per.setDireccion(direccion);
		per.setDireccionNro(direccionNro);
		per.setFechaNac(fecha);
		per.setMail(email);
		per.setNacionalidad(nacionalidad);
		per.setSexo(sexo);
		per.setLocalidad(loc);
		per.setProvincia(pro);
		per.setActivo(true);
		session.save(per);
		return per;
	}

	public static Usuario AddUsuario(Persona persona, Perfil perfil,String nombreUsuario, String password, Session session) {
		Usuario usu = new Usuario();
		usu.setPersona(persona);
		usu.setPerfil(perfil);
		usu.setNombreUsuario(nombreUsuario);
		usu.setPassword(password);
		usu.setActivo(true);
		session.save(usu);
		return usu;
	}
}

