package frgp.utn.edu.ar.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="cuotas")
public class Cuota implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="prestamo")
	private Prestamo prestamo;

	@Column(name="nro_cuota")
	private int NroCuota;
	
	@Column(name="fecha_vencimiento")
	private Date FechaVencimiento;
	
	@Column(name="fecha_pago")
	private Date FechaPago;
	
	@Column(name="Monto")
	private double Monto;
	
	public double getMonto() {
		return Monto;
	}

	public void setMonto(double monto) {
		Monto = monto;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}
	
	public Date getFechaVencimiento() {
		return FechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		FechaVencimiento = fechaVencimiento;
	}

	public Prestamo getPrestamo() {
		return prestamo;
	}

	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}

	public int getNroCuota() {
		return NroCuota;
	}

	public void setNroCuota(int nroCuota) {
		NroCuota = nroCuota;
	}

	public Date getFechaPago() {
		return FechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		FechaPago = fechaPago;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
