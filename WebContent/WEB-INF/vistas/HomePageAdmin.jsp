<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/contact_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />" type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<style type="text/css">
	.titulo{
	padding-bottom: inherit;
	}
	
	.rowCuenta{
    padding-bottom: 12px;
    border-top-style: double;
    border-bottom: double;
    padding-top: 21px;
    border-color: coral;
	}
	.textoTitulo{
		    margin-left: auto;
    margin-right: auto;
	}

	.rowCuentaInterior{
	float: left;
    padding-right: inherit;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: auto;
    margin-top: auto;
	}
	
	.iconMoney{
		float: left;
    margin-right: 10px;
	}

/* table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{ */
/*  padding-right: 30px; */
/*  background: coral; */
/* } */


.contenedorPrincipal{
    margin-left: auto;
    margin-right: auto;
    margin-bottom: auto;
    margin-top: auto;
	}
	
	.buttonLogin{
	padding-top: 30px;
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
	}
	
	
	.red_button2 {
    display: -webkit-inline-box;
    display: -moz-inline-box;
    display: -ms-inline-flexbox;
    display: -webkit-inline-flex;
    display: inline-flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    /* width: auto; */
    height: 40px;
    background: #fe4c50;
    border-radius: 3px;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
	</style>

</head>

<body>

	<div class="super_container">

		<!-- Header -->

		<header class="header trans_300"> <!-- Main Navigation -->

 <%@include file="Menu.jsp"%>

		</header>
		<div class="container contact_container">

			<div class="row">
				<div class="col-lg-6 get_in_touch_col contenedorPrincipal">
					<div class="get_in_touch_contents" >
						<h1>Bienvenido a su página Administración ${userName}</h1>
						<table>
						<tr>
						<td>
						<br/>
						<form method="get" action="redirigir_AltaCuenta.html">
							<button id="btnNuevaCuenta" type="submit"
								class="red_button2 message_submit_btn trans_300"
								name="btnAccion" value="NuevaCuenta"
								>Nueva Cuenta
							</button>
						</form>
						</td>
						<td>
						<br/>
						<form method="get" action="redirigir_AddEditUsuario.html">
							<button id="btnNuevoUsuario" type="submit"
								class="red_button2 message_submit_btn trans_300" name="btnAccion"
								value="NuevoUsuario" >
								Nuevo Usuario</button>
						</form>
						</td>
						</tr>
						<tr>
						<td>
						<br/>
						<form method="get" action="Lista_Cuenta_Usuarios.html">
							<button id="btnNuevoUsuario" type="submit"
								class="red_button2 message_submit_btn trans_300" name="btnAccion"
								value="NuevoUsuario" >
								Lista de cuentas</button>
						</form>
						</td>
						<td>
						<br/>
						<form method="get" action="listadoClientes.html">
							<button id="btnListadoClientes" type="submit"
								class="red_button2 message_submit_btn trans_300" name="btnAccion"
								value="ListadoClientes" >
								Listado Clientes
							</button>
						</form>
						</td>
						</tr>
						<tr>
						  <td>
						  <br/>
							  <form method="get" action="Listado_Prestamos.html">
								<button id="btnListadoClientesPrestamo" type="submit"
									class="red_button2 message_submit_btn trans_300" name="btnAccion"
									value="ListadoClientes" style="height:100px; width:100px">
									Listado Prestamos de  Clientes
								</button>
							</form>
						  </td>
						  <td>
						  <br/>
							  <form method="get" action="InformeDeGestion.html">
								<button id="btnListadoClientesPrestamo" type="submit"
									class="red_button2 message_submit_btn trans_300" name="btnAccion"
									value="Informes" style="height:100px; width:100px">
									Gestión Prestamo
								</button>
							  </form>
						  </td>						  
						</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div
					class="footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center">

				</div>
			</div>
			<div class="col-lg-6">
				<div
					class="footer_social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="footer_nav_container">
					<div class="cr" style="text-align: center;">©2020 Trabajo
						practico - Inegrador Final</div>
				</div>
			</div>
		</div>
	</div>
	</footer>

	</div>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap4/popper.js"></script>
	<script src="styles/bootstrap4/bootstrap.min.js"></script>
	<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>