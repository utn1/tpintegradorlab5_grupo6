package frgp.utn.edu.ar.entidad;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name="Usuario")
public class Usuario implements Serializable{

	public Usuario( Persona Persona, Perfil perfil,String nombreUsuario, String password) {
		this.Persona = Persona;
		this.Perfil = perfil;
		this.NombreUsuario = nombreUsuario;
		this.Password = password;
		this.Activo = true;
	}

	//Implementar serializable
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IdUsuario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int IdUsuario;
	
	
	@Column(name="NombreUsuario", unique=true)
	private String NombreUsuario;
	
	@Column(name="Password")
	private String Password;
	
	@Column(name="Activo")
	private Boolean Activo;

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name="IdPersona")
	private Persona Persona = new Persona();
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="IdPerfil")
	private Perfil Perfil;



	public Usuario()
	{
		
	}
	

	public void setPerfil(Perfil Perfil) {
		this.Perfil = Perfil;
	}
	public Perfil getPerfil() {
		return this.Perfil;
	}
	
	public int getIdUsuario() {
		return IdUsuario;
	}

	public void setPersona(Persona Persona) {
		this.Persona = Persona;
	}
	public Persona getPersona() {
		return this.Persona;
	}

	public String getNombreUsuario() {
		return NombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		NombreUsuario = nombreUsuario;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	
	

}
