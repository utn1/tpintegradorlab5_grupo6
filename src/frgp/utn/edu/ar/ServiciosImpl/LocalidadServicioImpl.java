//package ServiciosImpl;
//
//public class LocalidadServicioImpl {
//
//}
package frgp.utn.edu.ar.ServiciosImpl;



import java.util.List;

import frgp.utn.edu.ar.Dao.LocalidadDao;
import frgp.utn.edu.ar.Servicios.LocalidadServicio;
import frgp.utn.edu.ar.entidad.Localidad;
import frgp.utn.edu.ar.entidad.Persona;


public class LocalidadServicioImpl implements LocalidadServicio {

	private LocalidadDao dataAccess = null;

	public void setDataAccess(LocalidadDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public Localidad obtenerLocalidad(Integer CP) {

			return this.dataAccess.obtenerLocalidad(CP);

	}

	@Override
	public List<Localidad> obtenerTodos() {

			return this.dataAccess.obtenerTodos();

	}
	
	@Override
	public boolean InsertarLocalidad(Localidad loc) {
			return	this.dataAccess.InsertarLocalidad(loc);
	}

}