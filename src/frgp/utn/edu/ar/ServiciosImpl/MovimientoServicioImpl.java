package frgp.utn.edu.ar.ServiciosImpl;

import java.util.Date;
import java.util.List;

import frgp.utn.edu.ar.Dao.MovimientoDao;

import frgp.utn.edu.ar.Servicios.MovimientoServicio;

import frgp.utn.edu.ar.entidad.Movimiento;



public class MovimientoServicioImpl implements MovimientoServicio {

	private MovimientoDao dataAccess = null;

	public void setDataAccess(MovimientoDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public  List<Movimiento> obtenerMovimientosPorIdCuenta(int idCuenta) {

			return this.dataAccess.obtenerMovimientosPorIdCuenta(idCuenta);

	}

	@Override
	public List<Movimiento> obtenerTodos() {

			return this.dataAccess.obtenerTodos();

	}
	
	@Override
	public boolean insertarMovimiento(Movimiento mov) {
			return	this.dataAccess.insertarMovimiento(mov);
	}

	@Override
	public  List<Movimiento> obtenerMovimientosPorIdCuentaDesdeHasta(int id,Date desde,Date hasta) {
			return	this.dataAccess.obtenerMovimientosPorIdCuentaDesdeHasta(id,desde,hasta);
	}

	
	
}