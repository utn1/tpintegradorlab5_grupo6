package frgp.utn.edu.ar.Servicios;

import java.util.List;

import frgp.utn.edu.ar.entidad.EstadoPrestamo;

public interface EstadoPrestamoServicio {

	 List<EstadoPrestamo> obtenerTodos();
	 EstadoPrestamo obtenerEstadoPrestamo(Integer idEstadi);
	 boolean InsertarEstadoPrestamo(EstadoPrestamo estado);
}