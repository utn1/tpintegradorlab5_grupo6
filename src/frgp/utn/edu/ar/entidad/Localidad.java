package frgp.utn.edu.ar.entidad;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Localidad")
public class Localidad implements Serializable {
	//Implementar serializable
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CP")
	private int CP;
	
	@Column(name="Perfil")
	private String Localidad;

	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="idProvincia")
	private Provincia provincia;
	
	@Column(name="Activo")
	private Boolean Activo;
	
	public Localidad() {
		
	}


	public Localidad(int cP, String localidad, Boolean activo) {
		super();
		CP = cP;
		Localidad = localidad;
		Activo = activo;
	}



	public int getCP() {
		return CP;
	}
	public void setCP(int cP) {
		CP = cP;
	}
	public String getLocalidad() {
		return Localidad;
	}
	public void setLocalidad(String localidad) {
		Localidad = localidad;
	}
	
	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	public Provincia getProvincia() {
		return provincia;
	}


	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	@Override
	public String toString() {
		return "Localidad [CP=" + CP + ", Localidad=" + Localidad + "]";
	}
}
