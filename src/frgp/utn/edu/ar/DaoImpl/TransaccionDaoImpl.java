package frgp.utn.edu.ar.DaoImpl;

import frgp.utn.edu.ar.Dao.TransaccionDao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.entidad.*;


public class TransaccionDaoImpl implements TransaccionDao {

	@Autowired
	private Conexion<Movimiento> cn;
	@Autowired
	private Conexion<Transaccion> cnTr;
	private Conexion<Cuota> cnCt;
	
	public TransaccionDaoImpl()
	{
		cn = new Conexion<Movimiento>();
		cnTr = new Conexion<Transaccion>();
		cnCt = new Conexion<Cuota>();
	}

	@Override
	public TipoMovimiento obtenerTipoMovimiento(String codigo) {
		System.out.println("dao->obtenerTipoMovimiento->init");
		 
		Session session = cn.abrirConexion();
		String hql = "from TipoMovimiento where codigo='" + codigo + "'";
		TipoMovimiento obj = (TipoMovimiento)session.createQuery(hql).uniqueResult();
		System.out.println("dao->obtenerTipoMovimiento->end");
		cn.cerrarSession();
		return obj;
	}
	
	@Override
	public EstadoPrestamo obtenerEstadoPrestamo(String codigo) {
		System.out.println("dao->obtenerEstadoPrestamo->init");
		 
		Session session = cn.abrirConexion();
		String hql = "from EstadoPrestamo where codigo='" + codigo + "'";
		EstadoPrestamo obj = (EstadoPrestamo)session.createQuery(hql).uniqueResult();
		System.out.println("dao->obtenerEstadoPrestamo->end");
		cn.cerrarSession();
		return obj;		
	}
	
	@Override
	public boolean insertarAltaCuenta(AltaCuenta altaCuenta) {
		try {
			cnTr.add(altaCuenta);
			return true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			return false;
		}
	}
	
	@Override
	public boolean insertarMovimiento(Movimiento movimiento) {
		try {
			cn.add(movimiento);
			return true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			return false;
		}
	}
	
	@Override
	public boolean insertarTransferencia(Transferencia transferencia) {
		boolean resp;
		try {
			cnTr.add(transferencia);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;
	}

	@Override
	public boolean insertarPrestamo(Prestamo prestamo) {
		boolean resp;
		try {
			cnTr.add(prestamo);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;
	}

	@Override
	public boolean actualizarPrestamo(Prestamo prestamo) {
		boolean resp;
		try {
			cnTr.update(prestamo);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;
	}

	@Override
	public boolean insertarCuota(Cuota cuota) {
		boolean resp;
		try {
			cnCt.add(cuota);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;		
	}
	
	@Override
	public boolean insertarCuotas(ArrayList<Cuota> cuotas) {
		boolean resp;
		try {
			cnCt.addAll(cuotas);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;
	}
	
	@Override
	public boolean actualizarCuota(Cuota cuota) {
		boolean resp;
		try {
			cnCt.update(cuota);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;		
	}
	
	@Override
	public ArrayList<Transferencia> obtenerTransferenciasCuentaOrigen(int idCuenta) {
		System.out.println("dao->obtenerTransferenciasCuentaOrigen->init");
		 
		Session session = cn.abrirConexion();
		String hql = "from Transferencia where CuentaOrigen='" + idCuenta + "'";
		ArrayList<Transferencia> obj = (ArrayList<Transferencia>)session.createQuery(hql).list();
		System.out.println("dao->obtenerTransferenciasCuentaOrigen->end");
		cn.cerrarSession();
		return obj;		
	}
	
	@Override
	public ArrayList<Prestamo> obtenerPrestamosPersonaId(int idPersona){
		System.out.println("dao->obtenerTransferenciasCuentaOrigen->init");
		 
		Session session = cn.abrirConexion();
		String hql = "from Prestamo where Cliente='" + idPersona + "'";
		ArrayList<Prestamo> obj = (ArrayList<Prestamo>)session.createQuery(hql).list();
		System.out.println("dao->obtenerPrestamosPersonaId->end");
		cn.cerrarSession();
		return obj;
	}
	
	@Override
	public ArrayList<Cuota> obtenerCuotasPrestamo(int idPrestamo){
		System.out.println("dao->obtenerCuotasPrestamo->init");
		 
		Session session = cn.abrirConexion();
		String hql = "from Cuota where prestamo='" + idPrestamo + "' order by NroCuota asc";
		ArrayList<Cuota> obj = (ArrayList<Cuota>)session.createQuery(hql).list();
		System.out.println("dao->obtenerCuotasPrestamo->end");
		cn.cerrarSession();
		return obj;		
	}
	
	@Override
	public Cuota obtenerCuota(int idCuota) {
		System.out.println("dao->obtenerCuota->init");
		 
		Session session = cn.abrirConexion();
		String hql = "from Cuota where Id='" + idCuota + "'";
		Cuota obj = (Cuota)session.createQuery(hql).uniqueResult();
		System.out.println("dao->obtenerCuota->end");
		cn.cerrarSession();
		return obj;		
	}
	
	@Override
	public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta){
		Session session = cn.abrirConexion();
		String hql = "from Prestamo";
		List<Prestamo> aux = (List<Prestamo>) session.createQuery(hql).list();
		List<Prestamo> tu = new ArrayList<Prestamo>();
		for(Prestamo p : aux) {
			if(p.getCuentaDestino().getId() == idCuenta) {
				tu.add(p);
			}
		}
		cn.cerrarSession();
		return tu;		
	}

}
