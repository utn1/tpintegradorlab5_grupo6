package frgp.utn.edu.ar.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="Perfil")
public class Perfil implements Serializable{


	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IdPerfil")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int IdPerfil;
	
	@Column(name="Perfil")
	private String Perfil;
	
	@Column(name="Activo")
	private Boolean Activo;
	



	public Perfil(int idPerfil, String perfil, Boolean activo) {
		super();
		IdPerfil = idPerfil;
		Perfil = perfil;
		Activo = activo;
	}

	//Constructor vacio
	public Perfil()
	{
		
	}

	public String getPerfil() {
		return Perfil;
	}
	
	public int getIdPerfil() {
		return IdPerfil;
	}

	public void setPerfil(String perfil) {
		this.Perfil = perfil;
	}

	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	@Override
	public String toString() {
		return "perfil [perfil=" + Perfil + "]";
	}
	
}
