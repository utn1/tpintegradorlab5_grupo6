package frgp.utn.edu.ar.DaoImpl;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.Dao.PersonaDao;

import frgp.utn.edu.ar.entidad.Persona;

public class PersonaDaoImpl  implements PersonaDao {

	@Autowired
	private Conexion<Persona> cn;
	
	public PersonaDaoImpl()
	{
		cn = new Conexion<Persona>();
	}

	@Override
	public boolean insertarPersona(Persona persona) {		
		try {
			cn.add(persona);
			return true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception"); // Entra en este catch
			return false;
		}
	}

	@Override
	public boolean actualizarPersona(Persona persona) {
		System.out.println("actualizarPersona:" + persona);
		boolean resp;
		try {
			cn.update(persona);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception"); // Entra en este catch
			resp = false;
		}
		return resp;
	}

	@Override
	public boolean actualizarPersona2(Persona persona) {
		boolean resp;
		System.out.println("actualizarPersona2");
		//Configuration configuration = new Configuration();
	    //configuration.configure();
	    //ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
	  //  SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	    Session session=sessionFactory.openSession();
	    try {
	    	session.beginTransaction();
	    	session.update(persona);
	    	session.getTransaction().commit();
	    	resp = true;
	    }
	    catch(Exception e) {
	    	System.out.println("Error al actualizar persona2: " + e.getMessage());
	    	resp = false;
	    }
        cn.cerrarSession();
        sessionFactory.close();
	    return resp;
	}

	@Override
	public Persona getPersonaById(Integer id) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Persona where IdPersona='" + id  + "' and Activo = 1"; 
		Persona tu = (Persona)session.createQuery(hql).uniqueResult();
		
		cn.cerrarSession();
		return tu;
	}

	public Persona obtenerPersonaPorDni(String dni) {
		 
		Session session = cn.abrirConexion();
		String hql = "from Persona where Dni='" + dni  + "' and Activo = 1";
		Persona obj = (Persona)session.createQuery(hql).uniqueResult();
		cn.cerrarSession();
		return obj;
	}

}
