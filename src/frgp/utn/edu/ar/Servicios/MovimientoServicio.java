package frgp.utn.edu.ar.Servicios;

import java.util.Date;
import java.util.List;

import frgp.utn.edu.ar.entidad.Movimiento;

public interface MovimientoServicio {
	 boolean insertarMovimiento(Movimiento movimiento);
	 List<Movimiento> obtenerTodos();
	 List<Movimiento> obtenerMovimientosPorIdCuenta(int id);
	 List<Movimiento> obtenerMovimientosPorIdCuentaDesdeHasta(int id,Date desde,Date hasta);
}
