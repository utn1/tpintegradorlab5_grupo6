package frgp.utn.edu.ar.ServiciosImpl;

import frgp.utn.edu.ar.Servicios.TransaccionServicio;

import java.util.ArrayList;
import java.util.List;

import frgp.utn.edu.ar.Dao.TransaccionDao;

import frgp.utn.edu.ar.entidad.*;

public class TransaccionServicioImpl implements TransaccionServicio{
	private TransaccionDao dataAccess = null;

	public void setDataAccess(TransaccionDao dataAccess) {
		this.dataAccess = dataAccess;
	}
	
	@Override
	public TipoMovimiento obtenerTipoMovimiento(String codigo) {
		System.out.println("srv->obtenerTipoMovimiento->init");
		return this.dataAccess.obtenerTipoMovimiento(codigo);
	}
	
	@Override
	public EstadoPrestamo obtenerEstadoPrestamo(String codigo) {
		System.out.println("obtenerEstadoPrestamo");
		return this.dataAccess.obtenerEstadoPrestamo(codigo);
	}
	
	@Override
	public boolean insertarMovimiento(Movimiento movimiento) {
		return this.dataAccess.insertarMovimiento(movimiento);
	}

	@Override
	public boolean insertarAltaCuenta(AltaCuenta altaCuenta) {
		return this.dataAccess.insertarAltaCuenta(altaCuenta);
	}
	
	@Override
	public boolean insertarTransferencia(Transferencia transferencia) {
		return this.dataAccess.insertarTransferencia(transferencia);
	}
	
	@Override
	public boolean insertarPrestamo(Prestamo prestamo) {
		return this.dataAccess.insertarPrestamo(prestamo);
	}
	
	@Override
	public boolean actualizarPrestamo(Prestamo prestamo) {
		return this.dataAccess.actualizarPrestamo(prestamo);
	}
	
	@Override
	public boolean insertarCuota(Cuota cuota) {
		return this.dataAccess.insertarCuota(cuota);
	}
	
	@Override
	public boolean actualizarCuota(Cuota cuota) {
		return this.dataAccess.actualizarCuota(cuota);
	}
	
	@Override
	public boolean insertarCuotas(ArrayList<Cuota> cuotas) {
		return this.dataAccess.insertarCuotas(cuotas);
	}

	@Override
	public ArrayList<Transferencia> obtenerTransferenciasCuentaOrigen(int idCuenta) {
		System.out.println("srv->obtenerTransferenciasCuentaOrigen->init");
		return this.dataAccess.obtenerTransferenciasCuentaOrigen(idCuenta);
	}
	
	@Override
	public ArrayList<Prestamo> obtenerPrestamosPersonaId(int idPersona){
		System.out.println("srv->obtenerPrestamosPersonaId->init");
		return this.dataAccess.obtenerPrestamosPersonaId(idPersona);
	}
	
	@Override
	public ArrayList<Cuota> obtenerCuotasPrestamo(int idPrestamo){
		System.out.println("srv->obtenerCuotasPrestamo->init");
		return this.dataAccess.obtenerCuotasPrestamo(idPrestamo);
	}
	
	@Override
	public Cuota obtenerCuota(int idCuota) {
		System.out.println("srv->obtenerCuotasPrestamo->init");
		return this.dataAccess.obtenerCuota(idCuota);
	}
	
	@Override
	public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta){
		return this.dataAccess.obtenerPrestamoPorCuenta(idCuenta);
	}

}
