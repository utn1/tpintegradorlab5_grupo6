package frgp.utn.edu.ar.Dao;

import java.util.Date;
import java.util.List;


import frgp.utn.edu.ar.entidad.Movimiento;

public interface MovimientoDao {
	public boolean insertarMovimiento(Movimiento movimiento);
	public List<Movimiento> obtenerTodos();
	public List<Movimiento> obtenerMovimientosPorIdCuenta(int id);
	public List<Movimiento> obtenerMovimientosPorIdCuentaDesdeHasta(int id,Date Desde,Date Hasta);
}
