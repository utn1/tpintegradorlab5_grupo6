package frgp.utn.edu.ar.Servicios;


import java.util.List;

import frgp.utn.edu.ar.entidad.Usuario;

public interface UsuarioServicio {

	//Alta de Usuario
	 boolean insertarUsuario(Usuario Usuario);
	 boolean UpdatearUsuario(Usuario Usuario);
	 Usuario getUserByUsernameAndPassword(String User,String Pass);
	 Usuario getUserByUsername(String User);
	 List<Usuario> obtenerClientes();
	 Usuario getUserById(int id);
	 boolean borradoLogico(String id);
}