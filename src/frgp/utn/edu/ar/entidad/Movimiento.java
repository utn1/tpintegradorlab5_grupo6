package frgp.utn.edu.ar.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
@Table(name="Movimiento")
public class Movimiento implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int IdMovimiento;

	//@ManyToOne(cascade={CascadeType.ALL})
	@ManyToOne
	@JoinColumn(name="IdTipoMovimiento")
	private TipoMovimiento tipoMovimiento;

	@Column(name="FechaAlta")
	private Date FechaAlta;

	@Column(name="DetalleMovimiento")
	private String DetalleMovimiento;

	@Column(name="Importe")
	private Double Importe;
	
	//@ManyToOne(cascade={CascadeType.ALL})
	@ManyToOne
	@JoinColumn(name="idCuenta")
	private Cuenta Cuenta;

	public Double getImporte() {
		return Importe;
	}

	public void setImporte(Double importe) {
		Importe = importe;
	}

	public Cuenta getCuenta() {
		return Cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		Cuenta = cuenta;
	}

	public int getIdMovimiento() {
		return IdMovimiento;
	}	

	public void setIdMovimiento(int idMovimiento) {
		IdMovimiento = idMovimiento;
	}

	public TipoMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public Date getFechaAlta() {
		return FechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		FechaAlta = fechaAlta;
	}

	public String getDetalleMovimiento() {
		return DetalleMovimiento;
	}

	public void setDetalleMovimiento(String detalleMovimiento) {
		DetalleMovimiento = detalleMovimiento;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
