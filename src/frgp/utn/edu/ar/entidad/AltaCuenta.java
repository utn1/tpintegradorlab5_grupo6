package frgp.utn.edu.ar.entidad;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
@Table(name="AltaCuenta")
public class AltaCuenta implements Transaccion{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@OneToOne
	@JoinColumn(name="Cuenta")
	private Cuenta Cuenta;
	
	@Column(name="Monto")
	private double Monto;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Cuenta getCuenta() {
		return Cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		Cuenta = cuenta;
	}

	public double getMonto() {
		return Monto;
	}

	public void setMonto(double monto) {
		Monto = monto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
