<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="<c:url value="/resources/css/contact_styles.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"
	type="text/css" rel="stylesheet">
		<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">
	<style type="text/css">
	
/* table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{ */
/*  padding-right: 30px; */
/*  background: coral; */
/* } */
	</style>

<%-- <link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />" type="text/css" rel="stylesheet"> --%>

<!-- Bootstrap Date-Picker Plugin -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<!-- <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script> -->



<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>

 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<script type="text/javascript">
	$(document).ready(function($) {


			debugger;
			$('#example').DataTable(
			{
				"language" : {
					"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});
		});
</script>
</head>

<body>

	<div class="super_container">

	<header class="header trans_300">

			 <%@include file="Menu.jsp"%>
	</header>
		<div class="container contact_container">
			<div class="row">
				<div class="col-lg-6 get_in_touch_col">
					<div class="get_in_touch_contents">
						<h1>Listado de Cuotas</h1>
							<div>
								<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
								<table id="example" class="table table-striped table-bordered" style="width:100%">
								 <thead>
								<tr>
									<td>Nro Cuota</td>
									<td>Fecha Vencimiento</td>
									<td>Monto</td>
									<td>Fecha Pago</td>
									<td>Acciones</td>
								</tr>
								 </thead>
						     <tbody>
								<c:forEach var="cuota" items="${cuotas}">
									<tr>
										<td>${cuota.getNroCuota()}</td>
										<td>${cuota.getFechaVencimiento()}</td>
										<td>${cuota.getMonto()}</td>
										<td>${cuota.getFechaPago()}</td>
										<td>
										<c:if test = "${cuota.getFechaPago()==null}">
										<form method="get" action="redirigir_PagarCuotaSeleccionCuenta.html">
											<button id="btnSubmit" type="submit"
												name="btnIdCuota"
												value="${cuota.getId()}" style="height: 60px; width: 60px">
												Pagar Cuota
											</button>
										</form>
								     </c:if>

										</td>
									</tr>
									</c:forEach>
									 </tbody>
								</table>
							</div>
					</div>
					<form method="get" action="procesar_accion_cliente.html?btnSubmit=listadoPrestamos">
						<button id="review_submit" type="submit" name="btnSubmit" value="listadoPrestamos" class="red_button message_submit_btn trans_300" style="background-color:red">VOLVER</button>
					</form>
					

				</div>
			</div>
		</div>
	</div>

<%@include file="Footer.jsp"%>

	

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="styles/bootstrap4/popper.js"></script>
	<script src="styles/bootstrap4/bootstrap.min.js"></script>
	<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
	<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="plugins/easing/easing.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>
