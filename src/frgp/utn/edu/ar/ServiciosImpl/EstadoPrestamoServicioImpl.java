//package ServiciosImpl;
//
//public class LocalidadServicioImpl {
//
//}
package frgp.utn.edu.ar.ServiciosImpl;



import java.util.List;

import frgp.utn.edu.ar.Dao.EstadoPrestamoDao;
import frgp.utn.edu.ar.Servicios.EstadoPrestamoServicio;
import frgp.utn.edu.ar.entidad.EstadoPrestamo;
import frgp.utn.edu.ar.entidad.Persona;


public class EstadoPrestamoServicioImpl implements EstadoPrestamoServicio {

	private EstadoPrestamoDao dataAccess = null;

	public void setDataAccess(EstadoPrestamoDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public EstadoPrestamo obtenerEstadoPrestamo(Integer idEstado) {

			return this.dataAccess.obtenerEstadoPrestamo(idEstado);

	}

	@Override
	public List<EstadoPrestamo> obtenerTodos() {

			return this.dataAccess.obtenerTodos();

	}
	
	@Override
	public boolean InsertarEstadoPrestamo(EstadoPrestamo estado) {
			return	this.dataAccess.InsertarEstadoPrestamo(estado);
	}

}