<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/contact_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />" type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">

<script type="text/javascript">
	$(document).ready(
			function($) {
				debugger;
				$('#example').DataTable(
				{
					"language" : {
						"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
					}
				});
			    $(".onlyNumber").on("keypress keyup blur", function (event) { //Clase onlyNumber para inputs que reconozcan solo texto numerico
			        $(this).val($(this).val().replace(/[^\d].+/, ""));
			        if ((event.which < 48 || event.which > 57)) {
			            event.preventDefault();
			        }
			    });
			});
	
	$(document).ready(
			function($) {
				debugger;
				$('#example').DataTable(
				{
					"language" : {
						"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
					}
				});
			    $(".onlyLetter").on("keypress keyup blur", function (event) {
			        $(this).val($(this).val().replace(/[^\d].+/, ""));
			        if ( (event.which < 65 || event.which > 90) && (event.which < 97 || event.which > 122) ) {
			            event.preventDefault();
			        }
			    });
			});
</script>


</head>

<body>

<div class="super_container">

	<!-- Header -->

		<header class="header trans_300">
		 <%@include file="Menu.jsp"%>
		</header>
	<div class="container contact_container">
		<div class="row">
			<div class="col-lg-6 get_in_touch_col">
				<div class="get_in_touch_contents">
					<h1>Modificacion Cuenta</h1>
					<form method="get" action="procesar_ModificarCuenta.html">

						<div style="float">
							<p>Descripcion:</p>
							<input id="txtDescripcion" value="${descripcion}" class="form_input input_name input_ph" type="text" name="txtDescripcion" required="required" >
						</div>

						<div style="float">
							<p>CBU:</p>
							<input id="txtCBU" readonly="true" value="${cbu}"class="form_input input_name input_ph onlyLetter" type="text" name="txtCBU" required="required" >
						</div>

						<div style="float">
							<p>Nombre cuenta:</p>
							<input id="txtNombreCuenta" value="${nombreCuenta}" class="form_input input_name input_ph onlyLetter" type="text" name="txtNombreCuenta" required="required" >
						</div>

						<div>
							<button id="review_submit" type="submit" name="btnSubmit" value="${idCuenta}" class="red_button message_submit_btn trans_300">Aceptar</button>
						</div>
					</form>
					
					<form method="post" action="redirigir_HomePage.html">
  						</div>
							<button id="review_submit" type="submit" class="red_button message_submit_btn trans_300" value="Submit">Volver</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>


 <%@include file="Footer.jsp"%>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>
</body>

</html>
