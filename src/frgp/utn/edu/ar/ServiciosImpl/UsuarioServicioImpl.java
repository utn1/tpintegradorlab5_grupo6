package frgp.utn.edu.ar.ServiciosImpl;


import java.util.List;
import frgp.utn.edu.ar.Dao.UsuarioDao;
import frgp.utn.edu.ar.Servicios.UsuarioServicio;
import frgp.utn.edu.ar.entidad.Usuario;

public class UsuarioServicioImpl implements UsuarioServicio {

	private UsuarioDao dataAccess = null;

	public void setDataAccess(UsuarioDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public boolean insertarUsuario(Usuario usu) {

			return this.dataAccess.insertarUsuario(usu);
	}
	@Override
	public boolean UpdatearUsuario(Usuario usu) {

			return this.dataAccess.UpdatearUsuario(usu);
	}
	
	
	@Override
	public Usuario getUserByUsernameAndPassword(String User,String Pass) {
		System.out.println("srv->getUserByUsernameAndPassword:" + User + "/" + Pass);
		System.out.println("srv->getUserByUsernameAndPassword.dataAccess:" + this.dataAccess);
		return this.dataAccess.getUserByUsernameAndPassword(User, Pass);
	}
	@Override
	public Usuario getUserByUsername(String User) {
		return this.dataAccess.getUserByUsername(User);
	}

	@Override
	public List<Usuario> obtenerClientes() {

			return this.dataAccess.obtenerClientes();
	}
	
	@Override
	public Usuario getUserById(int id) {

			return this.dataAccess.getUserById(id);
	}
	
	@Override
	public boolean borradoLogico(String id) {
		return this.dataAccess.borradoLogico(id);
	}

}