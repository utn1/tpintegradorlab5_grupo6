package frgp.utn.edu.ar.DaoImpl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.Dao.MovimientoDao;
import frgp.utn.edu.ar.entidad.Movimiento;


public class MovimientoDaoImpl implements MovimientoDao{
	@Autowired
	private Conexion<Movimiento> cn;
	
	public MovimientoDaoImpl()
	{
		cn = new Conexion<Movimiento>();
	}

	@Override
	public boolean insertarMovimiento(Movimiento movimiento) {
		try {
			cn.add(movimiento);
			return true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			return false;
		}
	}
	
	@Override
	public List<Movimiento> obtenerTodos() {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Movimiento"; //TODO Agregar el activo
		List<Movimiento> tu = (List<Movimiento>)session.createQuery(hql).list();
		
		cn.cerrarSession();
		return tu;
	}
	
	
	@Override
	public List<Movimiento> obtenerMovimientosPorIdCuenta(int id) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Movimiento where idCuenta='" + id + "'"; //TODO Agregar el activo
		List<Movimiento> tu = (List<Movimiento>)session.createQuery(hql).list();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public List<Movimiento> obtenerMovimientosPorIdCuentaDesdeHasta(int id,Date desde,Date hasta) {
		
		 
		Session session = cn.abrirConexion();
		//String hql = "from Movimiento where idCuenta= ? and FechaAlta between ? and ?"; //TODO Agregar el activo
		String hql = "from Movimiento where idCuenta= :id and FechaAlta> :desde and FechaAlta<:hasta"; //TODO Agregar el activo
		
		//Por que en la base de datos se guarda con utc 0
//		desde=addHoursToJavaUtilDate(desde,3);
//		hasta=addHoursToJavaUtilDate(hasta,3);
		//List<Movimiento> tu = (List<Movimiento>)session.createQuery(hql).setInteger(0, id).setDate(1, desde).setDate(2, hasta).list();
		List<Movimiento> tu = (List<Movimiento>)session.createQuery(hql).setParameter("id",id)
.setParameter("desde",desde)
 .setParameter("hasta",hasta).list();
		
		cn.cerrarSession();
		return tu;
	}
	
	public Date addHoursToJavaUtilDate(Date date, int hours) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.add(Calendar.HOUR_OF_DAY, hours);
	    return calendar.getTime();
	}
	

}
