package frgp.utn.edu.ar.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.Servicios.*;
import frgp.utn.edu.ar.entidad.*;
import frgp.utn.edu.ar.resources.Config;

@Controller
public class UsuarioController {
	//
	// @Autowired
	public PersonaServicio servicePersona;
	// @Autowired
	public LocalidadServicio serviceLocalidad;

	public UsuarioServicio serviceUsuario;
	
	public PerfilServicio servicePerfil;

	public CuentaServicio serviceCuenta;
	
	public TransaccionServicio serviceTransaccion;
	
	private double dolarCompra = 80;
	private double dolarVenta = 140;

	@RequestMapping("Login.html")
	public ModelAndView eventoRedireccionarLogin() {
		ModelAndView MV = new ModelAndView();
		MV.setViewName("Login");
		return MV;
	}
	
	@RequestMapping("listadoClientes.html")
	public ModelAndView listadoClientes(HttpServletRequest request) {
		System.out.println("listadoClientes->init");
		ModelAndView MV = new ModelAndView();
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("serviceUsuarioBean");
		Usuario u = (Usuario) appContext.getBean("BeanUsuario");

		ArrayList<Usuario> usuarios = (ArrayList<Usuario>) this.serviceUsuario.obtenerClientes();

		MV.addObject("listaUsuarios", usuarios);
		
		MV.setViewName("ListadoClientes");
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}
	
	@RequestMapping("EliminarCliente.html")
	public ModelAndView EliminarCliente(String idCliente, HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		try {
			System.out.println("idCliente:" + idCliente);
			ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
			this.serviceUsuario = (UsuarioServicio) appContext.getBean("serviceUsuarioBean");
			this.serviceUsuario.borradoLogico(idCliente);
			((ConfigurableApplicationContext)(appContext)).close();
			MV.setViewName("EliminacionClienteOk");
		}
		catch(Exception e) {
			System.out.println("Error al eliminar usuario: " + e);
			MV.setViewName("EliminacionClienteErr");
		}
		return MV;
	}

	@RequestMapping("ModificarCliente.html")
	public ModelAndView modificarCliente(String idCliente, HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		System.out.println("modificarCliente->idCliente:" + idCliente);
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		Usuario usu = (Usuario) appContext.getBean("BeanUsuario");
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("serviceUsuarioBean");
		int auxId = Integer.parseInt(idCliente);
		usu = this.serviceUsuario.getUserById(auxId);
		
		this.serviceLocalidad = (LocalidadServicio) appContext.getBean("serviceLocalidadBean");
		ArrayList<Localidad> listaLoc = (ArrayList<Localidad>)this.serviceLocalidad.obtenerTodos();

		Persona per = usu.getPersona();
		MV.addObject("nombre", per.getNombre());
		MV.addObject("apellido", per.getApellido());
		MV.addObject("dni", per.getDni());
		MV.addObject("fechaNac", per.getFechaNac());
		MV.addObject("direccion", per.getDireccion());
		MV.addObject("direccionNro", per.getDireccionNro());
		MV.addObject("mail", per.getMail());
		MV.addObject("cuil", per.getCuil());
		MV.addObject("codigo",usu.getNombreUsuario());
		MV.addObject("password", usu.getPassword());
		MV.addObject("cp", per.getLocalidad()==null?-1:per.getLocalidad().getCP());
		MV.addObject("sexo", per.getSexo());
		MV.addObject("usuario", usu);
		MV.addObject("idUsuarioEdit", usu.getIdUsuario());
		//MV.addObject("localidades", listaLoc);
		MV.addObject("listaLocalidades", listaLoc);
		
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		
		String filtroFechaNac = simpleDateFormat.format(per.getFechaNac()==null?new java.util.Date():per.getFechaNac());
		MV.addObject("fechaNacDatePicker",filtroFechaNac);
		
		MV.setViewName("EditUsuario");
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	/*
	@RequestMapping("procesar_ModificacionCliente.html")
	public procesarModificacionCliente(HttpServletRequest request) {
		System.out.println("listadoClientes->init");
		ModelAndView MV = new ModelAndView();
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);

	}
	
	@RequestMapping("ProcesarModificacionCliente.html")
	public ModelAndView procesarModificacionCliente(HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		System.out.println("procesarModificacionCliente");
		
		return MV;
	}
	*/

	@RequestMapping("HomePageAdmin.html")
	public ModelAndView homePageAdmin(HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		MV.setViewName("HomePageAdmin");
		return MV;
	}

	
	@RequestMapping("HomePageCliente.html")
	public ModelAndView HomePageCliente(HttpServletRequest request) {

		HttpSession misession = request.getSession(true);
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);

		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");

		this.servicePersona = (PersonaServicio) appContext.getBean("personaServicioImpl");
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");

		Usuario usuario = (Usuario) appContext.getBean("usuario");
		int idUsuario = (int)misession.getAttribute("SessionIdUsuario");
		System.out.println("##SessionIdUsuario:" + idUsuario);
		misession.setAttribute("SessionIdUsuario", idUsuario);
		usuario=(Usuario)this.serviceUsuario.getUserById(idUsuario);
		List<Cuenta>  listaCuentas=(List<Cuenta>)this.serviceCuenta.obtenerCuentasActivasPorUsuario(usuario.getPersona().getIdPersona());

		MV.addObject("listaCuentas", listaCuentas);

		MV.setViewName("HomePageCliente");
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("redirigir_AddEditUsuario.html")
	public ModelAndView eventoRedireccionarAddEditUsuario(HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();

		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		this.servicePersona = (PersonaServicio) appContext.getBean("servicePersonaBean");
		this.serviceLocalidad = (LocalidadServicio) appContext.getBean("serviceLocalidadBean");
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("serviceUsuarioBean");
		this.servicePerfil = (PerfilServicio) appContext.getBean("servicePerfilBean");

		//Localidad loc = (Localidad) appContext.getBean("BeanLocalidad");
		//Persona p = (Persona) appContext.getBean("BeanPersona");
		//Usuario u = (Usuario) appContext.getBean("BeanUsuario");
		//Perfil perfilCliente = (Perfil) appContext.getBean("BeanPerfil");

		ArrayList<Localidad> listaLoc = (ArrayList<Localidad>)this.serviceLocalidad.obtenerTodos();
		ArrayList<Perfil> listaPerfil =(ArrayList<Perfil>)this.servicePerfil.obtenerTodos();

		MV.addObject("nombre", "");
		MV.addObject("apellido", "");
		MV.addObject("dni", "");
		MV.addObject("fechaNac", "");
		MV.addObject("direccion", "");
		MV.addObject("direccionNro", "");
		MV.addObject("mail", "");
		MV.addObject("cuil", "");
		MV.addObject("codigo", "");
		MV.addObject("password", "");

		MV.addObject("listaLocalidades", listaLoc);
		MV.addObject("listaPerfil", listaPerfil);
		MV.setViewName("AddEditUsuario");
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("procesar_accion_cliente.html")
	public ModelAndView procesarAccionCliente(String btnSubmit, HttpServletRequest request) {
		System.out.println("procesarAccionCliente.btnSubmit:" + btnSubmit);
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		try {
			this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");
			this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
			this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
			HttpSession misession = request.getSession(true);
			int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
			System.out.println("##SessionIdUsuario:" + idUsuario);
			misession.setAttribute("SessionIdUsuario", idUsuario);
			Usuario usuario = this.serviceUsuario.getUserById(idUsuario);
			ArrayList<Cuenta> cuentas = (ArrayList<Cuenta>) this.serviceCuenta.obtenerCuentaPorUsuario(idUsuario);
			ArrayList<ArrayList<Transferencia>> transferencias = new ArrayList<ArrayList<Transferencia>>();
			ArrayList<Prestamo> prestamos = new ArrayList<Prestamo>();

			List<Cuenta> listaCuentasUsuario = this.serviceCuenta.obtenerCuentasActivasPorUsuario(idUsuario);
			if(btnSubmit.equals("nuevoPrestamo")) {
				
				if(listaCuentasUsuario.size()>0) //Valido que el usuario tenga cuenta
				{
					MV.addObject("listaCuentasUsuario", listaCuentasUsuario);
					MV.setViewName("NuevoPrestamo");
	            }
				else
				{
					MV.setViewName("MsgErrorPrestamoSinCuenta");
				}
			
			}
			else if(btnSubmit.equals("nuevaTransferencia")) {
				if(listaCuentasUsuario.size()>0) //Valido que el usuario tenga cuenta
				{
					MV.setViewName("NuevaTransferencia");
	            }
				else
				{
					MV.setViewName("MsgErrorTransferenciaSinCuenta");
				}
				
			}
			else if(btnSubmit.equals("listadoPrestamos")) {
				prestamos = this.serviceTransaccion.obtenerPrestamosPersonaId(usuario.getPersona().getIdPersona());
				 
				ArrayList<Prestamo> prestamosNoCancelados = new ArrayList<Prestamo>();
				for(Prestamo p : prestamos) {
					if(!p.getEstado().getCodigo().equals("cancelado")) {
						prestamosNoCancelados.add(p);
					}
				}
				
				MV.addObject("prestamos", prestamosNoCancelados);
				MV.setViewName("ListadoPrestamos");
			}
			else if(btnSubmit.equals("listadoTransferencias")) {
				for(Cuenta cuenta : cuentas) {
					int cuentaId = cuenta.getId();
					System.out.println("cuentaId:" + cuentaId);
					transferencias.add(this.serviceTransaccion.obtenerTransferenciasCuentaOrigen(cuentaId));
					System.out.println("transferencias:" + transferencias);
				}
				MV.addObject("transferencias", transferencias);
				MV.setViewName("ListadoTransferenciasCliente");
			}
		}
		catch(Exception e) {
			System.out.println("Error procesarAccionCliente:" + e.getMessage());
			MV.addObject("mensajeError", e.getMessage());
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("GuardarUsuario.html")
	public ModelAndView GuardarUsuario(int txtPerfil, String txtApellido, String txtNombre,String txtDNI,Date txtFechaNac,String SelectLocalidad,String txtDireccion,String TxtDireccionNro, String txtSexo,String txtMail,String txtCuil,String txtCodigo,String txtPassword, String idUsuarioEdit,String btnSubmit, HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		boolean resp = false;
		boolean resp2 = false;

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		this.servicePersona = (PersonaServicio) appContext.getBean("servicePersonaBean");
		this.serviceLocalidad = (LocalidadServicio) appContext.getBean("serviceLocalidadBean");
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("serviceUsuarioBean");
		this.servicePerfil = (PerfilServicio) appContext.getBean("servicePerfilBean");

		Localidad loc = (Localidad) appContext.getBean("BeanLocalidad");
		Persona p = (Persona) appContext.getBean("BeanPersona");
		Persona p2 = (Persona) appContext.getBean("BeanPersona");
		Usuario u = (Usuario) appContext.getBean("BeanUsuario");
		Usuario u2 = (Usuario) appContext.getBean("BeanUsuario");
		Perfil perfilCliente = (Perfil) appContext.getBean("BeanPerfil");

	 
		
			loc= (Localidad)this.serviceLocalidad.obtenerLocalidad(Integer.parseInt(SelectLocalidad));
			p.setDni(Integer.parseInt(txtDNI));
			p.setNombre(txtNombre);
			p.setLocalidad(loc);
			p.setFechaNac(txtFechaNac);
			p.setApellido(txtApellido);
			p.setSexo(txtSexo);
			p.setMail(txtMail);
			p.setCuil(txtCuil);
			p.setDireccion(txtDireccion);
			p.setDireccionNro(TxtDireccionNro);
			p.setActivo(true);
			
			try {
				if(btnSubmit.equals("alta")) {
					p2 = this.servicePersona.obtenerPersonaPorDni(txtDNI);
					u2= this.serviceUsuario.getUserByUsername(txtCodigo);
					
					if(p2!=null) {
						MV.addObject("nombre", p.getNombre());
						MV.addObject("apellido", p.getApellido());
						MV.addObject("dni", p.getDni());
						MV.addObject("fechaNac", p.getFechaNac());
						MV.addObject("direccion", p.getDireccion());
						MV.addObject("direccionNro", p.getDireccionNro());
						MV.addObject("mail", p.getMail());
						MV.addObject("cuil", p.getCuil());
						MV.addObject("codigo",txtCodigo);
						MV.addObject("password", txtPassword);
						MV.addObject("cp", p.getLocalidad()==null?-1:p.getLocalidad().getCP());
						MV.addObject("sexo", p.getSexo());
						MV.addObject("idUsuarioEdit", idUsuarioEdit);
					

						ArrayList<Localidad> listaLoc = (ArrayList<Localidad>)this.serviceLocalidad.obtenerTodos();
						MV.addObject("listaLocalidades", listaLoc);
						
						String pattern = "yyyy-MM-dd HH:mm:ss";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
						String filtroFechaNac = simpleDateFormat.format(p.getFechaNac()==null?new java.util.Date():p.getFechaNac());
						MV.addObject("fechaNacDatePicker",filtroFechaNac);
						MV.addObject("DniDuplicado", true);
						MV.setViewName("AddEditUsuario");
						
						

					}
					else if(u2!=null) {
						
						MV.addObject("nombre", p.getNombre());
						MV.addObject("apellido", p.getApellido());
						MV.addObject("dni", p.getDni());
						MV.addObject("fechaNac", p.getFechaNac());
						MV.addObject("direccion", p.getDireccion());
						MV.addObject("direccionNro", p.getDireccionNro());
						MV.addObject("mail", p.getMail());
						MV.addObject("cuil", p.getCuil());
						MV.addObject("codigo",txtCodigo);
						MV.addObject("password", txtPassword);
						MV.addObject("cp", p.getLocalidad()==null?-1:p.getLocalidad().getCP());
						MV.addObject("sexo", p.getSexo());

						ArrayList<Localidad> listaLoc = (ArrayList<Localidad>)this.serviceLocalidad.obtenerTodos();
						MV.addObject("listaLocalidades", listaLoc);
						
						String pattern = "yyyy-MM-dd HH:mm:ss";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
						String filtroFechaNac = simpleDateFormat.format(p.getFechaNac()==null?new java.util.Date():p.getFechaNac());
						MV.addObject("fechaNacDatePicker",filtroFechaNac);
						MV.addObject("CodigoDuplicado", true);
						MV.setViewName("AddEditUsuario");
					}
					else
					{
						resp = this.servicePersona.insertarPersona(p);
						if(!resp) {
							throw new Exception("Error al Insertar Persona");
						}
						perfilCliente = this.servicePerfil.obtenerPerfil(txtPerfil);
						u.setActivo(true);
						u.setNombreUsuario(txtCodigo);
						u.setPassword(txtPassword);
						u.setPerfil(perfilCliente);
						u.setPersona(p);
						if(btnSubmit.equals("alta")) {
							resp2 = this.serviceUsuario.insertarUsuario(u);
						}
						MV.setViewName("AltaUsuarioOk");
					}
				}
				else if(btnSubmit.equals("modificacion")) {

					u = this.serviceUsuario.getUserById(Integer.parseInt(idUsuarioEdit));
					u2= this.serviceUsuario.getUserByUsername(txtCodigo);
					p2 = this.servicePersona.obtenerPersonaPorDni(txtDNI);
					if(p2!=null && u.getPersona().getIdPersona()!=p2.getIdPersona()) { //Estoy editando el codigo de usuario y ya existe un usuario con ese codigo de usuario

						MV.addObject("nombre", p.getNombre());
						MV.addObject("apellido", p.getApellido());
						MV.addObject("dni", p.getDni());
						MV.addObject("fechaNac", p.getFechaNac());
						MV.addObject("direccion", p.getDireccion());
						MV.addObject("direccionNro", p.getDireccionNro());
						MV.addObject("mail", p.getMail());
						MV.addObject("cuil", p.getCuil());
						MV.addObject("codigo",txtCodigo);
						MV.addObject("password", txtPassword);
						MV.addObject("cp", p.getLocalidad()==null?-1:p.getLocalidad().getCP());
						MV.addObject("sexo", p.getSexo());
						MV.addObject("idUsuarioEdit", idUsuarioEdit);
					

						ArrayList<Localidad> listaLoc = (ArrayList<Localidad>)this.serviceLocalidad.obtenerTodos();
						MV.addObject("listaLocalidades", listaLoc);
						
						String pattern = "yyyy-MM-dd HH:mm:ss";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
						String filtroFechaNac = simpleDateFormat.format(p.getFechaNac()==null?new java.util.Date():p.getFechaNac());
						MV.addObject("fechaNacDatePicker",filtroFechaNac);
						MV.addObject("DniDuplicado", true);
						MV.setViewName("EditUsuario");
						
						
					}
					else if(u2!=null && u.getIdUsuario()!=u2.getIdUsuario()) {
						MV.addObject("nombre", p.getNombre());
						MV.addObject("apellido", p.getApellido());
						MV.addObject("dni", p.getDni());
						MV.addObject("fechaNac", p.getFechaNac());
						MV.addObject("direccion", p.getDireccion());
						MV.addObject("direccionNro", p.getDireccionNro());
						MV.addObject("mail", p.getMail());
						MV.addObject("cuil", p.getCuil());
						MV.addObject("codigo",txtCodigo);
						MV.addObject("password", txtPassword);
						MV.addObject("cp", p.getLocalidad()==null?-1:p.getLocalidad().getCP());
						MV.addObject("sexo", p.getSexo());
						MV.addObject("idUsuarioEdit", idUsuarioEdit);
					

						ArrayList<Localidad> listaLoc = (ArrayList<Localidad>)this.serviceLocalidad.obtenerTodos();
						MV.addObject("listaLocalidades", listaLoc);
						
						String pattern = "yyyy-MM-dd HH:mm:ss";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
						String filtroFechaNac = simpleDateFormat.format(p.getFechaNac()==null?new java.util.Date():p.getFechaNac());
						MV.addObject("fechaNacDatePicker",filtroFechaNac);
						MV.addObject("CodigoDuplicado", true);
						MV.setViewName("EditUsuario");

						
					}
					else
					{
						p.setIdPersona(u.getPersona().getIdPersona());
						resp = this.servicePersona.actualizarPersona(p);
						if(!resp) {
							throw new Exception("Error al actualizar persona.");
						}
						
						
						perfilCliente = this.servicePerfil.obtenerPerfil(txtPerfil);
						u.setActivo(true);
						u.setNombreUsuario(txtCodigo);
						u.setPassword(txtPassword);
						u.setPerfil(perfilCliente);
						u.setPersona(p);
						resp2 = this.serviceUsuario.UpdatearUsuario(u);
						if(!resp2) {
							throw new Exception("Error al actualizar usuario");
						}
						
						MV.setViewName("ModificacionUsuarioOk");
					}
				}
				
			}
			catch(Exception e) {
				System.out.println("Error dando de alta o modificando usuarios: " + e.getMessage());
				MV.setViewName("AltaUsuarioErr");
			}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("LoginErr.html")
	public ModelAndView eventoRedireccionarLoginErr(HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		
		HttpSession misession = request.getSession(true);
//		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
//		misession.setAttribute("SessionIdUsuario", idUsuario);

		MV.setViewName("LoginErr");
		return MV;
	}
	
	@RequestMapping("procesar_Login.html")
	public String procesarLoginAdmin(String txtCodigo, String txtContrasenia, String btnSubmit, HttpServletRequest request) {
		System.out.println("procesar_Login.html");
		System.out.println("txtCodigo:" + txtCodigo);
		System.out.println("txtContrasenia:" + txtContrasenia);
		HttpSession misession = request.getSession(true);
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);

		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");

		this.servicePersona = (PersonaServicio) appContext.getBean("personaServicioImpl");
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");

		Persona persona = (Persona) appContext.getBean("persona");
		Usuario usuario = (Usuario) appContext.getBean("usuario");

		usuario=(Usuario)this.serviceUsuario.getUserByUsernameAndPassword(txtCodigo, txtContrasenia);

		if(usuario==null) {
			((ConfigurableApplicationContext)(appContext)).close();
			return "redirect:/LoginErr.html";
		}
		else {
			persona=(Persona)this.servicePersona.getPersonaById(usuario.getPersona().getIdPersona());

			if(persona==null) {
				((ConfigurableApplicationContext)(appContext)).close();
				return "redirect:/LoginErr.html";
			}
			else {

				if (misession.getAttribute("SessionIdUsuario") == null)
				{
					misession.setAttribute("SessionApeNomUsuario", persona.getNombre() + " " + persona.getApellido());
					misession.setAttribute("SessionPerfilUsuario", usuario.getPerfil().getPerfil());
					misession.setAttribute("SessionIdUsuario", usuario.getIdUsuario());
					System.out.println("##SessionIdUsuario:" + usuario.getIdUsuario());
				}
				
				if (misession.getAttribute("UsuarioLogueado") == null)
				{
					misession.setAttribute("UsuarioLogueado", usuario);
				}
				
				MV.addObject("NombreUsuario", (String) misession.getAttribute("SessionApeNomUsuario"));
				MV.addObject("UsuarioLogueado", (Usuario) misession.getAttribute("UsuarioLogueado"));
				
				((ConfigurableApplicationContext)(appContext)).close();
				if(usuario.getPerfil().getPerfil().equals("administrador")) {
					return "redirect:/HomePageAdmin.html";
				}
				else {
					return "redirect:/HomePageCliente.html";
				}
			}
		}

	}

	public String getRandomIntegerString(int len, int max){
		  Random r = new Random();
		  String aRandom = "";
		  for(int i = 0; i < len; i++){
		    aRandom += String.valueOf(r.nextInt(max)+1);
		  }
		  return aRandom;
		}

	
	
	
	@RequestMapping("DesloguearUsuario.html")
	public ModelAndView DesloguearUsuario(HttpServletRequest request) {
		HttpSession misession = request.getSession(true);
		
		ModelAndView MV = new ModelAndView();
		misession.setAttribute("SessionApeNomUsuario",null);
		misession.setAttribute("SessionPerfilUsuario",null);
		misession.setAttribute("SessionIdUsuario",null);
		misession.setAttribute("UsuarioLogueado",null);
		MV.setViewName("Login");
		System.out.println("DesloguearUsuario");
		return MV;
	}

	@RequestMapping("procesar_AltaCuenta.html")
	public ModelAndView procesarAltaCuenta(int txtTipoCuenta, String txtDniCliente, String btnSubmit, HttpServletRequest request) {
		System.out.println("ProcesarAltaCuenta.html");
		System.out.println(btnSubmit);
		if(btnSubmit == null) {
			btnSubmit = "volver";
		}
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);

		if(btnSubmit.equals("aceptar")) {
			try {
				System.out.println("Dentro del submit");

				Cuenta cuenta = (Cuenta) appContext.getBean("cuenta");
				Movimiento movimiento = (Movimiento) appContext.getBean("movimiento");
				AltaCuenta altaCuenta = (AltaCuenta) appContext.getBean("altaCuenta");
				this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
				this.servicePersona = (PersonaServicio) appContext.getBean("personaServicioImpl");
				this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");
				TipoCuenta tipoCuenta = (TipoCuenta) this.serviceCuenta.obtenerTipoCuenta(txtTipoCuenta);
				Persona persona = (Persona) this.servicePersona.obtenerPersonaPorDni(txtDniCliente);
				TipoMovimiento tipoMovimiento = (TipoMovimiento) this.serviceTransaccion.obtenerTipoMovimiento("alta_cuenta");
				
				int contCuentasActivas = 0;
				try {
					List<Cuenta> listaCuentas = this.serviceCuenta.obtenerCuentasPorIdPersona(persona.getIdPersona());
					for(Cuenta c : listaCuentas) {
						if(c.getActivo()) {
							contCuentasActivas++;
						}
					}					
				}
				catch(Exception e) {
					System.out.println("error al recuperar cuentas de la persona:" + e);
				}

				if(persona==null) {
					MV.addObject("msgTitulo", "Atencion!");
					MV.addObject("msgTexto", "No se encontro un usuario con ese DNI.");
					MV.setViewName("MsgGenerico");
				}
				else if( contCuentasActivas > 3) {
					MV.addObject("msgTitulo", "Atencion!");
					MV.addObject("msgTexto", "El maximo permitido de cuentas es 4");
					MV.setViewName("MsgGenerico");					
				}
				else{
					String nroCuenta = this.getRandomIntegerString(6, 9);
					String cbu = this.getRandomIntegerString(22, 9);
					String codigo = tipoCuenta.getTipoCuenta() + "-" + nroCuenta;

					cuenta.setCliente(persona);
					cuenta.setTipoCuenta(tipoCuenta);
					cuenta.setNroCuenta(nroCuenta);
					cuenta.setCbu(cbu);
					cuenta.setCodigo(codigo);
					cuenta.setDescripcion(codigo);
					cuenta.setNombre(codigo);
					cuenta.setActivo(true);
					cuenta.setSaldo(10000.0);
					cuenta.setFechaCreacion(new Date());

					altaCuenta.setCuenta(cuenta);
					altaCuenta.setMonto(10000.0);

					movimiento.setDetalleMovimiento("Alta Cuenta");
					movimiento.setFechaAlta(new Date());
					movimiento.setTipoMovimiento(tipoMovimiento);
					movimiento.setCuenta(cuenta);
					movimiento.setImporte(10000.0);

					if(!this.serviceCuenta.insertarCuenta(cuenta)) {
						throw new Exception("Error al dar de alta la cuenta");
					}
					if( !this.serviceTransaccion.insertarAltaCuenta(altaCuenta) ) {
						throw new Exception("Error dando de alta la cuenta");
					}
					if(!this.serviceTransaccion.insertarMovimiento(movimiento)) {
						throw new Exception("Error al generar los movimientos");
					}					
					MV.setViewName("AltaCuentaOk");
				}
			}
			catch(Exception e) {
				//TODO: BORRAR TODO SI FALLA ALGUNO. MEJORAR COMO SE VALIDA.
				MV.addObject("mensajeError", e.getMessage());
				MV.setViewName("AltaCuentaErr");
			}
		}
		else {
			MV.setViewName("HomePageAdmin");
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}
	
	@RequestMapping("procesar_BorrarCuenta.html")
	public ModelAndView procesarBorrarCuenta(int btnSubmit, HttpServletRequest request) {
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);		
		
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		
		try {
			Cuenta cuenta = this.serviceCuenta.obtenerCuentaPorId(btnSubmit);
			cuenta.setActivo(false);
			
			this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");
			List<Prestamo> listaPrestamos = this.serviceTransaccion.obtenerPrestamoPorCuenta(cuenta.getId());
			EstadoPrestamo estado = this.serviceTransaccion.obtenerEstadoPrestamo("cancelado");
			for(Prestamo p : listaPrestamos) {
				if(p.getEstado().getCodigo().equals("pendiente")) {
					p.setEstado(estado);
					this.serviceTransaccion.actualizarPrestamo(p);
				}
			}
			
			this.serviceCuenta.actualizarCuenta(cuenta);
			MV.setViewName("BajaCuentaOk");
		}
		catch(Exception e) {
			System.out.println("Error al modificar cuenta:" + e.getMessage());
			MV.addObject("mensajeError", "Error al modificar cuenta:" + e.getMessage());
			MV.setViewName("ModificadoCuentaErr");
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	
	@RequestMapping("redirigir_ModificarCuenta.html")
	public ModelAndView redirigir_ModificarCuenta(int btnSubmit, HttpServletRequest request) {
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);		

		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		
		try {
			Cuenta cuenta = this.serviceCuenta.obtenerCuentaPorId(btnSubmit);
			MV.addObject("descripcion", cuenta.getDescripcion());
			MV.addObject("cbu", cuenta.getCbu());
			MV.addObject("nombreCuenta", cuenta.getNombre());
			MV.addObject("idCuenta", cuenta.getId());
			MV.setViewName("ModificacionCuenta");
		}
		catch(Exception e) {
			System.out.println("Error al modificar cuenta:" + e.getMessage());
			MV.addObject("mensajeError", "Error al modificar cuenta:" + e.getMessage());
			MV.setViewName("ModificadoCuentaErr");
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	
	
	@RequestMapping("procesar_ModificarCuenta.html")
	public ModelAndView procesar_ModificarCuenta(String txtDescripcion, String txtCBU, String txtNombreCuenta, int btnSubmit, HttpServletRequest request) {
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);		
		
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		
		try {
			Cuenta cuenta = this.serviceCuenta.obtenerCuentaPorId(btnSubmit);

			cuenta.setDescripcion(txtDescripcion);
			cuenta.setCbu(txtCBU);
			cuenta.setNombre(txtNombreCuenta);
			this.serviceCuenta.actualizarCuenta(cuenta);
			
			MV.setViewName("ModificadoCuentaOk");
		}
		catch(Exception e) {
			System.out.println("Error al modificar cuenta:" + e.getMessage());
			MV.addObject("mensajeError", "Error al modificar cuenta:" + e.getMessage());
			MV.setViewName("ModificadoCuentaErr");
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}
	
	
	@RequestMapping("redirigir_TipoTransferencia.html")
	public ModelAndView redirigirTipoTransferencia(String txtTipoTransferencia ,String btnSubmit, HttpServletRequest request) {
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		
		if(btnSubmit.equals("volver")) {
			System.out.println("redirigirTipoTransferencia->volver");
			MV = HomePageCliente(request);
		}
		else {

				//Muestro cuentas existentes del usuario
				List<Cuenta> listaCuentasOrigen = this.serviceCuenta.obtenerCuentasActivasPorUsuario(idUsuario);
				
				MV.addObject("listaCuentasOrigen", listaCuentasOrigen);

				if(txtTipoTransferencia.equals("terceros")) {
					MV.setViewName("NuevaTransferenciaCbu");
				}
				else if(txtTipoTransferencia.equals("propias")) {
					MV.setViewName("NuevaTransferenciaCuentas");
				}	

					
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("procesar_NuevaTransferenciaCuentas.html")
	public ModelAndView procesarNuevaTransferenciaCuentas(int txtIdCuentaOrigen, int txtIdCuentaDestino, double txtMonto, String btnSubmit, HttpServletRequest request) {
		System.out.println("procesarNuevaTransferenciaCuentas->init");
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
		Usuario usuario = this.serviceUsuario.getUserById(idUsuario);
		
		if(btnSubmit.equals("volver")) {
			MV = HomePageCliente(request);
		}
		else {
			try {
				this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
				Cuenta cuentaOrigen = this.serviceCuenta.obtenerCuentaPorId(txtIdCuentaOrigen);
				Cuenta cuentaDestino = this.serviceCuenta.obtenerCuentaPorId(txtIdCuentaDestino);
				if(cuentaOrigen==null) {
					MV.addObject("msgTitulo", "Atencion!");
					MV.addObject("msgTexto", "Usted no posee el tipo de cuenta de origen seleccionado.");
					MV.setViewName("MsgGenerico");
				}
				else if(cuentaDestino==null) {
					MV.addObject("msgTitulo", "Atencion!");
					MV.addObject("msgTexto", "Usted no posee el tipo de cuenta destino seleccionado.");
					MV.setViewName("MsgGenerico");
				}
				else if(cuentaOrigen.getId()==cuentaDestino.getId()) {
					MV.addObject("CuentasIguales", "true!");
					//Muestro cuentas existentes del usuario
					List<Cuenta> listaCuentasOrigen = this.serviceCuenta.obtenerCuentasActivasPorUsuario(idUsuario);
					
					MV.addObject("listaCuentasOrigen", listaCuentasOrigen);
					MV.setViewName("NuevaTransferenciaCuentas");
				}
				else {
					MV.addObject("cuentaOrigen", cuentaOrigen);
					MV.addObject("cuentaDestino", cuentaDestino);
					MV.addObject("monto", txtMonto);
					MV.setViewName("NuevaTransferenciaConfirmar");
				}
			}
			catch(Exception e){
				System.out.println("Error:" + e);
				MV.setViewName("NuevaTransferenciaErr");
			}
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}
	
	@RequestMapping("procesar_NuevaTransferenciaCbu.html")
	public ModelAndView procesarNuevaTransferenciaCbu(int txtIdCuentaOrigen, String txtCBU, double txtMonto, String btnSubmit, HttpServletRequest request) {
		System.out.println("procesarNuevaTransferenciaCbu->init");
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		System.out.println("idUsuario:" + idUsuario);

		if(btnSubmit.equals("volver")) {
			MV = HomePageCliente(request);
		}
		else {
			try {			
				this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
				this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
				Usuario usuario = this.serviceUsuario.getUserById(idUsuario);
				Cuenta cuentaDestino = this.serviceCuenta.obtenerCuentaPorCbu(txtCBU);
				Cuenta cuentaOrigen = this.serviceCuenta.obtenerCuentaPorId(txtIdCuentaOrigen);
				System.out.println("Cuentas:" + cuentaOrigen + "||" + cuentaDestino);
				if(cuentaOrigen == null) {
					throw new Exception("Cuenta origen inexistente");
				}
				if(cuentaDestino == null) {
					throw new Exception("Cuenta destino inexistente");
				}
				if(cuentaOrigen.getCodigo().equals(cuentaDestino.getCodigo())) {
					throw new Exception("La cuenta origen y destino deben ser diferentes");
				}
				MV.addObject("cuentaOrigen", cuentaOrigen);
				MV.addObject("cuentaDestino", cuentaDestino);
				MV.addObject("monto", txtMonto);
				MV.setViewName("NuevaTransferenciaConfirmar");
			}
			catch(Exception e) {
				System.out.println(e);
				MV.addObject("mensajeError", e.getMessage());
				MV.setViewName("NuevaTransferenciaErr");
			}			
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("procesar_NuevaTransferencia.html")
	public ModelAndView procesarNuevaTransferencia(int idCuentaOrigen, int idCuentaDestino, double monto, String btnSubmit, HttpServletRequest request) {
		System.out.println("procesarNuevaTransferencia->init");
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		
		if(btnSubmit.equals("volver")) {
			MV = HomePageCliente(request);
		}
		else {
			try {
				Cuenta cuentaOrigen = this.serviceCuenta.obtenerCuentaPorId(idCuentaOrigen);
				Cuenta cuentaDestino = this.serviceCuenta.obtenerCuentaPorId(idCuentaDestino);
				System.out.println("Cuentas:" + cuentaOrigen + "||||" + cuentaDestino);
				
				double montoDestino = monto;
				if( (	cuentaOrigen.getTipoCuenta().getCodigo().equals("ca_dolares") ||
						cuentaOrigen.getTipoCuenta().getCodigo().equals("ca_esp_dolares") ) &&
					(	cuentaDestino.getTipoCuenta().getCodigo().equals("ca_pesos") ||
						cuentaDestino.getTipoCuenta().getCodigo().equals("ca_esp_pesos") ||
						cuentaDestino.getTipoCuenta().getCodigo().equals("cc"))) {

					montoDestino = monto * dolarVenta;
				}
				else if(
						(	cuentaOrigen.getTipoCuenta().getCodigo().equals("ca_pesos") ||
							cuentaOrigen.getTipoCuenta().getCodigo().equals("ca_esp_pesos") ||
							cuentaOrigen.getTipoCuenta().getCodigo().equals("cc")) &&
						(	cuentaDestino.getTipoCuenta().getCodigo().equals("ca_dolares") ||
							cuentaDestino.getTipoCuenta().getCodigo().equals("ca_esp_dolares") )
						) {

					montoDestino = monto / dolarCompra;
				}

				
				
				if(cuentaOrigen.getSaldo() < monto) {
					throw new Exception("El saldo actual es inferior al que se intenta transferir");
				}
				if(monto <= 0) {
					throw new Exception("El monto de la transferencia debe ser un valor positivo");
				}
				if(cuentaOrigen.getId() == cuentaDestino.getId()) {
					throw new Exception("Cuenta origen y destino no pueden ser la misma");
				}
				if(cuentaDestino == null || !cuentaDestino.getActivo()) {
					throw new Exception("La cuenta destino no existe o no esta habilitada");
				}

				Transferencia transferencia = (Transferencia) appContext.getBean("transferencia");
				Movimiento movimientoPos = (Movimiento) appContext.getBean("movimiento");
				Movimiento movimientoNeg = (Movimiento) appContext.getBean("movimiento");

				System.out.println(transferencia);
				Date fechaAlta = new Date();
				
				transferencia.setCuentaOrigen(cuentaOrigen);
				transferencia.setCuentaDestino(cuentaDestino);
				transferencia.setMonto(monto);
				transferencia.setFechaAlta(fechaAlta);

				System.out.println("movimientoPos:" + movimientoPos);
				System.out.println("movimientoNeg:" + movimientoNeg);

				TipoMovimiento tipoMovimiento = this.serviceTransaccion.obtenerTipoMovimiento("transferencia");

				System.out.println("tipoMovimiento:" + tipoMovimiento);

				movimientoPos.setDetalleMovimiento("Transferencia recibida de " + cuentaOrigen.getDescripcion());
				movimientoPos.setFechaAlta(fechaAlta);
				movimientoPos.setTipoMovimiento(tipoMovimiento);
				movimientoPos.setImporte(montoDestino);
				movimientoPos.setCuenta(cuentaDestino);

				movimientoNeg.setDetalleMovimiento("Transferencia enviada a " + cuentaDestino.getDescripcion());
				movimientoNeg.setFechaAlta(fechaAlta);
				movimientoNeg.setTipoMovimiento(tipoMovimiento);
				movimientoNeg.setCuenta(cuentaOrigen);
				movimientoNeg.setImporte(monto * (-1));
				
				cuentaOrigen.setSaldo(cuentaOrigen.getSaldo()-monto);
				cuentaDestino.setSaldo(cuentaDestino.getSaldo()+montoDestino);
				List<Cuenta> cuentas = Arrays.asList(cuentaOrigen, cuentaDestino);

				if( !this.serviceCuenta.actualizarCuentas(cuentas) ) {
					throw new Exception("Error al actualizar saldos de cuentas");
				}

				this.serviceTransaccion.insertarTransferencia(transferencia);
				this.serviceTransaccion.insertarMovimiento(movimientoPos);
				this.serviceTransaccion.insertarMovimiento(movimientoNeg);
				MV.addObject("transferencia", transferencia);
				MV.setViewName("NuevaTransferenciaOk");
			}
			catch(Exception e){
				System.out.println(e);
				MV.addObject("mensajeError", e.getMessage());
				MV.setViewName("NuevaTransferenciaErr");
			}
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}
	
	@RequestMapping("procesar_NuevoPrestamoConfirmar.html")
	public ModelAndView procesarNuevoPrestamoConfirmar(int txtIdCuentaDestino, double txtImporte, int txtCantidadCuotas, String btnSubmit, HttpServletRequest request) {
		System.out.println("procesarNuevoPrestamoConfirmar->init");
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		Cuenta cuenta = this.serviceCuenta.obtenerCuentaPorId(txtIdCuentaDestino);
		
		if(btnSubmit.equals("volver")) {
			MV = HomePageCliente(request);
		}
		else {
			try {
				MV.addObject("idCuentaDestino", txtIdCuentaDestino);
				MV.addObject("cuentaDestino", cuenta.getDescripcion());
				MV.addObject("importe", txtImporte);
				MV.addObject("cuotas", txtCantidadCuotas);
				MV.addObject("montoPorCuota", txtImporte/txtCantidadCuotas);
				MV.setViewName("NuevoPrestamoConfirmar");
			}
			catch(Exception e) {
				System.out.println("Error: " + e.getMessage());
				MV.addObject("mensajeError", e.getMessage());
				MV.setViewName("NuevoPrestamoErr");
			}			
		}

		return MV;
	}

	@RequestMapping("procesar_nuevoPrestamo.html")
	public ModelAndView procesarNuevoPrestamo(int idCuentaDestino, double txtImporte, int txtCantidadCuotas, String btnSubmit, HttpServletRequest request) {
		System.out.println("procesarNuevoPrestamo->init");
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);
		System.out.println("idUsuario:" + idUsuario);

		if(btnSubmit.equals("volver")) {
			MV = HomePageCliente(request);
		}
		else {
			try {

				this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");
				this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
				this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
				Usuario usuario = (Usuario) this.serviceUsuario.getUserById(idUsuario);
				EstadoPrestamo estadoPrestamo = (EstadoPrestamo) this.serviceTransaccion.obtenerEstadoPrestamo("pendiente");
				Prestamo prestamo = (Prestamo) appContext.getBean("prestamo");
				//Movimiento movimiento = (Movimiento) appContext.getBean("movimiento");
				Cuenta cuenta = this.serviceCuenta.obtenerCuentaPorId(idCuentaDestino);

				System.out.println("Cuenta:" + cuenta);
				if(cuenta == null) {
					throw new Exception("Debe tener una cuenta en pesos para poder acceder a prestamos");
				}
				if(txtCantidadCuotas < 1) {				
					throw new Exception("La cantidad de cuotas debe ser un valor positivo entero");
				}
				if(txtImporte <= 0) {
					throw new Exception("El importe del prestamo debe ser un valor positivo");
				}

				//cuenta.setSaldo(cuenta.getSaldo() + txtImporte);

				prestamo.setCliente(usuario.getPersona());
				prestamo.setFecha(new Date());
				prestamo.setMonto(txtImporte);
				prestamo.setCuotas(txtCantidadCuotas);
				prestamo.setPlazo(txtCantidadCuotas);
				prestamo.setMontoPorMes( txtImporte / txtCantidadCuotas );
				prestamo.setEstado(estadoPrestamo);
				prestamo.setCuentaDestino(cuenta);

				//TODO: Se tiene que hacer rollback por cualquier cosa que falle..
				if( !this.serviceTransaccion.insertarPrestamo(prestamo)) {
					throw new Exception("No se puedo generar el prestamo.");
				}
		        Calendar cal = Calendar.getInstance();
		        double MontoPorCuota = prestamo.getMontoPorMes();
				for(int nc = 1; nc <= txtCantidadCuotas; nc++) {
					cal.add(Calendar.MONTH, 1);
					ArrayList<Cuota> cuotas = new ArrayList<Cuota>();
					Cuota cuota = (Cuota) appContext.getBean("cuota");
					cuota.setFechaVencimiento(cal.getTime());
					cuota.setNroCuota(nc);
					cuota.setPrestamo(prestamo);
					cuota.setMonto(MontoPorCuota);
					cuotas.add(cuota);
					if( !this.serviceTransaccion.insertarCuotas(cuotas) ) {
						throw new Exception("Error al generar cuotas.");
						//Tengo que rollbackear el prestamo...
					}
				}

				MV.addObject("prestamo", prestamo);
				MV.setViewName("NuevoPrestamoOk");
			}
			catch(Exception e) {
				System.out.println("Error:" + e.getMessage());
				MV.addObject("mensajeError", e.getMessage());
				MV.setViewName("NuevoPrestamoErr");
			}			
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;		
	}
	
	@RequestMapping("redirigir_CuotasPrestamo.html")
	public ModelAndView redirigir_CuotasPrestamo(int btnIdPrestamo, HttpServletRequest request) {
		System.out.println("redirigir_PagarCuota->IdPrestamo:" + btnIdPrestamo);
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);

		ArrayList<Cuota> cuotas = this.serviceTransaccion.obtenerCuotasPrestamo(btnIdPrestamo);
		System.out.println("cuotas:" + cuotas);
		
		MV.addObject("cuotas", cuotas);
		MV.setViewName("ListadoCuotasPrestamo");
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("redirigir_PagarCuotaSeleccionCuenta.html")
	public ModelAndView redirigir_PagarCuotaSeleccionCuenta(int btnIdCuota, HttpServletRequest request) {
		
		System.out.println("redirigir_PagarCuotaSeleccionCuenta->btnIdCuota:" + btnIdCuota);
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");
		Cuota cuota = this.serviceTransaccion.obtenerCuota(btnIdCuota);

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);

		this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
		Usuario usuario = this.serviceUsuario.getUserById(idUsuario);
		
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		List<Cuenta>  listaCuentas=(List<Cuenta>)this.serviceCuenta.obtenerCuentaPorUsuario(usuario.getPersona().getIdPersona());
		
		//nroCuota, cuentas, monto, idCuota 
		MV.addObject("nroCuota", cuota.getNroCuota());
		MV.addObject("cuentas", listaCuentas);
		MV.addObject("monto", cuota.getMonto());
		MV.addObject("idCuota", cuota.getId());

		MV.setViewName("PagarCuotaSeleccionCuenta");
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("procesar_PagarCuota.html")
	public ModelAndView procesar_PagarCuota(int btnIdCuota, int txtIdCuentaOrigen, HttpServletRequest request) {
		System.out.println("procesar_PagarCuota->btnIdCuota:" + btnIdCuota);
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		this.serviceTransaccion = (TransaccionServicio) appContext.getBean("transaccionServicioImpl");
		Cuota cuota = this.serviceTransaccion.obtenerCuota(btnIdCuota);

		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);

		this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
		Usuario usuario = this.serviceUsuario.getUserById(idUsuario);
		Persona persona = usuario.getPersona();
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		
		Cuenta cuenta = this.serviceCuenta.obtenerCuentaPorId(txtIdCuentaOrigen);
		Prestamo prestamo = cuota.getPrestamo();
		
		int cuotaHabilitada = 0;
		for (Cuota c : this.serviceTransaccion.obtenerCuotasPrestamo(prestamo.getId())) {
			if(c.getFechaPago() == null) {
				cuotaHabilitada = c.getNroCuota();
				break;
			}
		}
		
		try {
			if( !prestamo.getEstado().getDescripcion().equals("Aceptado") ) { //Hardcodeo aceptado
				throw new Exception("El prestamo no fue aprobado aun.");
			}
			if(cuenta.getSaldo() < cuota.getMonto()) {
				throw new Exception("No posee fondos suficientes en su caja de ahorro.");
			}
			if(cuota.getNroCuota() != cuotaHabilitada) {
				throw new Exception("La cuota seleccionada no corresponde.");
			}
			cuota.setFechaPago(new Date());
			cuenta.setSaldo( cuenta.getSaldo() - cuota.getMonto() );
			this.serviceTransaccion.actualizarCuota(cuota);
			this.serviceCuenta.actualizarCuenta(cuenta);
			
			Movimiento movimientoNeg = (Movimiento) appContext.getBean("movimiento");
			movimientoNeg.setCuenta(cuenta);
			movimientoNeg.setDetalleMovimiento("Pago cuota " + cuota.getNroCuota() + ", monto: " + cuota.getMonto());
			movimientoNeg.setFechaAlta(cuota.getFechaPago());
			movimientoNeg.setImporte(cuota.getMonto());
			TipoMovimiento tipoMovimiento = (TipoMovimiento) this.serviceTransaccion.obtenerTipoMovimiento("pago_prestamo");
			movimientoNeg.setTipoMovimiento(tipoMovimiento);
			this.serviceTransaccion.insertarMovimiento(movimientoNeg);
			
			MV.setViewName("PagarCuotaOk");
		}
		catch(Exception e) {
			System.out.println("Error al pagar cuota:" + e.getMessage());
			MV.addObject("mensajeError", "Error al pagar cuota:" + e.getMessage());
			MV.setViewName("PagarCuotaErr");
		}
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("redirigir_HomePage.html")
	public String redirigir_HomePage(HttpServletRequest request) {

		ModelAndView MV = new ModelAndView();
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);

		if (misession.getAttribute("SessionPerfilUsuario") == null) 
		{
			return "redirect:/Login.html";
			//MV.setViewName("Login");
		}
		else if(misession.getAttribute("SessionPerfilUsuario").equals("administrador")) {
			return "redirect:/HomePageAdmin.html";
//			MV.setViewName("HomePageAdmin");
		}
		else {
			return "redirect:/HomePageCliente.html";
//			MV.setViewName("HomePageCliente");
		}
	}
}
