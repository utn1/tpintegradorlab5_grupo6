package frgp.utn.edu.ar.DaoImpl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.entidad.Prestamo;
import frgp.utn.edu.ar.entidad.Usuario;
import frgp.utn.edu.ar.Dao.PrestamoDao;


public class PrestamoDaoImpl implements PrestamoDao  {
	@Autowired
	private Conexion<Prestamo> cn;
	
	public PrestamoDaoImpl()
	{
		cn = new Conexion<Prestamo>();
	}	

	@Override
	public List<Prestamo> obtenerTodosPrestamo() {
		 
		Session session = cn.abrirConexion();
		String hql = "from Prestamo";
		List<Prestamo> tu = (List<Prestamo>)session.createQuery(hql).list();
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public List<Prestamo> obtenerPrestamoPorUsuario(Integer id) {
		 
		Session session = cn.abrirConexion();
		String hql = "from Prestamo where Id= '" + id + "'";
		List<Prestamo> tu = (List<Prestamo>) session.createQuery(hql).list();
		cn.cerrarSession();
		return tu;
	}
	
	
	@Override
	public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta){
		Session session = cn.abrirConexion();
		String hql = "from Prestamo";
		List<Prestamo> aux = (List<Prestamo>) session.createQuery(hql).list();
		List<Prestamo> tu = new ArrayList<Prestamo>();
		for(Prestamo p : aux) {
			if(p.getCuentaDestino().getId() == idCuenta) {
				tu.add(p);
			}
		}
		cn.cerrarSession();
		return tu;		
	}
	
	
	@Override
	public boolean actualizarEstadoPrestamoE(Prestamo prestamo)
	{
		boolean resp;
		try {
			cn.update(prestamo);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;
	}
	
	@Override
	public Prestamo getPrestamoById(int id) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Prestamo where Id='" + id + "'"; //TODO Agregar el activo
		Prestamo tu = (Prestamo)session.createQuery(hql).uniqueResult();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public List<Prestamo> obtenerTodosPrestamoByEstadoDesdeHasta(Integer id,Date desde,Date hasta) {
		 
		Session session = cn.abrirConexion();
		
		if(id==-1) //Todos
		{
			String hql = "from Prestamo where Fecha> :desde and Fecha<:hasta"; 
			List<Prestamo> tu = (List<Prestamo>)session.createQuery(hql)
			.setParameter("desde",desde)
			.setParameter("hasta",hasta).list();
			cn.cerrarSession();
			return tu;
			
		}else //Por Estado
		{
			String hql = "from Prestamo where estado.Id= :id and Fecha> :desde and Fecha<:hasta"; 
			List<Prestamo> tu = (List<Prestamo>)session.createQuery(hql).setParameter("id",id)
			.setParameter("desde",desde)
			.setParameter("hasta",hasta).list();
			cn.cerrarSession();
			return tu;
		}

//      Por que en la base de datos se guarda con utc 0
//		desde=addHoursToJavaUtilDate(desde,3);
//		hasta=addHoursToJavaUtilDate(hasta,3);
		//List<Movimiento> tu = (List<Movimiento>)session.createQuery(hql).setInteger(0, id).setDate(1, desde).setDate(2, hasta).list();

		
	
		
	}

}

