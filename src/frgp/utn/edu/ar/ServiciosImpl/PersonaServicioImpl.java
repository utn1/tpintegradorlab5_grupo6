package frgp.utn.edu.ar.ServiciosImpl;



import frgp.utn.edu.ar.Dao.PersonaDao;
import frgp.utn.edu.ar.Servicios.PersonaServicio;
import frgp.utn.edu.ar.entidad.Persona;
import frgp.utn.edu.ar.entidad.Usuario;

public class PersonaServicioImpl implements PersonaServicio {

	private PersonaDao dataAccess = null;

	public void setDataAccess(PersonaDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public boolean insertarPersona(Persona persona) {
		try {
			this.dataAccess.insertarPersona(persona);
			return true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception"); // Entra en este catch
			return false;
		}

	}
	@Override
	public Persona getPersonaById(Integer idPersona) {

			return this.dataAccess.getPersonaById(idPersona);
	}
	
	@Override
	public Persona obtenerPersonaPorDni(String dni) {
		System.out.println("obtenerPersonaPorDni.dni:" + dni);
		System.out.println("obtenerPersonaPorDni.dao:" + this.dataAccess);
		return this.dataAccess.obtenerPersonaPorDni(dni);
	}

	@Override
	public boolean actualizarPersona(Persona persona) {
		return this.dataAccess.actualizarPersona(persona);
		//return this.dataAccess.actualizarPersona2(persona);
	}
}