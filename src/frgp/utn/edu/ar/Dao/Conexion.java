package frgp.utn.edu.ar.Dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Conexion<Obj> {


	private SessionFactory sessionFactory;

	private Session session;
	
	public Conexion()
	{
		//Configuration configuration = new Configuration();
	    //configuration.configure();
	    //ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
	    //sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	    sessionFactory=HibernateUtil.getSessionFactory();
	}
	
	public Session abrirConexion()
	{
	    //sessionFactory = frgp.utn.edu.ar.Dao.HibernateUtil.getSessionFactory();
		session=sessionFactory.openSession();
		return session;
	}
	
	public void cerrarSession()
	{
		//sessionFactory = frgp.utn.edu.ar.Dao.HibernateUtil.getSessionFactory();
		session.close();
		//sessionFactory.close();
		//cerrarSessionFactory();
	}
	
	
	private void cerrarSessionFactory()
	{
		sessionFactory.close();
	}
	
	
	public void add(Obj o )
	{
		
	    Session session=sessionFactory.openSession();
	    session.beginTransaction();
	    session.save(o);
	    session.getTransaction().commit();
	    session.close();
	   // this.cerrarSessionFactory();
	}

	public void addAll(ArrayList<Obj> list )
	{
		ArrayList<Session> sessionList = new ArrayList<Session>();
		boolean all = true;
	    for(Obj o : list) {
	    	try {
			    Session session=sessionFactory.openSession();
			    session.beginTransaction();
		    	System.out.println("save: " + o);
		    	session.save(o);
		    	System.out.println("save OK");
		    	sessionList.add(session);
	    	}
	    	catch(Exception e) {
	    		System.out.println("Error addAll: " + e.getMessage());
	    		all = false;
	    		break;
	    	}
	    }
	    if(all) {
		    for( Session session : sessionList) {
			    session.getTransaction().commit();
			    session.close();
		    }
	    }
	    else {
		    for( Session session : sessionList) {
			    session.getTransaction().rollback();
			    session.close();
		    }	    	
	    }
	}

	
	public void update(Obj o )
	{
	    Session session=sessionFactory.openSession();
	    session.beginTransaction();
	    session.update(o);
	    session.getTransaction().commit();
	    session.close();
	   // this.cerrarSessionFactory();

	}

	public void updateAll(List<Obj> list) {
		ArrayList<Session> sessionList = new ArrayList<Session>();
		boolean all = true;
	    for(Obj o : list) {
	    	try {
			    Session session=sessionFactory.openSession();
			    session.beginTransaction();
		    	System.out.println("update: " + o);
		    	session.update(o);
		    	System.out.println("update OK");
		    	sessionList.add(session);
	    	}
	    	catch(Exception e) {
	    		System.out.println("Error updateAll: " + e.getMessage());
	    		all = false;
	    		break;
	    	}
	    }
	    if(all) {
		    for( Session session : sessionList) {
			    session.getTransaction().commit();
			    session.close();
		    }
	    }
	    else {
		    for( Session session : sessionList) {
			    session.getTransaction().rollback();
			    session.close();
		    }	    	
	    }
	}

	public void delete(Obj o )
	{
	    Session session=sessionFactory.openSession();
	    session.beginTransaction();
	    session.delete(o);
	    session.getTransaction().commit();   
	    session.close();
	}
}