package frgp.utn.edu.ar.Dao;
import java.util.List;

import frgp.utn.edu.ar.entidad.Localidad;

public interface LocalidadDao {
	public List<Localidad> obtenerTodos();
	public Localidad obtenerLocalidad(Integer CP);
	public boolean InsertarLocalidad(Localidad loc);
}

