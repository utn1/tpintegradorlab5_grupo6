package frgp.utn.edu.ar.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TipoMovimiento")
public class TipoMovimiento implements Serializable{

	//Implementar serializable
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IdTipoMovimiento")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int IdTipoMovimiento;
	
	@Column(name="Movimiento")
	private String movimiento;
	
	@Column(name="Activo")
	private Boolean Activo;
	
	@Column(name="Codigo", unique=true)
	private String Codigo;
	
	//Constructor vacio
	public TipoMovimiento()
	{
		
	}

	public String getMovimiento() {
		return movimiento;
	}
	
	public int getIdTipoMovimiento() {
		return IdTipoMovimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public Boolean getActivo() {
		return Activo;
	}
	public String getCodigo() {
		return Codigo;
	}

	public void setCodigo(String codigo) {
		this.Codigo = codigo;
	}
	
	public void setActivo(Boolean activo) {
		Activo = activo;
	}
	@Override
	public String toString() {
		return "TipoMovimiento [Movimiento=" + movimiento + "]";
	}
	
}
