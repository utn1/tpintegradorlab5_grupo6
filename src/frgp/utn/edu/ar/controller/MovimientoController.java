package frgp.utn.edu.ar.controller;



import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.Servicios.*;
import frgp.utn.edu.ar.entidad.*;
import frgp.utn.edu.ar.resources.Config;

@Controller
public class MovimientoController {
	//
	// @Autowired
	public PersonaServicio servicePersona;
	// @Autowired
	public LocalidadServicio serviceLocalidad;

	public UsuarioServicio serviceUsuario;
	
	public PerfilServicio servicePerfil;

	public CuentaServicio serviceCuenta;
	
	public TransaccionServicio serviceTransaccion;
	
	public MovimientoServicio serviceMovimiento;
	
	
	@RequestMapping("Redirigir_MovimientosPorCuenta.html")
	public ModelAndView Redirigir_MovimientosPorCuenta(int idCuenta,Date  Desde,Date  Hasta,HttpServletRequest request) {
		
		Date FechaDesde;
		Date FechaHasta;
		if(Desde==null) {
			FechaDesde=new java.util.Date();
			//PorDefecto dejo un mes
			FechaDesde=addDays(FechaDesde,-30);
		}
		else {
			FechaDesde=Desde;
		}
		if(Hasta==null) {
			FechaHasta=new java.util.Date();
		}else {
			FechaHasta=Hasta;
		}
		
		
		ModelAndView MV = new ModelAndView();
		HttpSession misession = request.getSession(true);
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);

		this.serviceMovimiento = (MovimientoServicio) appContext.getBean("movimientoServicioImpl");

		Movimiento u = (Movimiento) appContext.getBean("movimiento");

		//ArrayList<Movimiento> Movimientos = (ArrayList<Movimiento>) this.serviceMovimiento.obtenerMovimientosPorIdCuenta(idCuenta);
		ArrayList<Movimiento> Movimientos = (ArrayList<Movimiento>) this.serviceMovimiento.obtenerMovimientosPorIdCuentaDesdeHasta(idCuenta,FechaDesde,FechaHasta);
		
		
		this.serviceUsuario = (UsuarioServicio) appContext.getBean("usuarioServicioImpl");
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");

		Usuario usuario = (Usuario) appContext.getBean("usuario");
		int idUsuario = (int)misession.getAttribute("SessionIdUsuario");
		System.out.println("##SessionIdUsuario:" + idUsuario);
		misession.setAttribute("SessionIdUsuario", idUsuario);
		usuario=(Usuario)this.serviceUsuario.getUserById(idUsuario);
		List<Cuenta>  listaCuentas=(List<Cuenta>)this.serviceCuenta.obtenerCuentasActivasPorUsuario(usuario.getPersona().getIdPersona());

		MV.addObject("listaCuentas", listaCuentas);
		MV.addObject("listaMovimientos", Movimientos);
		
		//uso esto para devolver la fecha que filtramos e inicializar los datepicker con esta fecha
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String filtroDesde = simpleDateFormat.format(FechaDesde);
		String filtroHasta = simpleDateFormat.format(FechaHasta);

		MV.addObject("filtroDesde", filtroDesde);
		MV.addObject("filtroHasta", filtroHasta);
		MV.addObject("filtroIdCuenta", idCuenta);

		MV.setViewName("ListadoMovimientos");
		((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	public static Date addDays(Date fecha, int dias) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fecha);
		cal.add(Calendar.DATE, dias);
		return cal.getTime();
		}
	
}
