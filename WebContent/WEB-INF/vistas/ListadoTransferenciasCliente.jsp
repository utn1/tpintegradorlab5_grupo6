<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="<c:url value="/resources/css/contact_styles.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"
	type="text/css" rel="stylesheet">
	
	<link href="<c:url value="/resources/css/style.css" />"
	type="text/css" rel="stylesheet">
		<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">


	
	
<!--------------------------------stilo grilla----------------- -->
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
<!-- ------------------fin estilo grilla------------------ -->





<script type="text/javascript">
	$(document).ready(function($) {


			debugger;
			$('#example').DataTable(
			{
				"language" : {
					"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});
		});
</script>
</head>

<body>

	<div class="super_container">

		<!-- Header -->

		<header class="header trans_300"> <!-- Main Navigation -->
	 <%@include file="Menu.jsp"%>

		</header>
		<div class="container contact_container">

			<div class="row">
				<div class="col-lg-6 get_in_touch_col">
					<div class="get_in_touch_contents">
						<h1>Listado de Transferencias ${userName}</h1>
							<div>
								<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
								<table id="example" class="table table-striped table-bordered" style="width:100%">
								<thead>
								<tr>
									<td>Cuenta Destino</td>
									<td>Persona Origen</td>
									<td>Cuil Origen</td>
									<td>Fecha</td>
									<td>Monto</td>
								</tr>
								</thead>
						<tbody>								
								<c:forEach var="subTransferencias" items="${transferencias}">
									<c:forEach var="transferencia" items="${subTransferencias}">
										<tr>
											<td>${transferencia.getCuentaOrigen().getCodigo()}</td>
											<td>${transferencia.getCuentaOrigen().getCliente().getApellidoNombre()}</td>
											<td>${transferencia.getCuentaOrigen().getCliente().getCuil()}</td>
											<td>${transferencia.getFechaAlta()}</td>
											<td>${transferencia.getMonto()}</td>
											<!--
											<td>
											<form method="get" action="ModificarCliente.html">
												<button id="btnSubmit" type="submit"
													name="idCliente"
													value="" style="height: 60px; width: 60px">
													Consultar
												</button>
											</form>
											</td>
											-->
										</tr>
									</c:forEach>
								</c:forEach>
								</tbody>
								</table>
							</div>
					</div>
						<form method="post" action="HomePageCliente.html">
						<div>
							<button id="btnRegistarse" type="submit" class="red_button message_submit_btn trans_300">
								Volver
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
<%@include file="Footer.jsp"%>
	</footer>

	</div>

</body>

</html>