package frgp.utn.edu.ar.DaoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.Dao.LocalidadDao;
import frgp.utn.edu.ar.Dao.ProvinciaDao;
import frgp.utn.edu.ar.entidad.Localidad;
import frgp.utn.edu.ar.entidad.Provincia;



public class ProvinciaDaoImpl implements ProvinciaDao  {
	@Autowired
	private Conexion<Provincia> cn;
	
	public ProvinciaDaoImpl()
	{
		cn = new Conexion<Provincia>();
	}

	
	@Override
	public List<Provincia> obtenerTodos() {
		
		 
		Session session = cn.abrirConexion();

		String hql = "from Provincia";
		List<Provincia> tu = (List<Provincia>)session.createQuery(hql).list();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public Provincia obtenerProvincia(Integer id) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Provincia where IdProvincia = '" + id + "'";
		Provincia tu = (Provincia)session.createQuery(hql).uniqueResult();
		
		cn.cerrarSession();
		return tu;
	}
	
}
