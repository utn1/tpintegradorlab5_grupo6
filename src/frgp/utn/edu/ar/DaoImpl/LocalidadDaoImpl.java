package frgp.utn.edu.ar.DaoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.Dao.LocalidadDao;
import frgp.utn.edu.ar.entidad.Localidad;
import frgp.utn.edu.ar.entidad.Persona;

public class LocalidadDaoImpl implements LocalidadDao  {
@Autowired
	private Conexion<Localidad> cn;
	
	public LocalidadDaoImpl()
	{
		cn = new Conexion<Localidad>();
	}

	
	@Override
	public List<Localidad> obtenerTodos() {
		
		 
		Session session = cn.abrirConexion();

		String hql = "from Localidad";
		List<Localidad> tu = (List<Localidad>)session.createQuery(hql).list();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public Localidad obtenerLocalidad(Integer CP) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Localidad where CP = '" + CP + "'";
		Localidad tu = (Localidad)session.createQuery(hql).uniqueResult();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public boolean InsertarLocalidad(Localidad categoria) {
		
		cn.add(categoria);
		return true;
	}


}
