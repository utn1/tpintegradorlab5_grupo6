<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<c:url value="/resources/css/contact_styles.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />"
	type="text/css" rel="stylesheet">

	

	
	<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"
	type="text/css" rel="stylesheet">
	
		<link href="<c:url value="/resources/css/style.css" />"
	type="text/css" rel="stylesheet">
<!--------------------------------stilo grilla----------------- -->
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
<!-- ------------------fin estilo grilla------------------ -->


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
	function isNumberKey(evt)
	{
	   var charCode = (evt.which) ? evt.which : event.keyCode
	   if (charCode > 31 && (charCode < 48 || charCode > 57))
	      return false;
	
	   return true;
	}

	function isLetterKey(evt)
	{
	   var charCode = (evt.which) ? evt.which : event.keyCode
	   if ( (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) )
	      return false;
	
	   return true;
	}
	
	function ValidateEmail(email) 
	{
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	}
	
	function Submit(){
		form = document.getElementById("form");
		email = document.getElementById("txtMail").value;
		if(ValidateEmail(email)){
			form.submit();
		}
		else{
			alert("Email incorrecto");
		}			
	}

	$(document).ready(function($) {

		var d = new Date();
		//d.setDate(d.getDate() -30);
		//filtroDesde=new Date($("#HdfiltroDesde").val());
		filtroDesde=d;

		
        $('#datetimepicker6').datetimepicker({
        	//format: 'YYYY-MM-DD',
            defaultDate:filtroDesde
        })
	});
</script>

	
</head>

<body>

	<div class="super_container">

		<header class="header trans_300">
		 <%@include file="Menu.jsp"%>
		</header>
		<div class="container contact_container">
			<div class="row">
				<div class="col">

				</div>
			</div>

			<div class="row">
				<div class="col-lg-6 get_in_touch_col">
					<div class="get_in_touch_contents">
						<h1>Alta Usuario</h1>
						<form id = "form" method="get" action="GuardarUsuario.html">
							<div style="">
								<p>Perfil: Cliente</p>
								<input type="hidden" name="txtPerfil" value=2>
								<!--
								<select id="SelectPerfil" class="form_input input_name input_ph"
									type="select" name="SelectPerfil" required="required"
									data-error="Perfil is required.">
									<option value="-1">Seleccione</option>
									<c:forEach var="varLocalidad" items="${listaPerfil}">
										<option value="${varLocalidad.getIdPerfil()}">${varLocalidad.getPerfil()}</option>
									</c:forEach>
								</select>
								<input type="hidden" name="hiddenPerfil" value="${usuario.getPerfil().getIdPerfil()}">
								-->
							</div>
							<div style="">
								<p>Apellido:</p>
								<input id="txtApellido" class="form_input input_name input_ph onlyLetter"
									type="text" name="txtApellido" value="${apellido}" placeholder="Apellido"
									required="required" data-error="Apellido is required."
									maxlength="50"
									onkeypress="return isLetterKey(event)">
							</div>
							<div style="">
								<p>Nombre:</p>
								<input id="txtNombre" class="form_input input_name input_ph onlyLetter"
									type="text" name="txtNombre" value="${nombre}" placeholder="Nombre"
									required="required" data-error="Nombre is required."
									maxlength="50"
									onkeypress="return isLetterKey(event)">
							</div>

							<div style="">
								<p>DNI:</p>
							     <c:if test = "${DniDuplicado!=null}">
								  <h4><span style="color:Red">Ya existe un usuario con este Dni.</span></h4> 
							     </c:if>
								<input id="txtDNI" class="form_input input_name input_ph"
									type="text" name="txtDNI" value="${dni}" placeholder="DNI"
									required="required" data-error="DNI is required."
									maxlength="8"
									onkeypress="return isNumberKey(event)">
							</div>

							<div style="">
								<p>Fecha de Nacimiento:</p>
			    <div class='input-group date' id='datetimepicker6' >
		                   <input type='text' class="form-control" name="txtFechaNac" />
		                   <span class="input-group-addon">
		                       <span class="glyphicon glyphicon-calendar"></span>
		                   </span>
		               </div>
								
							</div>
							<div style="">
								<p>Localidad:</p>
								
								<select id="SelectLocalidad"
									class="form_input input_name input_ph" type="select"
									name="SelectLocalidad" required="required"
									data-error="Localidad is required.">
									<option value="-1">Seleccione</option>
									<c:forEach var="varLocalidad" items="${listaLocalidades}">
										<option <c:if test="${varLocalidad.getCP() eq cp}">selected="selected"</c:if>   value="${varLocalidad.getCP()}">${varLocalidad.getLocalidad()}</option>
									</c:forEach>
								</select>
								
								<!--
								<select id="SelectLocalidad"
									class="form_input input_name input_ph onlyLetter" type="select"
									name="SelectLocalidad" required="required"
									data-error="Localidad is required."
									maxlength="100"
									onkeypress="return isLetterKey(event)">
									<option value="-1">Seleccione</option>
									<c:forEach var="varLocalidad" items="${listaLocalidades}">
										<option value="${varLocalidad.getCP()}">${varLocalidad.getLocalidad()}</option>
									</c:forEach>
								</select>
								-->
							</div>
							<div style="">
								<p>Direccion:</p>
								<input id="txtDireccion" class="form_input input_name input_ph"
									type="text" name="txtDireccion" value="${direccion}" placeholder="Direccion"
									maxlength="100"
									required="required" data-error="Direccion is required.">
							</div>
							<div style="">
								<p>Direccion Nro:</p>
								<input id="TxtDireccionNro" class="form_input input_name input_ph"
									type="text" name="TxtDireccionNro" value="${direccionNro}" placeholder="Nro Direccion"
									maxlength="10"
									required="required" data-error="Direccion Nro is required."
									onkeypress="return isNumberKey(event)">
							</div>
							<div style="">
								<p>Sexo:</p>
								<select id="txtSexo" class="form_input input_name input_ph"
									type="select" name="txtSexo" required="required"
									data-error="Perfil is required.">
										<option value="Masculino">Masculino</option>
										<option value="Femenino">Femenino</option>
										<option value="No Informa">No Informa</option>
								</select>
							</div>
							<div style="">
								<p>Mail:</p>
								<input id="txtMail" class="form_input input_name input_ph"
									type="email" name="txtMail" value="${mail}" placeholder="Mail"
									maxlength="100"
									required="required" data-error="Mail is required.">
							</div>
							<div style="">
								<p>Cuil:</p>
								<input id="txtCuil" class="form_input input_name input_ph onlyNumber"
									type="text" name="txtCuil" value="${cuil}" placeholder="Cuil"
									required="required" data-error="Cuil is required."
									maxlength="14"
									onkeypress="return isNumberKey(event)">
							</div>
							<div style="">
								<p>Codigo Usuario:</p>
								<c:if test = "${CodigoDuplicado!=null}">
							       <h4><span style="color:Red">Ya existe un usuario con ese codigo, por favor elija otro.</span></h4> 
							       </c:if>
								<input id="txtCodigo" class="form_input input_name input_ph"
									type="text" name="txtCodigo" value="${codigo}" placeholder="Codigo"
									maxlength="50"
									required="required" data-error="Codigo is required.">
							</div>
							<div style="">
								<p>Password:</p>
								<input id="txtPassword" class="form_input input_name input_ph"
									type="text" name="txtPassword" value="${password}" placeholder="Password"
									maxlength="50"
									required="required" data-error="Password is required.">
							</div>
							<div>
								<button id="review_submit"  type="submit"
									class="red_button message_submit_btn trans_300" name="btnSubmit" value="alta">Aceptar</button>
								<!--<input type="button" name="btnSubmit" onclick="Submit()" value="alta"/>-->
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>


		<!-- Footer -->

		<footer class="footer"> <%@include file="Footer.jsp"%>
		</footer>

	</div>

</body>

</html>