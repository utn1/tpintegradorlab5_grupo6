<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/contact_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />" type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">

<script type="text/javascript">
	$(document).ready(
			function($) {
				debugger;
				$('#example').DataTable(
				{
					"language" : {
						"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
					}
				});
			    $(".onlyNumber").on("keypress keyup blur", function (event) { //Clase onlyNumber para inputs que reconozcan solo texto numerico
			        $(this).val($(this).val().replace(/[^\d].+/, ""));
			        if ((event.which < 48 || event.which > 57)) {
			            event.preventDefault();
			        }
			    });
			});
	
	$(document).ready(
			function($) {
				debugger;
				$('#example').DataTable(
				{
					"language" : {
						"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
					}
				});
			    $(".onlyLetter").on("keypress keyup blur", function (event) {
			        $(this).val($(this).val().replace(/[^\d].+/, ""));
			        if ( (event.which < 65 || event.which > 90) && (event.which < 97 || event.which > 122) ) {
			            event.preventDefault();
			        }
			    });
			});
</script>


</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

		<!-- Main Navigation -->

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<a href="#">bank<span>utn</span></a>
						</div>
						<nav class="navbar">
							<ul class="navbar_menu">
								<li><a href="#">Inicio</a></li>
								<li><a href="#">Mi cuenta</a></li>
								<li><a href="contact.html">contact</a></li>
							</ul>
							<ul class="navbar_user">
								<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></li>
							</ul>
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>

	</header>
	<div class="container contact_container">
		<div class="row">
			<div class="col">

				<!-- Breadcrumbs -->

				<div class="breadcrumbs d-flex flex-row align-items-center">
					<ul>
						<li><a href="index.html">Home</a></li>
						<li class="active"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Prestamos</a></li>
					</ul>
				</div>

			</div>
		</div>

		<!-- Contact Us -->

		<div class="row">
			<div class="col-lg-6 get_in_touch_col">
				<div class="get_in_touch_contents">
					<h1>Nuevo Cliente</h1>
					<form method="get" action="procesar_alta_cliente.html">

						<div style="float">
							<p>Codigo:</p>
							<input id="txtCodigo" class="form_input input_name input_ph" type="text" name="txtCodigo" placeholder="Importe" required="required" data-error="Codigo is required.">
						</div>

						<div style="float">
							<p>Apellido:</p>
							<input id="txtApellido" class="form_input input_name input_ph onlyLetter" type="text" name="txtApellido" placeholder="Apellido" required="required" data-error="Apellido is required.">
						</div>

						<div style="float">
							<p>Nombre:</p>
							<input id="txtNombre" class="form_input input_name input_ph onlyLetter" type="text" name="txtNombre" placeholder="Nombre" required="required" data-error="Nombre is required.">
						</div>

						<div style="float">
							<p>DNI:</p>
							<input id="txtDNI" class="form_input input_name input_ph onlyNumber" type="text" name="txtDNI" placeholder="DNI" required="required" data-error="DNI is required.">
						</div>

						<div style="float">
							<p>Fecha de Nacimiento:</p>
							<input id="txtFechaNac" class="form_input input_name input_ph onlyNumber" type="text" name="txtFechaNac" placeholder="YYYYMMDD" required="required" data-error="Fecha de Nacimiento is required.">
						</div>

            			<div style="float">
							<p>Nacionalidad:</p>
							<input id="txtNacionalidad" class="form_input input_name input_ph onlyLetter" type="text" name="txtNacionalidad" placeholder="Nacionalidad" required="required" data-error="Nacionalidad is required.">
						</div>

            			<div style="float">
							<p>Provincia:</p>
							<input id="txtProvincia" class="form_input input_name input_ph onlyLetter" type="text" name="txtProvincia" placeholder="Provincia" required="required" data-error="Provincia is required.">
						</div>

            			<div style="float">
							<p>Localidad:</p>
							<input id="txtLocalidad" class="form_input input_name input_ph onlyLetter" type="text" name="txtLocalidad" placeholder="Localidad" required="required" data-error="Localidad is required.">
						</div>

            			<div style="float">
							<p>Direccion:</p>
							<input id="txtDireccion" class="form_input input_name input_ph" type="text" name="txtDireccion" placeholder="Direccion" required="required" data-error="Direccion is required.">
						</div>

            			<div style="float">
							<p>Sexo:</p>
							<input id="txtSexo" class="form_input input_name input_ph" type="text" name="txtSexo" placeholder="Sexo" required="required" data-error="Sexo is required.">
						</div>

			            <div style="float">
							<p>Usuario:</p>
							<input id="txtUsuario" class="form_input input_name input_ph" type="text" name="txtUsuario" placeholder="Usuario" required="required" data-error="Usuario is required.">
						</div>

						<div>
							<button id="review_submit" type="submit" class="red_button message_submit_btn trans_300" value="Submit">Aceptar</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>


	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center">

					</div>
				</div>
				<div class="col-lg-6">
					<div class="footer_social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="footer_nav_container">
						<div class="cr" style="text-align: center;">©2020 Trabajo practico - Inegrador Final</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>
</body>

</html>
