package frgp.utn.edu.ar.Servicios;


import java.util.List;


import frgp.utn.edu.ar.entidad.Perfil;


public interface PerfilServicio {

	 Perfil obtenerPerfil(Integer idPerfil);
	 List<Perfil> obtenerTodos();
}
