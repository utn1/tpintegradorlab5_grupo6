package frgp.utn.edu.ar.DaoImpl;


import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.Dao.CuentaDao;
import frgp.utn.edu.ar.entidad.Cuenta;
import frgp.utn.edu.ar.entidad.TipoCuenta;
import frgp.utn.edu.ar.entidad.Persona;


public class CuentaDaoImpl implements CuentaDao  {
	@Autowired
	private Conexion<Cuenta> cn;

	public CuentaDaoImpl()
	{
		cn = new Conexion<Cuenta>();
	}	

	@Override
	public List<Cuenta> obtenerTodosCuenta() {
		Session session = cn.abrirConexion();
		String hql = "from Cuenta where Activo=1";
		List<Cuenta> tu = (List<Cuenta>)session.createQuery(hql).list();
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public List<Cuenta> obtenerCuentaPorUsuario(Integer id) {
		Session session = cn.abrirConexion();
		String hql = "from Cuenta where persona= '" + id + "' and Activo = 1";
		List<Cuenta> tu = (List<Cuenta>) session.createQuery(hql).list();
		cn.cerrarSession();
		return tu;
	}

	@Override
	public List<Cuenta> obtenerCuentasActivasPorUsuario(Integer id) {
		Session session = cn.abrirConexion();
		String hql = "from Cuenta where persona= '" + id + "' and Activo = 1";
		List<Cuenta> tu = (List<Cuenta>) session.createQuery(hql).list();
		cn.cerrarSession();
		return tu;
	}

	
	@Override
	public boolean insertarCuenta(Cuenta cuenta) {
		boolean resp;
		try {
			cn.add(cuenta);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception"); // Entra en este catch
			ex.printStackTrace();
			resp = false;
		}
		return resp;
	}

	@Override
	public List<TipoCuenta> obtenerTodosTipoCuenta(){
		System.out.println("dao->obtenerTodosTipoCuenta->init");
		Session session = cn.abrirConexion();
		String hql = "from TipoCuenta";
		List<TipoCuenta> list = (List<TipoCuenta>) session.createQuery(hql).list();
		// cn.cerrarSession();
		cn.cerrarSession();
		System.out.println("dao->obtenerTodosTipoCuenta->end");
		return list;
	}

	@Override
	public TipoCuenta obtenerTipoCuenta(int id) {
		Session session = cn.abrirConexion();
		String hql = "from TipoCuenta where IdTipoCuenta='" + id + "'";
		TipoCuenta tcta = (TipoCuenta) session.createQuery(hql).uniqueResult();
		cn.cerrarSession();
		return tcta;
	}

	@Override
	public TipoCuenta obtenerTipoCuenta(String codigo) {
		Session session = cn.abrirConexion();
		String hql = "from TipoCuenta where codigo='" + codigo + "'";
		TipoCuenta tcta = (TipoCuenta) session.createQuery(hql).uniqueResult();
		cn.cerrarSession();
		return tcta;		
	}
	
	@Override
	public Cuenta obtenerCuentaPorCbu(String cbu) {
		Session session = cn.abrirConexion();
		String hql = "from Cuenta where Cbu='" + cbu + "' and Activo = 1";
		Cuenta cuenta = (Cuenta) session.createQuery(hql).uniqueResult();
		cn.cerrarSession();
		return cuenta;		
	}

	@Override
	public Cuenta obtenerCuentaPorIdPersonaIdTipoCuenta(int idPersona, int idTipoCuenta) {
		Session session = cn.abrirConexion();

		String hql = "from Cuenta where Persona.IdPersona='" + idPersona + "' and TipoCuenta.IdTipoCuenta='" + idTipoCuenta + "'";
		Cuenta cuenta = (Cuenta) session.createQuery(hql).uniqueResult();

		cn.cerrarSession();
		return cuenta;
	}
	
	@Override
	public List<Cuenta>obtenerCuentasPorIdPersona(int idPersona) {
		Session session = cn.abrirConexion();
		String hql = "from Cuenta where Persona.IdPersona='" + idPersona + "'";
		List<Cuenta> cuenta = (List<Cuenta>) session.createQuery(hql).list();
		cn.cerrarSession();
		return cuenta;		
	}
	
	@Override
	public Cuenta obtenerCuentaPorId(int id) {
		
		Session session = cn.abrirConexion();


		String hql = "from Cuenta where IdCuenta='" + id + "'";
		Cuenta cuenta = (Cuenta) session.createQuery(hql).uniqueResult();
		cn.cerrarSession();
		return cuenta;
	}

	@Override
	public boolean actualizarCuenta(Cuenta cuenta) {
		boolean resp;
		try {
			cn.update(cuenta);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception"); // Entra en este catch
			resp = false;
		}
		return resp;		
	}

	@Override
	public boolean actualizarCuentas(List<Cuenta> cuentas) {
		boolean resp;
		try {
			cn.updateAll(cuentas);
			resp = true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception");
			resp = false;
		}
		return resp;
	}
}

