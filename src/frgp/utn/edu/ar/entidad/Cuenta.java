package frgp.utn.edu.ar.entidad;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="Cuenta")

public class Cuenta implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int IdCuenta;
	
	@Column(name="codigo")
	private String codigo;

	@Column(name="descripcion")
	private String descripcion;

	@Column(name="Cbu", unique=true)
	private String Cbu;
	
	@Column(name="Saldo")
	private double Saldo;
	
	@Column(name="nro_cuenta", unique=true)
	private String NroCuenta;
	
	@Column(name="NombreCuenta")
	private String NombreCuenta;
	
	@Column(name="FechaAlta")
	private Date FechaAlta;
	
	@Column(name="Activo")
	private Boolean Activo;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="IdtipoCuenta")
	private TipoCuenta TipoCuenta;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="persona")
	private Persona Persona;

	public int getId() {
		return IdCuenta;
	}

	public void setId(int id) {
		IdCuenta = id;
	}

	public TipoCuenta getTipoCuenta() {
		return TipoCuenta;
	}

	public void setTipoCuenta(TipoCuenta tipoCuenta) {
		TipoCuenta = tipoCuenta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Persona getCliente() {
		return Persona;
	}

	public void setCliente(Persona Persona) {
		this.Persona = Persona;
	}

	public String getCbu() {
		return Cbu;
	}

	public void setCbu(String cbu) {
		Cbu = cbu;
	}

	public double getSaldo() {
		return Saldo;
	}

	public void setSaldo(double saldo) {
		Saldo = saldo;
	}

	public String getNroCuenta() {
		return NroCuenta;
	}

	public void setNroCuenta(String nroCuenta) {
		NroCuenta = nroCuenta;
	}

	public String getNombre() {
		return NombreCuenta;
	}

	public void setNombre(String nombre) {
		NombreCuenta = nombre;
	}

	public Date getFechaCreacion() {
		return FechaAlta;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		FechaAlta = fechaCreacion;
	}
	
	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	
}

