
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%> 
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<style type="text/css">
#nav {
list-style:none inside;
margin:0;
padding:0;
text-align:center;
}
#nav li {
display:block;
position:relative;
float:left;
background: coral; /* menu background color */
}
#nav li a {
display:block;
padding:0;
text-decoration:none;
width:200px; /* this is the width of the menu items */
line-height:35px; /* this is the hieght of the menu items */
color:#ffffff; /* list item font color */
}
#nav li li a {font-size:80%;} /* smaller font size for sub menu items */
#nav li:hover {background:#003f20;} /* highlights current hovered list item and the parent list items when hovering over sub menues */
#nav ul {
position:absolute;
padding:0;
left:0;
display:none; /* hides sublists */
}
#nav li:hover ul ul {display:none;} /* hides sub-sublists */
#nav li:hover ul {display:block;} /* shows sublist on hover */
#nav li li:hover ul {
display:block; /* shows sub-sublist on hover */
margin-left:200px; /* this should be the same width as the parent list item */
margin-top:-35px; /* aligns top of sub menu with top of list item */
}


divLogueado {
    color: coral;
}
</style>
</head>
<body>

 <script type="text/javascript">

$(document).ready(function($) {
debugger;
//Agarro el idUsuario del usuario Logueado del hidden
var idUserSession=$("#HdIdUsuarioSession").val();
var NombreUsuarioSession=$("#HdNombreUsuario").val();

if(idUserSession==null){ // si es null es por que se perdio la sesion entonces lo mando al login
	document.getElementById("BtnSalir").click();
}
else{
	var name=document.getElementById("NameUsuarioLogueado");
	name.textContent=NombreUsuarioSession.toString();
}


//Teste de funcionamiento de la session
// if(idUserSession){ 
// 	 alert(idUserSession);
// 	}

});
   </script>

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<a href="<c:url value="/redirigir_HomePage.html" />">bank<span>utn</span></a>
						</div>
						<nav class="navbar">
							<ul class="navbar_menu" id="nav">
							<%if(session.getAttribute("SessionPerfilUsuario").equals("administrador")) {%>
								<!--Menu Administrador -->
								<li><a href="#">Clientes</a>
								<ul>
									<li><a href="<c:url value="/Lista_Cuenta_Usuarios.html" />">Lista de cuenta</a></li>
									<li><a href="<c:url value="/redirigir_AddEditUsuario.html" />">Alta Cliente</a></li>
									<li><a href="<c:url value="/redirigir_AltaCuenta.html" />">Nueva Cuenta</a></li>
									<li><a href="<c:url value="/Lista_Cuenta_Usuarios.html" />">Listado Cuenta</a></li>
									<li><a href="<c:url value="/listadoClientes.html" />">Listado Cliente</a></li>
									
									
								</ul>
								</li>
								<li><a href="#">Prestamos</a>
								<ul>

									<li><a href="<c:url value="/Listado_Prestamos.html" />">Listado Solicitudes Prestamos</a></li>
								</ul>
								</li>
								<li><a href="#">Informe</a>
								<ul>
									<li><a href="<c:url value="/InformeDeGestion.html" />">Gestion Prestamos </a></li>
								</ul>
								</li>
								 <% }else{%>
								<!--Menu CLIENTE -->
								<li><a href="#">Prestamo</a>
								<ul>
									<li><a href="<c:url value="/procesar_accion_cliente.html?btnSubmit=nuevoPrestamo" />">Nuevo Prestamo</a></li>
									<li><a href="<c:url value="/procesar_accion_cliente.html?btnSubmit=listadoPrestamos" />">Listado Prestamos</a></li>
									
								</ul>
								</li>
								<li><a href="#">Transferencia</a>
								<ul>
									<li><a href="<c:url value="/procesar_accion_cliente.html?btnSubmit=nuevaTransferencia" />">Nueva Transferencia</a></li>
									<li><a href="<c:url value="/procesar_accion_cliente.html?btnSubmit=listadoTransferencias" />">Listado Transferencias</a></li>
								</ul>
								</li>
								<% }; %>
							</ul>
							<ul class="navbar_user">
								<div class="divLogueado"> Logueado: <span id="NameUsuarioLogueado" name="NameUsuarioLogueado">-</span> &nbsp; &nbsp;<a href="<c:url value="/DesloguearUsuario.html" />" id="BtnSalir" name="BtnSalir"> Salir</a>
								</div>
								
								
							</ul>
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
		 <input type="hidden" id="HdIdUsuarioSession" value='${SessionIdUsuario}'/>
		 <input type="hidden" id="HdNombreUsuario" value='${SessionApeNomUsuario}'/>
</body>
</html>
