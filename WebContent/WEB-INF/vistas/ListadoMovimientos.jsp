<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="<c:url value="/resources/css/contact_styles.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />"
	type="text/css" rel="stylesheet">

	

	
	<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"
	type="text/css" rel="stylesheet">
	
		<link href="<c:url value="/resources/css/style.css" />"
	type="text/css" rel="stylesheet">
<!--------------------------------stilo grilla----------------- -->
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>


<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>
<!-- ------------------fin estilo grilla------------------ -->


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


<style type="text/css">

.form-inline {
    display: -ms-flexbox;
    /* display: flex; */
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    -ms-flex-align: center;
    align-items: center;
}!important

</style>


<script type="text/javascript">
	$(document).ready(function($) {


			debugger;
			$('#example').DataTable(
			{
				"language" : {
					"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});

			filtroDesde=new Date($("#HdfiltroDesde").val());
			filtroHasta=new Date($("#HdfiltroHasta").val());
			
            $('#datetimepicker6').datetimepicker({
                defaultDate:filtroDesde
            })
            $('#datetimepicker7').datetimepicker({
                defaultDate:filtroHasta
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });


		});
</script>
</head>

<body>

	<div class="super_container">

		<!-- Header -->

		<header class="header trans_300"> <!-- Main Navigation -->

		 <%@include file="Menu.jsp"%>

		</header>
		<div class="container contact_container">

			<div class="row">
				<div>
					<h1>Listado de Movimientos ${userName}</h1>
				</div>
			</div>
				<form method="post" action="Redirigir_MovimientosPorCuenta.html">
			<div class="row">
				<div class="col-sm-2">
				  <label>Cuenta:</label>
					<select id="txtTipoCuenta" class="form_input input_name input_ph" type="select" name="idCuenta" placeholder="Cuenta" required="required" data-error=" Cuenta is required.">
						<c:forEach var="Cuenta" items="${listaCuentas}">
							<option value="${Cuenta.getId()}"  ${Cuenta.getId() == filtroIdCuenta ? 'selected' : ''} >${Cuenta.getNombre()}</option>
						</c:forEach>
					</select>
					 </div>
				</div>
				
			<div class="row">

				<div class="col-sm-4">
				  <label>Desde:</label>
				    <div class='input-group date' id='datetimepicker6' >
		                   <input type='text' class="form-control" name="Desde" />
		                   <span class="input-group-addon">
		                       <span class="glyphicon glyphicon-calendar"></span>
		                   </span>
		               </div>
				</div>
				<div class="col-sm-4">
				                <label>Hasta:</label>
		               <div class='input-group date' id='datetimepicker7'>
		                   <input type='text' class="form-control"  name="Hasta"/>
		                   <span class="input-group-addon">
		                       <span class="glyphicon glyphicon-calendar"></span>
		                   </span>
		               </div>
				</div>
				<div lass="col-sm-4">
				          <label></label>
		               <div class='input-group date' id='datetimepicker9'>
						<button type="submit" name="btnBuscar"
				value="${mov.getIdMovimiento()}">Buscar</button>
		               </div>

				</div>
			</div>
			</form>
			<div  class="row">
<br>
			</div>
			<div class="row">
				<div >
					<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
					<table id="example" class="table table-striped table-bordered"
						style="width: 100%">
						<thead>
							<tr>
								<td>Cuenta</td>
								<td>Importe</td>
								<td>Fecha Alta</td>
								<td>Detalle</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="mov" items="${listaMovimientos}">
								<tr>
									
									<td>${mov.getCuenta().getNombre()}</td>
									<td>${mov.getImporte()}</td>
									<td>${mov.getFechaAlta()}</td>
									<td>${mov.getDetalleMovimiento()}</td>
								</tr>
							</c:forEach>
						</tbody>

					</table>
				</div>
			</div>
			<form method="post" action="HomePageCliente.html">
				<div>
					<button id="btnRegistarse" type="submit"
						class="red_button message_submit_btn trans_300">Volver</button>
				</div>
			</form>
		</div>
	</div>

	<!-- Footer -->


<%@include file="Footer.jsp"%>


		 <input type="hidden" id="HdfiltroIdCuenta" value='${filtroIdCuenta}'/>
		 <input type="hidden" id="HdfiltroDesde" value='${filtroDesde}'/>
		 <input type="hidden" id="HdfiltroHasta" value='${filtroHasta}'/>
	
		 

</body>

</html>
