package frgp.utn.edu.ar.entidad;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Provincia")
public class Provincia implements Serializable {
	//Implementar serializable
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IdProvincia")
	private int IdProvincia;
	
	@Column(name="Provincia")
	private String Provincia;
	
	@Column(name="Activo")
	private Boolean Activo;
	
	
	
	@Override
	public String toString() {
		return "Provincia [Provincia=" + Provincia + ", Provincia=" + Provincia + ", Activo=" + Activo + "]";
	}



	public int getIdProvincia() {
		return IdProvincia;
	}



	public String getProvincia() {
		return Provincia;
	}



	public void setProvincia(String provincia) {
		Provincia = provincia;
	}



	public Boolean getActivo() {
		return Activo;
	}



	public void setActivo(Boolean activo) {
		Activo = activo;
	}



	public Provincia() {
		
	}
}
