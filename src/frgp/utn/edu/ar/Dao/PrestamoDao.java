package frgp.utn.edu.ar.Dao;
import java.util.Date;
import java.util.List;

import frgp.utn.edu.ar.entidad.Cuenta;
import frgp.utn.edu.ar.entidad.Prestamo;
import frgp.utn.edu.ar.entidad.Usuario;

public interface PrestamoDao {
	public List<Prestamo> obtenerTodosPrestamo();
	public List<Prestamo> obtenerPrestamoPorUsuario(Integer id);
	public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta);
	public boolean actualizarEstadoPrestamoE(Prestamo prestamo);
	public Prestamo getPrestamoById(int id);
	public List<Prestamo> obtenerTodosPrestamoByEstadoDesdeHasta(Integer id,Date desde,Date hasta);
}

