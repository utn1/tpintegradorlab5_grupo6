package frgp.utn.edu.ar.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.context.ConfigurableApplicationContext;

import frgp.utn.edu.ar.Servicios.LocalidadServicio;
import frgp.utn.edu.ar.Servicios.PerfilServicio;
import frgp.utn.edu.ar.Servicios.PersonaServicio;
import frgp.utn.edu.ar.Servicios.UsuarioServicio;
import frgp.utn.edu.ar.Servicios.CuentaServicio;
import frgp.utn.edu.ar.Servicios.PrestamoServicio;
import frgp.utn.edu.ar.Servicios.TransaccionServicio;
import frgp.utn.edu.ar.Servicios.EstadoPrestamoServicio;
//import frgp.utn.edu.ar.entidades.*;
import frgp.utn.edu.ar.entidad.*;
import frgp.utn.edu.ar.resources.*;

@Controller
public class PaginaController {
	
	public PersonaServicio servicePersona;
	// @Autowired
	public LocalidadServicio serviceLocalidad;

	public UsuarioServicio serviceUsuario;
	
	public PerfilServicio servicePerfil;

	private ApplicationContext appContext;

	public CuentaServicio serviceCuenta;
	
	public PrestamoServicio servicePrestamo;
	
	public TransaccionServicio serviceTransaccion;
	
	public EstadoPrestamoServicio servicioEstadoPrestamo;

	public PaginaController() {
		this.appContext = new AnnotationConfigApplicationContext(Config.class);
	}

	public void closeAppContext() {
		((ConfigurableApplicationContext)(appContext)).close();
	}
	
	@RequestMapping("index.html")
	public ModelAndView eventoRedireccionarPag1()
	{
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		MV.setViewName("Login");
		return MV;
	}

	@RequestMapping("index.jsp")
	public ModelAndView eventoRedireccionarPag1Jsp()
	{
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		MV.setViewName("Login");
		return MV;
	}

	
	@RequestMapping("redirigir_AltaCuenta.html")
	public ModelAndView redirigirNuevaCuenta() {
		System.out.println("redirigirNuevaCuenta");
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		this.serviceCuenta = (CuentaServicio) appContext.getBean("cuentaServicioImpl");
		List<TipoCuenta> list = this.serviceCuenta.obtenerTodosTipoCuenta();
		MV.addObject("tiposCuentas", list);
		MV.addObject("dni", "");
		MV.addObject("saldoInicial", "");
		MV.setViewName("AltaCuenta");
		return MV;
	}

	
	@RequestMapping("redirigir_AltaAdministrador.html")
	public ModelAndView IrAlta(String perfil)
	{
		System.out.println(perfil);
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		if(perfil.equals("RegistarAdministrador")) {
			MV.setViewName("AltaAdministrador");
		}
		else if(perfil.equals("Cliente")) {
			MV.setViewName("AltaCliente");
		}
		return MV;
	}

	@RequestMapping("SaveAdmin.html")
	public ModelAndView SaveAdmin(String txtCodigo, String txtApellido, String txtNombre, String DNI, String txtFechaNac, String txtNacionalidad,String txtProvincia,String txtLocalidad,String txtSexo) {
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		MV.setViewName("HomePageAdmin");
		return MV;
	}
		
	@RequestMapping("Lista_Cuenta_Usuarios.html")
	public ModelAndView CuentasUsuarios() {
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		this.serviceCuenta = (CuentaServicio) appContext.getBean("serviceCuentaBean");
			
			List<Cuenta> ListaCuentaUsuarios =(List<Cuenta>)this.serviceCuenta.obtenerTodosCuenta();
			MV.addObject("ListaCuentaUsuarios", ListaCuentaUsuarios);
			System.out.println("test cuenta usuarios");
			MV.setViewName("ListaCuentaUsuarios");
			((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}

	@RequestMapping("Listado_Prestamos.html")
	public ModelAndView ListaPrestamos()
	{
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		this.servicePrestamo = (PrestamoServicio) appContext.getBean("servicePrestamoBean");
			
		List<Prestamo> ListaPrestamo =(List<Prestamo>)this.servicePrestamo.obtenerTodosPrestamo();
		MV.addObject("prestamos", ListaPrestamo);
		System.out.println("test Prestamos:" + ListaPrestamo.toArray().length);
		System.out.println("test Prestamos:0 ");
		MV.setViewName("ListadoPrestamos");
		return MV;
	}
	
	
	@RequestMapping("redirigir_Informes.html")
	public ModelAndView redirigirInforme(String btnAccion, HttpServletRequest request) {
		ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);
		ModelAndView MV = (ModelAndView) appContext.getBean("modelAndView");
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);		

		MV.setViewName("ListadoInformes");
		
		return MV;
	}
	
	@RequestMapping("redirigir_informeSeleccionado.html")
	public ModelAndView redirigir_informeSeleccionado(String txtInforme, Date desde, Date hasta, HttpServletRequest request)
	{
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		
		HttpSession misession = request.getSession(true);
		int idUsuario = (int) misession.getAttribute("SessionIdUsuario");
		misession.setAttribute("SessionIdUsuario", idUsuario);		

		if(txtInforme.contentEquals("prestamos_solicitados")) {
			
			this.servicePrestamo = (PrestamoServicio) appContext.getBean("servicePrestamoBean");
				
			List<Prestamo> listaPrestamos =(List<Prestamo>)this.servicePrestamo.obtenerTodosPrestamo();

			double promedioCuotas = 0;
			int auxCantCuotasTotal = 0;
			double totalPrestado = 0;
			for(Prestamo prestamo : listaPrestamos) {
				auxCantCuotasTotal += prestamo.getCuotas();
				totalPrestado += prestamo.getMonto();
			}
			promedioCuotas = auxCantCuotasTotal / listaPrestamos.size();
			MV.addObject("prestamos", listaPrestamos);
			MV.addObject("promedioCuotas", promedioCuotas);
			MV.addObject("totalPrestado", totalPrestado);

			MV.setViewName("InformePrestamosSolicitados");
		}
		else {
			MV.setViewName("HomePageAdmin");
		}
		
		return MV;
	}

	
	
	
	@RequestMapping("Estado_Prestamo_Actualizar.html")
	public ModelAndView EstadoPrestamoActualizar(String idPrestamo)
	{
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");

		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		this.servicePrestamo = (PrestamoServicio) appContext.getBean("servicePrestamoBean");
		this.servicioEstadoPrestamo = (EstadoPrestamoServicio) appContext.getBean("serviceEstadoPrestamoBean");
		Prestamo prestamo = (Prestamo) appContext.getBean("BeanPrestamo");
		prestamo = this.servicePrestamo.getPrestamoById(Integer.parseInt(idPrestamo));

		List<Prestamo> ListaPrestamo =(List<Prestamo>)this.servicePrestamo.obtenerPrestamoPorUsuario(Integer.parseInt(idPrestamo));
		List<EstadoPrestamo> ListaEstadoPrestamo =(List<EstadoPrestamo>)this.servicioEstadoPrestamo.obtenerTodos();
		MV.addObject("usuario", prestamo);
		MV.addObject("ListaEstadoPrestamoCombo", ListaEstadoPrestamo);

		MV.setViewName("EstadoPrestamo");
		return MV;
	}
	
	@RequestMapping("Estado_Prestamo_Actualizar_Guardar.html")
	public ModelAndView Estado_Prestamo_Actualizar_Guardar(String idPrestamo, String idEstado)
	{
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		this.servicePrestamo = (PrestamoServicio) appContext.getBean("servicePrestamoBean");
		this.servicioEstadoPrestamo = (EstadoPrestamoServicio) appContext.getBean("serviceEstadoPrestamoBean");
		this.serviceCuenta = (CuentaServicio) appContext.getBean("serviceCuentaBean");
		try {
		
			Prestamo prestamo = (Prestamo) appContext.getBean("BeanPrestamo");
			prestamo = this.servicePrestamo.getPrestamoById(Integer.parseInt(idPrestamo));

			EstadoPrestamo estadoPrestamo = (EstadoPrestamo) appContext.getBean("BeanEstadoPrestamo");
			estadoPrestamo = this.servicioEstadoPrestamo.obtenerEstadoPrestamo(Integer.parseInt(idEstado));
			
			prestamo.setEstado(estadoPrestamo);
			this.servicePrestamo.actualizarPrestamoE(prestamo);
			
			//Generar movimiento de autorazaci�n de prestamos
			GenerarM(prestamo.getCliente().getIdPersona(), prestamo.getMonto(), estadoPrestamo.getCodigo(), prestamo.getCuentaDestino().getId());

			MV.addObject("usuario", prestamo);
		
			System.out.println("Todo va bien");
			MV.setViewName("AltaPrestamoEstadoOk");
		}
		catch(Exception e) {
			System.out.println("Error procesarAccion" + e.getMessage());
			MV.addObject("mensajeError", e.getMessage());
		}
		return MV;
	}
	
	public void GenerarM(int idCliente, Double monto, String codigoEstado, int idCuenta)
	{
		ApplicationContext appContext2 = new AnnotationConfigApplicationContext(Config.class);
		this.serviceTransaccion = (TransaccionServicio) appContext2.getBean("transaccionServicioImpl");
		this.serviceCuenta = (CuentaServicio) appContext2.getBean("cuentaServicioImpl");
		
		Movimiento movimiento = (Movimiento) appContext2.getBean("movimiento");
		Cuenta cuenta = (Cuenta) this.serviceCuenta.obtenerCuentaPorId(idCuenta);

		if(codigoEstado.equals("aceptado")) {
			System.out.println("paso: " + cuenta.getTipoCuenta().getIdTipoCuenta() + "->" + codigoEstado );
			
			movimiento.setDetalleMovimiento("Prestamo");
			movimiento.setImporte(monto);
			movimiento.setFechaAlta(new Date());
			movimiento.setTipoMovimiento(this.serviceTransaccion.obtenerTipoMovimiento("alta_prestamo"));
			movimiento.setCuenta(cuenta);
			this.serviceTransaccion.insertarMovimiento(movimiento);
			
			cuenta.setSaldo(cuenta.getSaldo() + monto);
			this.serviceCuenta.actualizarCuenta(cuenta);
		}
	}
	@RequestMapping("InformeDeGestion.html")
	public ModelAndView InformeDeGestion(Date  Desde,Date  Hasta,HttpServletRequest request) {
		Date FechaDesde;
		Date FechaHasta;
		if(Desde==null) {
			FechaDesde=new java.util.Date();
			//PorDefecto dejo un mes
			FechaDesde=addDays(FechaDesde,-30);
		}
		else {
			FechaDesde=Desde;
		}
		if(Hasta==null) {
			FechaHasta=new java.util.Date();
		}else {
			FechaHasta=Hasta;
		}
		
		ModelAndView MV = (ModelAndView) this.appContext.getBean("modelAndView");
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("frgp/utn/edu/ar/resources/Beans.xml");
		this.serviceCuenta = (CuentaServicio) appContext.getBean("serviceCuentaBean");
		this.servicePrestamo = (PrestamoServicio) appContext.getBean("servicePrestamoBean");
		
	
			System.out.println("test cuenta usuarios");
			
			List<Prestamo> listaPrestamosAceptados =(List<Prestamo>)this.servicePrestamo.obtenerTodosPrestamoByEstadoDesdeHasta(2,FechaDesde,FechaHasta); //Prestamos Aceptados

			List<Prestamo> listaPrestamosTodos =(List<Prestamo>)this.servicePrestamo.obtenerTodosPrestamoByEstadoDesdeHasta(-1,FechaDesde,FechaHasta); //Prestamos Aceptados
			

			double promedioCuotasAceptados = 0;
			int auxCantCuotasTotalAceptados = 0;
			double totalMontoPrestado = 0;
			for(Prestamo prestamo : listaPrestamosAceptados) {
				auxCantCuotasTotalAceptados += prestamo.getCuotas();
				totalMontoPrestado += prestamo.getMonto();
			}
			
			double promedioCuotasTodos = 0;
			int auxCantCuotasTotalTodos = 0;
			double totalMontoSolicitado=0;
			for(Prestamo prestamo : listaPrestamosTodos) {
				auxCantCuotasTotalTodos += prestamo.getCuotas();
				totalMontoSolicitado += prestamo.getMonto();
			}
			promedioCuotasAceptados = auxCantCuotasTotalAceptados / (listaPrestamosAceptados.size()>0?listaPrestamosAceptados.size():1);
			promedioCuotasTodos = auxCantCuotasTotalTodos / (listaPrestamosTodos.size()>0?listaPrestamosTodos.size():1);
			MV.addObject("prestamos", listaPrestamosAceptados);
			MV.addObject("promedioCuotas", promedioCuotasAceptados);
			MV.addObject("promedioCuotasTodos", promedioCuotasTodos);
			MV.addObject("totalMontoPrestado", totalMontoPrestado);
			MV.addObject("totalMontoSolicitado", totalMontoSolicitado);
			MV.addObject("totalPrestamosSolicitados", listaPrestamosTodos.size());
			MV.addObject("totalPrestamosAceptados", listaPrestamosAceptados.size());
			

			
			
			
			
			
			
			//uso esto para devolver la fecha que filtramos e inicializar los datepicker con esta fecha
			String pattern = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String filtroDesde = simpleDateFormat.format(FechaDesde);
			String filtroHasta = simpleDateFormat.format(FechaHasta);

			MV.addObject("filtroDesde", filtroDesde);
			MV.addObject("filtroHasta", filtroHasta);
			MV.setViewName("InformeDeGestion");
			((ConfigurableApplicationContext)(appContext)).close();
		return MV;
	}
	public static Date addDays(Date fecha, int dias) 
	{
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fecha);
		cal.add(Calendar.DATE, dias);
		return cal.getTime();
	}

	
}

