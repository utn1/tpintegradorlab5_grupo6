package frgp.utn.edu.ar.Dao;


import frgp.utn.edu.ar.entidad.Persona;

public interface PersonaDao {

	public boolean insertarPersona(Persona persona);
	public boolean actualizarPersona(Persona persona);
	public boolean actualizarPersona2(Persona persona);
	public Persona getPersonaById(Integer idPersona);
	public Persona obtenerPersonaPorDni(String dni);
}
