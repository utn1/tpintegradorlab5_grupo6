package frgp.utn.edu.ar.DaoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;

import frgp.utn.edu.ar.Dao.PerfilDao;

import frgp.utn.edu.ar.entidad.Perfil;


public class PerfilDaoImpl implements PerfilDao  {
	@Autowired
	private Conexion<Perfil> cn;
	
	public PerfilDaoImpl()
	{
		cn = new Conexion<Perfil>();
	}

	
	@Override
	public List<Perfil> obtenerTodos() {
		
		
		 
		Session session = cn.abrirConexion();

		String hql = "from Perfil";
		List<Perfil> tu = (List<Perfil>)session.createQuery(hql).list();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public Perfil obtenerPerfil(Integer id) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Perfil where IdPerfil = '" + id + "'";
		Perfil tu = (Perfil)session.createQuery(hql).uniqueResult();
		
		cn.cerrarSession();
		return tu;
	}
}
