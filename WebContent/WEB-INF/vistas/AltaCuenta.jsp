<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link href="<c:url value="/resources/css/contact_styles.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />"
	type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"
	type="text/css" rel="stylesheet">
<link
	href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"
	type="text/css" rel="stylesheet">
	
<script type="text/javascript">
	function SubmitVolver(){
		form = document.getElementById("form");
		form.submit();
	}
</script>
</head>

<body>

	<div class="super_container">



		<header class="header trans_300">
		 <%@include file="Menu.jsp"%>
		</header>

		<div class="container contact_container">
			<div class="row">
				<div class="col"></div>
			</div>


			<div class="row">
				<div class="col-lg-6 get_in_touch_col">
					<div class="get_in_touch_contents">
						<h1>Alta de cuenta</h1>
						<form id="form" method="post" action="procesar_AltaCuenta.html">
							<div style="">
								<p>Tipo de cuenta:</p>
								<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
								<select id="txtTipoCuenta"
									class="form_input input_name input_ph" type="select"
									name="txtTipoCuenta" placeholder="Tipo de Cuenta"
									required="required" data-error="Tipo de Cuenta is required.">
									<c:forEach var="tipoCuenta" items="${tiposCuentas}">
										<option value="${tipoCuenta.getIdTipoCuenta()}">${tipoCuenta.getCodigo()}</option>
									</c:forEach>
								</select>
							</div>
							<div style="">
								<p>DNI Cliente:</p>
								<input id="txtDniCliente"
									class="form_input input_name input_ph" type="text"
									name="txtDniCliente" value="${dni}" placeholder="DNI Cliente"
									maxlength="9"
									required="required" data-error="Usuario Cliente is required.">
							</div>

							<button id="review_submit" type="submit"
								class="red_button message_submit_btn trans_300" name="btnSubmit" value="aceptar">Aceptar
							</button>
							
							<input type="button" id="review_submit" class="red_button message_submit_btn trans_300"
							name="btnSubmit" value="volver"
							onclick="SubmitVolver()"/>
							
							<!--
							<button id="review_submit" type="submit"
								class="red_button message_submit_btn trans_300" name="btnSubmit" value="volver">Volver
							</button>
							-->
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Footer -->

	 <%@include file="Footer.jsp"%>

	


</body>

</html>
