package frgp.utn.edu.ar.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@Table(name="prestamos")
public class Prestamo implements Transaccion{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="IdUsuario")
	private Persona Cliente;

	@Column(name="Fecha")
	private Date Fecha;

	@Column(name="Monto")
	private double Monto;
	
	@Column(name="Cuotas")
	private int Cuotas;
	
	@Column(name="Plazo")
	private int Plazo;
	
	@Column(name="monto_por_mes")
	private double MontoPorMes;

	@ManyToOne
	@JoinColumn(name="cuenta_destino")
	private Cuenta cuentaDestino;

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="estado")
	private EstadoPrestamo estado;

	
	public Cuenta getCuentaDestino() {
		return cuentaDestino;
	}

	public void setCuentaDestino(Cuenta cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public Persona getCliente() {
		return Cliente;
	}

	public void setCliente(Persona cliente) {
		Cliente = cliente;
	}

	public Date getFecha() {
		return Fecha;
	}

	public void setFecha(Date fecha) {
		Fecha = fecha;
	}

	public int getCuotas() {
		return Cuotas;
	}

	public void setCuotas(int cuotas) {
		Cuotas = cuotas;
	}

	public int getPlazo() {
		return Plazo;
	}

	public void setPlazo(int plazo) {
		Plazo = plazo;
	}

	public double getMonto() {
		return Monto;
	}

	public void setMonto(double monto) {
		Monto = monto;
	}

	
	public double getMontoPorMes() {
		return MontoPorMes;
	}

	public void setMontoPorMes(double montoPorMes) {
		MontoPorMes = montoPorMes;
	}

	public EstadoPrestamo getEstado() {
		return estado;
	}

	public void setEstado(EstadoPrestamo estado) {
		this.estado = estado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
