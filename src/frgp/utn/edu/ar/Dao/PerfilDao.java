package frgp.utn.edu.ar.Dao;
import java.util.List;


import frgp.utn.edu.ar.entidad.Perfil;


public interface PerfilDao {

	public Perfil obtenerPerfil(Integer idPerfil);
	public List<Perfil> obtenerTodos();
	
}
