package frgp.utn.edu.ar.Servicios;

import java.util.ArrayList;
import java.util.List;

import frgp.utn.edu.ar.entidad.*;

public interface TransaccionServicio {

	public boolean insertarMovimiento(Movimiento movimiento);
	public boolean insertarAltaCuenta(AltaCuenta altaCuenta);
	public boolean insertarTransferencia(Transferencia transferencia);
	public boolean insertarPrestamo(Prestamo prestamo);
	public boolean actualizarPrestamo(Prestamo prestamo);
	public boolean insertarCuota(Cuota cuota);
	public boolean insertarCuotas(ArrayList<Cuota> cuotas);
	public boolean actualizarCuota(Cuota cuota);
	public TipoMovimiento obtenerTipoMovimiento(String codigo);
	public EstadoPrestamo obtenerEstadoPrestamo(String codigo);
	public ArrayList<Transferencia> obtenerTransferenciasCuentaOrigen(int idCuenta);
	public ArrayList<Prestamo> obtenerPrestamosPersonaId(int idPersona);
	public ArrayList<Cuota> obtenerCuotasPrestamo(int idPrestamo);
	public Cuota obtenerCuota(int idCuota);
	public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta);
}
