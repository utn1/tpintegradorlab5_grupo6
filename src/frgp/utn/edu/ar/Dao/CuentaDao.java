package frgp.utn.edu.ar.Dao;
import java.util.List;

import frgp.utn.edu.ar.entidad.Cuenta;
import frgp.utn.edu.ar.entidad.TipoCuenta;

public interface CuentaDao {
	public List<Cuenta> obtenerTodosCuenta();
	public List<Cuenta> obtenerCuentaPorUsuario(Integer id);
	public List<Cuenta> obtenerCuentasActivasPorUsuario(Integer id);
	public boolean insertarCuenta(Cuenta cuenta);
	public List<TipoCuenta> obtenerTodosTipoCuenta();
	public TipoCuenta obtenerTipoCuenta(int id);
	public TipoCuenta obtenerTipoCuenta(String codigo);
	public Cuenta obtenerCuentaPorCbu(String cbu);
	public Cuenta obtenerCuentaPorIdPersonaIdTipoCuenta(int idPersona, int idTipoCuenta);
	public List<Cuenta>obtenerCuentasPorIdPersona(int idPersona);
	public Cuenta obtenerCuentaPorId(int id);
	public boolean actualizarCuenta(Cuenta cuenta);
	public boolean actualizarCuentas(List<Cuenta> cuentas);
}

