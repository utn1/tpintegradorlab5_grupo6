package frgp.utn.edu.ar.DaoImpl;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.Dao.UsuarioDao;
import frgp.utn.edu.ar.entidad.Localidad;
import frgp.utn.edu.ar.entidad.Usuario;
public class UsuarioDaoImpl  implements UsuarioDao {

	@Autowired
	private Conexion<Usuario> cn;
	
	public UsuarioDaoImpl()
	{
		cn = new Conexion<Usuario>();
	}
	
	@Override
	public boolean insertarUsuario(Usuario usu) {
		
		try {
			cn.add(usu);
			return true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception"); // Entra en este catch
			ex.printStackTrace();
			return false;
		}
		
	}
	@Override
	public boolean UpdatearUsuario(Usuario usu) {
		
		try {
			cn.update(usu);
			return true;
		} catch (Exception ex) {
			System.out.print(ex.getMessage() + "inner exception"); // Entra en este catch
			ex.printStackTrace();
			return false;
		}
		
	}
	
	@Override
	public Usuario getUserByUsernameAndPassword(String User,String Pass) {
		System.out.println("dao->getUserByUsernameAndPassword");
		 
		Session session = cn.abrirConexion();
		String hql = "from Usuario where NombreUsuario='" + User + "' and Password='" + Pass + "' and Activo=1";
		Usuario tu = (Usuario)session.createQuery(hql).uniqueResult();
		System.out.println("dao->getUserByUsernameAndPassword.usu: " + tu);
		cn.cerrarSession();
		return tu;
	}
	@Override
	public Usuario getUserByUsername(String User) {
		System.out.println("dao->getUserByUsername");
		 
		Session session = cn.abrirConexion();
		String hql = "from Usuario where NombreUsuario='" + User + "' and Activo=1";
		Usuario tu = (Usuario)session.createQuery(hql).uniqueResult();
		System.out.println("dao->getUserByUsername.usu: " + tu);
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public Usuario getUserById(int id) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from Usuario where IdUsuario='" + id + "'"; //TODO Agregar el activo
		Usuario tu = (Usuario)session.createQuery(hql).uniqueResult();
		
		cn.cerrarSession();
		return tu;
	}
	

	@Override
	public List<Usuario> obtenerClientes() {

	
		
//		 
		Session session = cn.abrirConexion();
		String hql = "from Usuario where IdPerfil=2 and Activo=1";
		List<Usuario> tu = (List<Usuario>)session.createQuery(hql).list();
		

		cn.cerrarSession();
	
		return tu;
	}
	
	@Override
	public boolean borradoLogico(String id) {
		boolean resp = false;
		try {
			 
			Session session = cn.abrirConexion();
			String hql = "from Usuario where IdUsuario='" + id + "'"; //TODO Agregar el activo
			Usuario usu = (Usuario)session.createQuery(hql).uniqueResult();
			usu.setActivo(false);
			session.beginTransaction();
			session.save(usu);
			session.getTransaction().commit();
			resp = true;
			cn.cerrarSession();
		}	
		catch(Exception e) {
			System.out.println(e);
		}
		return resp;
	}
	
	
	
}
	