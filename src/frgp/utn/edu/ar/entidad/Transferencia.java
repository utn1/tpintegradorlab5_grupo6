package frgp.utn.edu.ar.entidad;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;


@Entity
@Table(name="transferencias")
public class Transferencia implements Transaccion{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Id;

	@ManyToOne
	@JoinColumn(name="cuenta_origen")
	private Cuenta CuentaOrigen;

	@ManyToOne
	@JoinColumn(name="cuenta_destino")
	private Cuenta CuentaDestino;

	@Column(name="monto")
	private double Monto;

	@Column(name="fecha_alta")
	private Date FechaAlta;
	
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}
	
	public Date getFechaAlta() {
		return FechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		FechaAlta = fechaAlta;
	}

	public Cuenta getCuentaOrigen() {
		return CuentaOrigen;
	}

	public void setCuentaOrigen(Cuenta cuentaOrigen) {
		CuentaOrigen = cuentaOrigen;
	}

	public Cuenta getCuentaDestino() {
		return CuentaDestino;
	}

	public void setCuentaDestino(Cuenta cuentaDestino) {
		CuentaDestino = cuentaDestino;
	}

	public double getMonto() {
		return Monto;
	}

	public void setMonto(double monto) {
		Monto = monto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
