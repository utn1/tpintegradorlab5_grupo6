function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}



//Máscara para CPF
$(document).ready(function ($) {
    $("#txtCPF").mask("999.999.999-99"); //Mascara para Texbox con id txtCPF
    $(".MaskCelular").mask("99 99999-9999"); //Mascara para text box de celular con clase MaskCelular para poder enmascarar mas de un campo Celular en un mismo form
    $(".MaskPhone").mask("99 9999-9999"); //Mascara para text box de Phone con clase MaskPhone para poder enmascarar mas de un campo Phone en un mismo form
    $(".MaskCPF").mask("999.999.999-99"); //Mascara para text box de CPF con clase MaskCpf para poder enmascarar mas de un campo Cpf en un mismo form
    $('.onlyText').on("keypress keyup blur", function (event) { //Clase onlyText para inputs que reconozcan solo texto
        var arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ";
        var char = keychar = String.fromCharCode(event.which);
        if (arr.indexOf(char) == -1)
            return false;

    });
    $('.onlyCharacter').on("keypress keyup blur", function (event) { //Clase onlyCharacter para inputs que reconozcan solo texto alfanumerico
        var arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ";
        var char = keychar = String.fromCharCode(event.which);
        if (arr.indexOf(char) == -1)
            return false;

    });

    $(".onlyNumber").on("keypress keyup blur", function (event) { //Clase onlyNumber para inputs que reconozcan solo texto numerico
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

});