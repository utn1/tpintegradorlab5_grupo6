package frgp.utn.edu.ar.entidad;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TipoCuenta")
public class TipoCuenta implements Serializable{


	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IdTipoCuenta")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int IdTipoCuenta;
	
	@Column(name="TipoCuenta")
	private String TipoCuenta;
	
	@Column(name="Codigo", unique=true)
	private String Codigo;
	
	public String getCodigo() {
		return Codigo;
	}

	public void setCodigo(String codigo) {
		this.Codigo = codigo;
	}
	@Column(name="Activo")
	private Boolean Activo;
	
	//Constructor vacio
	public TipoCuenta()
	{
		
	}

	public int getIdTipoCuenta() {
		return IdTipoCuenta;
	}

	public void setIdTipoCuenta(int idTipoCuenta) {
		IdTipoCuenta = idTipoCuenta;
	}

	public String getTipoCuenta() {
		return TipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		TipoCuenta = tipoCuenta;
	}

	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}