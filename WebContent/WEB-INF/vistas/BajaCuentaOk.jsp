<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/contact_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />" type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">
</head>

<body>

<div class="super_container">

	<header class="header trans_300">

			 <%@include file="Menu.jsp"%>
	</header>
	<div class="container contact_container">
		<div class="row">
			<div class="col">

			</div>
		</div>

		<!-- Contact Us -->

		<div class="row">
			<div class="col-lg-6 get_in_touch_col">
				<div class="get_in_touch_contents">
					<h1>Eliminacion de cuenta</h1>
					<form method="post" action="Lista_Cuenta_Usuarios.html">
						<h3>La cuenta se dio de baja correctamente.</h3>
  						</div>
							<button id="review_submit" type="submit" class="red_button message_submit_btn trans_300" value="Submit">Aceptar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	
<%@include file="Footer.jsp"%>


<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>
</body>

</html>