package frgp.utn.edu.ar.Dao;

import java.util.ArrayList;
import java.util.List;

import frgp.utn.edu.ar.entidad.AltaCuenta;
import frgp.utn.edu.ar.entidad.EstadoPrestamo;
import frgp.utn.edu.ar.entidad.Transferencia;
import frgp.utn.edu.ar.entidad.Prestamo;
import frgp.utn.edu.ar.entidad.Cuota;
import frgp.utn.edu.ar.entidad.Movimiento;
import frgp.utn.edu.ar.entidad.TipoMovimiento;


public interface TransaccionDao {
	
	public boolean insertarMovimiento(Movimiento movimiento);
	public boolean insertarAltaCuenta(AltaCuenta altaCuenta);
	public boolean insertarTransferencia(Transferencia transferencia);
	public boolean insertarPrestamo(Prestamo prestamo);
	public boolean insertarCuota(Cuota cuota);
	public boolean actualizarPrestamo(Prestamo prestamo);
	public boolean actualizarCuota(Cuota cuota);
	public boolean insertarCuotas(ArrayList<Cuota> cuotas);
	public TipoMovimiento obtenerTipoMovimiento(String codigo);
	public EstadoPrestamo obtenerEstadoPrestamo(String codigo);
	public ArrayList<Transferencia> obtenerTransferenciasCuentaOrigen(int idCuenta);
	public ArrayList<Prestamo> obtenerPrestamosPersonaId(int idPersona);
	public ArrayList<Cuota> obtenerCuotasPrestamo(int idPrestamo);
	public Cuota obtenerCuota(int idCuota);
	public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta);

}
