package frgp.utn.edu.ar.resources;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;

import frgp.utn.edu.ar.entidad.*;
import frgp.utn.edu.ar.ServiciosImpl.*;
import frgp.utn.edu.ar.DaoImpl.*;

@Configuration
public class Config {

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
	public ModelAndView modelAndView() {
		ModelAndView MV = new ModelAndView();
		return MV;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
	public CuentaServicioImpl cuentaServicioImpl() {
		CuentaServicioImpl obj = new CuentaServicioImpl();
		obj.setDataAccess(new CuentaDaoImpl());
		return obj;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
	public PersonaServicioImpl personaServicioImpl() {
		PersonaServicioImpl obj = new PersonaServicioImpl();
		obj.setDataAccess(new PersonaDaoImpl());
		return obj;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
	public UsuarioServicioImpl usuarioServicioImpl() {
		UsuarioServicioImpl obj = new UsuarioServicioImpl();
		obj.setDataAccess(new UsuarioDaoImpl());
		return obj;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
	public TransaccionServicioImpl transaccionServicioImpl() {
		TransaccionServicioImpl obj = new TransaccionServicioImpl();
		obj.setDataAccess(new TransaccionDaoImpl());
		return obj;
	}
	
	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_SINGLETON)
	public MovimientoServicioImpl movimientoServicioImpl() {
		MovimientoServicioImpl obj = new MovimientoServicioImpl();
		obj.setDataAccess(new MovimientoDaoImpl());
		return obj;
	}

	//-----------
	//ENTIDADES
	//-----------
	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public AltaCuenta altaCuenta() {
		AltaCuenta obj = new AltaCuenta();
		return obj;
	}
	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Cuenta cuenta() {
		Cuenta cta = new Cuenta();
		return cta;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Cuota cuota() {
		Cuota cuo = new Cuota();
		return cuo;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Movimiento movimiento() {
		Movimiento mov = new Movimiento();
		return mov;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Prestamo prestamo() {
		Prestamo pre = new Prestamo();
		return pre;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public EstadoPrestamo estadoPrestamo() {
		EstadoPrestamo estPre = new EstadoPrestamo();
		return estPre;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public TipoCuenta tipoCuenta() {
		TipoCuenta tcta = new TipoCuenta();
		return tcta;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public TipoMovimiento tipoMovimiento() {
		TipoMovimiento tmov = new TipoMovimiento();
		return tmov;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Perfil perfil() {
		Perfil obj = new Perfil();
		return obj;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Transferencia transferencia() {
		Transferencia tra = new Transferencia();
		return tra;
	}
	
	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Localidad localidad() {
		Localidad obj = new Localidad();
		return obj;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Persona persona() {
		Persona obj = new Persona();
		return obj;
	}

	@Bean()
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Usuario usuario() {
		Usuario usu = new Usuario();
		return usu;
	}

}
