<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/contact_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />" type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<style type="text/css">
	.titulo{
	padding-bottom: inherit;
	}
	
	.rowCuenta{
    padding-bottom: 12px;
    border-top-style: double;
    border-bottom: double;
    padding-top: 21px;
    border-color: coral;
    margin-bottom: 15px;
	}
	.textoTitulo{
		    margin-left: auto;
    margin-right: auto;
	}

	.rowCuentaInterior{
	float: left;
    padding-right: inherit;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: auto;
    margin-top: auto;
	}
	
	.iconMoney{
		float: left;
    margin-right: 10px;
	}

/* table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting{ */
/*  padding-right: 30px; */
/*  background: coral; */
/* } */
	</style>

</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

 <%@include file="Menu.jsp"%>

	</header>
	<div class="container contact_container">
			<div class="row titulo">
					<h1 class="textoTitulo">Bienvenido a BankUTN ${userName}</h1>
			</div>
	
	
	
		<c:forEach var="cuenta" items="${listaCuentas}">
			<div class="row rowCuenta" >
    
				<div class="rowCuentaInterior">
					<p>Nombre De Cuenta:</p>
					<span>${cuenta.getNombre()}</span>
				</div>
				<div class="rowCuentaInterior">
					<p>Descripción:</p>
					<span>${cuenta.getDescripcion()}</span>
				</div>
				<div class="rowCuentaInterior">
					<p>Saldo:</p>
					<span>${cuenta.getSaldo()}</span>
				</div>
				<div class="rowCuentaInterior">
	<a href="<c:url value="/Redirigir_MovimientosPorCuenta.html?idCuenta=${cuenta.getId()}" />"><i class="material-icons iconMoney">monetization_on</i><span>ir a Movimientos</span> </a>

<!--  					<form method="get" action="Redirigir_MovimientosPorCuenta.html">	 -->
<!-- 					<button type="submit" name="idCuenta"  -->
<%-- 					value="${cuenta.getId()}"  --%>
<!-- 						style="height: 60px; width: 60px"><i class="material-icons">monetization_on</i>Movimientos</button> -->
<!-- 				</form>   -->
				
				</div>
			</div>
		
		</c:forEach>
</div>

	</div>


	  
 <%@include file="Footer.jsp"%>



</body>

</html>