package frgp.utn.edu.ar.Servicios;


import java.util.Date;
import java.util.List;


import frgp.utn.edu.ar.entidad.Prestamo;
import frgp.utn.edu.ar.entidad.Usuario;


public interface PrestamoServicio {

	 public List<Prestamo> obtenerPrestamoPorUsuario(Integer idUsuario);
	 public List<Prestamo> obtenerTodosPrestamo();
	 public boolean actualizarPrestamoE(Prestamo prestamo);
	 public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta);
	 
	 Prestamo getPrestamoById(int id);
	 public List<Prestamo> obtenerTodosPrestamoByEstadoDesdeHasta(Integer id,Date desde,Date hasta);
}
