package frgp.utn.edu.ar.controller;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import frgp.utn.edu.ar.Servicios.CuentaServicio;
import frgp.utn.edu.ar.Servicios.PersonaServicio;
import frgp.utn.edu.ar.entidad.*;
import frgp.utn.edu.ar.resources.*;

public class CuentaController {

	private ApplicationContext appContext;
	
	private CuentaServicio cuentaServicio;
	private PersonaServicio personaServicio;

	public CuentaController() {
		this.appContext = new AnnotationConfigApplicationContext(Config.class);
	}
	
	public void closeAppContext() {
		((ConfigurableApplicationContext)(appContext)).close();
	}

}
