package frgp.utn.edu.ar.Servicios;

import java.util.List;

import frgp.utn.edu.ar.entidad.Localidad;

public interface LocalidadServicio {

	 List<Localidad> obtenerTodos();
	 Localidad obtenerLocalidad(Integer CP);
	 boolean InsertarLocalidad(Localidad loc);
}