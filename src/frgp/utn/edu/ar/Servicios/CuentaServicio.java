package frgp.utn.edu.ar.Servicios;


import java.util.List;


import frgp.utn.edu.ar.entidad.Cuenta;
import frgp.utn.edu.ar.entidad.TipoCuenta;


public interface CuentaServicio {

	 public List<Cuenta> obtenerCuentaPorUsuario(Integer idUsuario);
	 public List<Cuenta> obtenerCuentasActivasPorUsuario(Integer idUsuario);
	 public List<Cuenta> obtenerTodosCuenta();
	 public boolean insertarCuenta(Cuenta cuenta);
	 public List<TipoCuenta> obtenerTodosTipoCuenta();
	 public TipoCuenta obtenerTipoCuenta(int id);
	 public TipoCuenta obtenerTipoCuenta(String codigo);
	 public Cuenta obtenerCuentaPorCbu(String cbu);
	 public Cuenta obtenerCuentaPorIdPersonaIdTipoCuenta(int idPersona, int idTipoCuenta);
	 public List<Cuenta>obtenerCuentasPorIdPersona(int idPersona);
	 public Cuenta obtenerCuentaPorId(int id);
	 public boolean actualizarCuenta(Cuenta cuenta);
	 public boolean actualizarCuentas(List<Cuenta> cuentas);
}
