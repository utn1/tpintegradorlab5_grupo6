package frgp.utn.edu.ar.Dao;
import java.util.List;

import frgp.utn.edu.ar.entidad.EstadoPrestamo;

public interface EstadoPrestamoDao {
	public List<EstadoPrestamo> obtenerTodos();
	public EstadoPrestamo obtenerEstadoPrestamo(Integer idEstadoPrestamo);
	public boolean InsertarEstadoPrestamo(EstadoPrestamo Est);
}

