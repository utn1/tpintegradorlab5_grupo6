package frgp.utn.edu.ar.ServiciosImpl;

import java.util.List;

import frgp.utn.edu.ar.Dao.PerfilDao;

import frgp.utn.edu.ar.Servicios.PerfilServicio;

import frgp.utn.edu.ar.entidad.Perfil;

public class PerfilServicioImpl implements PerfilServicio {

	private PerfilDao dataAccess = null;

	public void setDataAccess(PerfilDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public Perfil obtenerPerfil(Integer id) {

		return this.dataAccess.obtenerPerfil(id);

	}

	@Override
	public List<Perfil> obtenerTodos() {

		return this.dataAccess.obtenerTodos();

	}

}