package frgp.utn.edu.ar.DaoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frgp.utn.edu.ar.Dao.Conexion;
import frgp.utn.edu.ar.Dao.HibernateUtil;
import frgp.utn.edu.ar.Dao.EstadoPrestamoDao;
import frgp.utn.edu.ar.entidad.EstadoPrestamo;
import frgp.utn.edu.ar.entidad.Persona;

public class EstadoPrestamoDaoImpl implements EstadoPrestamoDao  {
@Autowired
	private Conexion<EstadoPrestamo> cn;
	
	public EstadoPrestamoDaoImpl()
	{
		cn = new Conexion<EstadoPrestamo>();
	}

	
	@Override
	public List<EstadoPrestamo> obtenerTodos() {
		
		 
		Session session = cn.abrirConexion();

		String hql = "from EstadoPrestamo";
		List<EstadoPrestamo> tu = (List<EstadoPrestamo>)session.createQuery(hql).list();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public EstadoPrestamo obtenerEstadoPrestamo(Integer Id) {
		
		 
		Session session = cn.abrirConexion();
		String hql = "from EstadoPrestamo where Id = '" + Id + "'";
		EstadoPrestamo tu = (EstadoPrestamo)session.createQuery(hql).uniqueResult();
		
		cn.cerrarSession();
		return tu;
	}
	
	@Override
	public boolean InsertarEstadoPrestamo(EstadoPrestamo estado) {
		
		cn.add(estado);
		return true;
	}

}
