<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/contact_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />" type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>

<script type="text/javascript">
	$(document).ready(
		function($) {
			debugger;
			$('#example').DataTable(
			{
				"language" : {
					"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});

		});
</script>
<style type="text/css">

.contenedorPrincipal{
    margin-left: auto;
    margin-right: auto;
    margin-bottom: auto;
    margin-top: auto;
	}
	
	.buttonLogin{
	padding-top: 30px;
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
	}
	
	
	.red_button2 {
    display: -webkit-inline-box;
    display: -moz-inline-box;
    display: -ms-inline-flexbox;
    display: -webkit-inline-flex;
    display: inline-flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    /* width: auto; */
    height: 40px;
    background: #fe4c50;
    border-radius: 3px;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
}
	
</style>
</head>


<body>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

		<!-- Main Navigation -->


		<!-- Main Navigation -->

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<a href="#">bank<span>utn</span></a>
						</div>
						<nav class="navbar">
							<ul class="navbar_menu">
							</ul>
							<ul class="navbar_user">
								<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></li>
							</ul>
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>

	</header>
	<div class="container contact_container">
		<div class="row">
			<div class="col">

			</div>
		</div>

		<!-- Contact Us -->

		<div class="row">
			<div class="col-lg-6 get_in_touch_col contenedorPrincipal" >
				<form method="post" action="procesar_Login.html">
				<div class="get_in_touch_contents">
				
					<h1>Bienvenido a UTN-BANK</h1>

					

						<div style="float">
							<p>Usuario:</p>
							<input id="txtCodigo" class="form_input input_name input_ph" type="text" name="txtCodigo" placeholder="codigo" required="required" data-error="Codigo is required.">
						</div>

						<div style="float">
							<p>Contraseņa:</p>
							<input id="txtContrasenia" class="form_input input_name input_ph" type="password" name="txtContrasenia" placeholder="contraseņa" required="required" data-error="Contraseņa is required.">
						</div>

				
				</div>
				<div  class="get_in_touch_contents">
						<div class=buttonLogin>
							<button id=btnSubmit type="submit" class="red_button2 message_submit_btn trans_300" name="btnSubmit" value="administrador">Login</button>
						</div>
				</div>
				</form>
			</div>

		</div>
	</div>


	<!-- Footer -->

	<footer class="footer">
		<%@include file="Footer.jsp"%>
	</footer>

</div>


</body>

</html>