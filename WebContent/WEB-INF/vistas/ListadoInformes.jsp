<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bank</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="<c:url value="/resources/css/contact_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/responsive.css" />" type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/main_styles.css" />"  type="text/css" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap4/bootstrap.min.css" />"  type="text/css" rel="stylesheet">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
	rel='stylesheet' type='text/css'>
<link href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css' rel='stylesheet' type='text/css'>

<script type="text/javascript">
	$(document).ready(
		function($) {
			debugger;
			$('#example').DataTable(
			{
				"language" : {
					"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				}
			});

		});
</script>

</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

		 <%@include file="Menu.jsp"%>

	</header>
	<div class="container contact_container">


		<div class="row">
			<div class="col-lg-6 get_in_touch_col">
				<div class="get_in_touch_contents">
					<h1>Informes</h1>
					<form method="get" action="redirigir_informeSeleccionado.html">
						<div style="">
							<p>Seleccion el informe</p>
							<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
							<select id="txtInforme"
								class="form_input input_name input_ph" type="select"
								name="txtInforme"
								required="required" data-error="Debe seleccionar un informe.">
								<option value="prestamos_solicitados">Prestamos solicitados</option>
							</select>
						</div>
						<div>
							
								<button id="review_submit" type="submit" name="btnSubmit" value="guardar"
								class="red_button message_submit_btn trans_300" value="Submit">
								CONTINUAR
								</button>
						</div>
						</form>
						<div>
							<form method="get" action="HomePageAdmin.html">
							<button id="review_submit" type="submit" name="btnSubmit" value="volver"
							class="red_button message_submit_btn trans_300" style="background-color:red" value="Submit">
							VOLVER
							</button>
							
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>


 <%@include file="Footer.jsp"%>

</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>
</body>

</html>
