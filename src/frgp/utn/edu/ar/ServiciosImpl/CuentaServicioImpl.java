package frgp.utn.edu.ar.ServiciosImpl;

import java.util.List;

import frgp.utn.edu.ar.Dao.CuentaDao;

import frgp.utn.edu.ar.Servicios.CuentaServicio;

import frgp.utn.edu.ar.entidad.Cuenta;
import frgp.utn.edu.ar.entidad.TipoCuenta;

public class CuentaServicioImpl implements CuentaServicio {

	private CuentaDao dataAccess = null;

	public void setDataAccess(CuentaDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public List<Cuenta> obtenerCuentaPorUsuario(Integer id) {
		System.out.println("srv->obtenerCuentaPorUsuario->init");
		return this.dataAccess.obtenerCuentaPorUsuario(id);
	}

	@Override
	public List<Cuenta> obtenerCuentasActivasPorUsuario(Integer id) {
		System.out.println("srv->obtenerCuentasActivasPorUsuario->init");
		return this.dataAccess.obtenerCuentasActivasPorUsuario(id);
	}

	@Override
	public List<Cuenta> obtenerTodosCuenta() {
		return this.dataAccess.obtenerTodosCuenta();
	}

	@Override
	public boolean insertarCuenta(Cuenta cuenta) {
		return this.dataAccess.insertarCuenta(cuenta);
	}

	@Override
	public List<TipoCuenta> obtenerTodosTipoCuenta(){
		System.out.println("srv->obtenerTodosTipoCuenta->init");
		return this.dataAccess.obtenerTodosTipoCuenta();
	}

	@Override
	public TipoCuenta obtenerTipoCuenta(int id) {
		System.out.println("srv->obtenerTipoCuenta->init");
		return this.dataAccess.obtenerTipoCuenta(id);
	}

	@Override
	public TipoCuenta obtenerTipoCuenta(String codigo) {
		System.out.println("srv->obtenerTipoCuenta->init");
		return this.dataAccess.obtenerTipoCuenta(codigo);
	}

	@Override
	public Cuenta obtenerCuentaPorCbu(String cbu) {
		return this.dataAccess.obtenerCuentaPorCbu(cbu);
	}

	@Override
	public Cuenta obtenerCuentaPorIdPersonaIdTipoCuenta(int idPersona, int idTipoCuenta) {
		return this.dataAccess.obtenerCuentaPorIdPersonaIdTipoCuenta(idPersona, idTipoCuenta);
	}
	
	@Override
	public List<Cuenta>obtenerCuentasPorIdPersona(int idPersona) {
		return this.dataAccess.obtenerCuentasPorIdPersona(idPersona);
	}
	
	@Override
	public Cuenta obtenerCuentaPorId(int id) {
		return this.dataAccess.obtenerCuentaPorId(id);
	}

	@Override
	public boolean actualizarCuenta(Cuenta cuenta) {
		return this.dataAccess.actualizarCuenta(cuenta);
	}
	
	@Override
	public boolean actualizarCuentas(List<Cuenta> cuentas) {
		return this.dataAccess.actualizarCuentas(cuentas);
	}
}