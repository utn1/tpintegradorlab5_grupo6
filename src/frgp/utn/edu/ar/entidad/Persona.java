package frgp.utn.edu.ar.entidad;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Persona")
public class Persona implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="IdPersona")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int IdPersona ;

	@Column(name="Dni", unique=true)
	private int Dni ;

	@Column(name="Nombre")
	private String Nombre ;
	
	@Column(name="Apellido")
	private String Apellido ;
	

	@Column(name="Direccion")
	private String Direccion;
	
	@Column(name="DireccionNro")
	private String DireccionNro;
	
	@Column(name="Sexo")
	private String Sexo;
	
	@Column(name="FechaNac")
	private Date FechaNac;
	

	@Column(name="Cuil", unique=true)
	private String Cuil ;
	
	@Column(name="Nacionalidad")
	private String Nacionalidad ;
	

	@Column(name="Mail")
	private String Mail;
	
	@Column(name="Activo")
	private Boolean Activo;

	@ManyToOne(cascade= {CascadeType.ALL}) //Tipo de relacion
	@JoinColumn(name="CP")
	private Localidad localidad;
	
	@ManyToOne(cascade= {CascadeType.ALL}) //Tipo de relacion
	@JoinColumn(name="idProvincia")
	private Provincia Provincia;
	
	public String getApellidoNombre() {
		return this.Apellido + " " + this.Nombre;
	}
	
	public Provincia getProvincia() {
		return Provincia;
	}

	public void setProvincia(Provincia provincia) {
		Provincia = provincia;
	}

	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	public Persona()
	{
		
	}

	public int getIdPersona() {
		return IdPersona;
	}

	public void setIdPersona(int idPersona) {
		this.IdPersona = idPersona;
	}

	public int getDni() {
		return Dni;
	}

	public void setDni(int dni) {
		this.Dni = dni;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		this.Nombre = nombre;
	}
	
	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		this.Direccion = direccion;
	}
	
	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String apellido) {
		Apellido = apellido;
	}

	public Date getFechaNac() {
		return FechaNac;
	}

	public void setFechaNac(Date fechaNac) {
		FechaNac = fechaNac;
	}

	public String getCuil() {
		return Cuil;
	}

	public void setCuil(String cuil) {
		Cuil = cuil;
	}
	
	public String getDireccionNro() {
		return DireccionNro;
	}

	public void setDireccionNro(String direccionNro) {
		DireccionNro = direccionNro;
	}
	
	public String getSexo() {
		return Sexo;
	}

	public void setSexo(String sexo) {
		Sexo = sexo;
	}

	public String getMail() {
		return Mail;
	}


	public void setMail(String mail) {
		Mail = mail;
	}
	
	public Boolean getActivo() {
		return Activo;
	}

	
	public String getNacionalidad() {
		return Nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		Nacionalidad = nacionalidad;
	}
	
	public void setActivo(Boolean activo) {
		Activo = activo;
	}


	@Override
	public String toString() {
		return "Persona [IdPersona=" + IdPersona + ", Dni=" + Dni + ", Nombre=" + Nombre + ", Apellido=" + Apellido
				+ ", Direccion=" + Direccion + ", DireccionNro=" + DireccionNro + ", Sexo=" + Sexo + ", FechaNac="
				+ FechaNac + ", Cuil=" + Cuil + ", localidad=" + localidad + "]";
	}

	
}
