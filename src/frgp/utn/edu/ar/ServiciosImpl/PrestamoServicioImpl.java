package frgp.utn.edu.ar.ServiciosImpl;

import java.util.Date;
import java.util.List;

import frgp.utn.edu.ar.Dao.PrestamoDao;
import  frgp.utn.edu.ar.entidad.*;

import frgp.utn.edu.ar.Servicios.PrestamoServicio;


public class PrestamoServicioImpl implements PrestamoServicio {

	private PrestamoDao dataAccess = null;

	public void setDataAccess(PrestamoDao dataAccess) {
		this.dataAccess = dataAccess;
	}

	@Override
	public List<Prestamo> obtenerPrestamoPorUsuario(Integer id) {
		System.out.println("srv->obtenerCuentaPorUsuario->init");
		return this.dataAccess.obtenerPrestamoPorUsuario(id);
	}

	@Override
	public List<Prestamo> obtenerTodosPrestamo() {
		return this.dataAccess.obtenerTodosPrestamo();
	}
	
	@Override
	public Prestamo getPrestamoById(int id) {

			return this.dataAccess.getPrestamoById(id);
	}
	
	@Override
	public boolean actualizarPrestamoE(Prestamo prestamo) {
		return this.dataAccess.actualizarEstadoPrestamoE(prestamo);
	}
	
	@Override
	public List<Prestamo> obtenerPrestamoPorCuenta(Integer idCuenta){
		return this.dataAccess.obtenerPrestamoPorCuenta(idCuenta);
	}
	@Override
	public List<Prestamo> obtenerTodosPrestamoByEstadoDesdeHasta(Integer id,Date desde,Date hasta) {
		return this.dataAccess.obtenerTodosPrestamoByEstadoDesdeHasta(id,desde,hasta);
	}
	
}