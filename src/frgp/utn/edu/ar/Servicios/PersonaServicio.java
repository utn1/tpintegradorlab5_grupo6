package frgp.utn.edu.ar.Servicios;



import frgp.utn.edu.ar.entidad.Persona;

public interface PersonaServicio {

	//Alta de persona
	 public boolean insertarPersona(Persona persona);
	 public boolean actualizarPersona(Persona persona);
	 public Persona getPersonaById(Integer idPersona);
	 public Persona obtenerPersonaPorDni(String dni);
	
}